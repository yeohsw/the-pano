/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import { Container, Header, Content, Button, Root } from 'native-base';

import {createStore} from 'redux';
import { Provider } from 'react-redux';

import AppNav from './screen/appNavigation.js';

const initialState = {
    token: "",
    refreshUnit: false,
    followUpId:'',
    refreshFollowUpList:false,
    refreshSalesList:true,
    permission:[],
    caseClose: 0,
    statusSetting:[],
    phaseName:'THE PANO'
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_TOKEN':
            return {...state, token: action.param }
        case 'SET_REFRESH_UNIT':
            return {...state, refreshUnit: action.param }
        case 'SET_REFRESH_SALESLIST':
            return {...state, refreshSalesList: action.param }
        case 'SET_FOLLOW_UP_ID':
            return {...state, followUpId: action.param }
        case 'SET_REFRESH_FOLLOW_UP':
            return {...state, refreshFollowUpList: action.param }
        case 'SET_PERMISSION':
            return {...state, permission: action.param }
        case 'SET_CASE_CLOSE':
            return {...state, caseClose: action.param }
        case 'SET_STATUS_SETTING':
            return {...state, statusSetting: action.param }
        default:
            return state
    }
}

const store = createStore(reducer);


type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
       <Provider store={store}>
          <Root>
            <AppNav />
          </Root>
       </Provider>
    );
  }
}


