import { AppRegistry, YellowBox } from 'react-native';
import 'react-native-gesture-handler'
import App from './App';

global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');


YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader",
  "Class RCTCxxModule"
]);

AppRegistry.registerComponent('Yolo', () => App);
