import React, { Component } from 'react'
import { Animated, View, StyleSheet, Image, Dimensions, ScrollView, TouchableOpacity, Modal, Text } from 'react-native'
import { Container, Header, Title, Right, Left, Body, Button, Icon, Item, Content } from 'native-base'
import { styles as defaultStyle, colors } from '../globalStyle.js'
import ImageZoom from 'react-native-image-pan-zoom'
import ImageViewer from 'react-native-image-zoom-viewer'
const deviceWidth = Dimensions.get('window').width
const deviceHeight = Dimensions.get('window').height*0.8
const FIXED_BAR_WIDTH = 100
const BAR_SPACE = 10

const images = [
  require('../image/usp/usp1.png'),
  require('../image/usp/usp2.png'),
  require('../image/usp/usp3.png'),
  require('../image/usp/usp4.png'),
  require('../image/usp/usp5.png'),
  require('../image/usp/usp6.png'),
]

export default class App extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            imageViewer:false,
            image:[],
            page:0,
            disableLeft:false,
            disableRight:false
        }
    }

  render() {
    const {goBack} = this.props.navigation;
    return (
        <Container style={{backgroundColor:colors.bgColor}}>
 
    {/*<Image
      style={[StyleSheet.absoluteFill,{height:200, width:'100%', marginTop:-100}]}
      source={require('../image/usp/USP.png')}
    />
            <Header style={{backgroundColor:'transparent',borderBottomWidth: 0, elevation:0, shadowColor: 'transparent'}}>
              <Left>
                <TouchableOpacity onPress={() => goBack()}>
                    <Icon name="back" type="Entypo" />
                </TouchableOpacity>                  
              </Left>
              <Body>
                <Title>USP</Title>
              </Body>
              <Right>
                <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
                  <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
                </Button>                     
              </Right>
            </Header>*/}
                  
            <Header style={defaultStyle.headerOverlap}>
              <Left>
                <TouchableOpacity onPress={() => goBack()}>
                    <Icon name="back" type="Entypo" />
                </TouchableOpacity>                  
              </Left>
              <Body>
                <Title style={{color:colors.formTxtColor}}>USP</Title>
              </Body>
              <Right>
                <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
                  <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
                </Button>                     
              </Right>
            </Header>                  
       

            <Content>
                <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>
                    <ImageZoom 
                               cropWidth={Dimensions.get('window').width*0.95}
                               cropHeight={Dimensions.get('window').height*0.8}
                               imageWidth={Dimensions.get('window').width*0.95}
                               imageHeight={Dimensions.get('window').height*0.8}
                               maxScale={3}
                               minScale={1}
                               style={{backgroundColor:'transparent', alignSelf:'center'}}
                    >                
                        <Image     
                            style={{ 
                                    width: '100%',
                                    height: '100%',
                                    resizeMode: 'contain',
                                    }}
                            source={images[this.state.page]}
                        />  
                    </ImageZoom>
                    <Item style={{alignSelf:'center', borderColor:'transparent'}}>
                        {this.state.page == 0?<View style={{width:'50%'}}/>:<Button style={{width:'50%', justifyContent:'flex-end'}} transparent disabled={this.state.disableLeft} 
                            onPress={()=>this.state.page<=0? 
                            this.setState({disableLeft:true, disableRight:false})
                            :this.setState((prev)=>({page:prev.page-1, disableRight:false}))}
                            >
                            <Icon name='doubleleft' type='AntDesign' style={{color:colors.btnColor}}/>
                        </Button>}
                        {this.state.page == 5?<View style={{width:'50%'}}/>:<Button style={{width:'50%'}} transparent disabled={this.state.disableRight} 
                            onPress={()=>this.state.page>=5?
                                this.setState({disableRight:true, disableLeft:false})
                                :this.setState((prev)=>({page:prev.page+1, disableLeft:false}))
                            }
                            >
                            <Icon name='doubleright' type='AntDesign' style={{color:colors.btnColor}}/>
                        </Button>}
                    </Item>
                </View>
            </Content>
            <Modal visible={this.state.imageViewer} transparent={false} style={{backgroundColor:colors.bgColor}} onRequestClose={() => {}}>
                <ImageViewer enablePreload={true} enableImageZoom={true} style={{backgroundColor:colors.bgColor}} imageUrls={this.state.image}/>
                <Button style={[defaultStyle.defaultBtn,{width:'100%'}]} block onPress={()=>{this.setState({imageViewer:false})}}><Text>Back</Text></Button>
            </Modal>            
        </Container>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  barContainer: {
    position: 'absolute',
    zIndex: 2,
    top: 20,
    flexDirection: 'row',
  },
  track: {
    backgroundColor: '#ccc',
    overflow: 'hidden',
    height: 2,
  },
  bar: {
    backgroundColor: '#5294d6',
    height: 2,
    position: 'absolute',
    left: 0,
    top: 0,
  },
})