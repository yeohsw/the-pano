import React, { Component } from 'react';
import { Container, Content, Header, Footer, Left, Body, Right, Button, Icon, Title, Segment, Text, Picker, Form, Item} from 'native-base';
import { Dimensions, StyleSheet, Animated, ScrollView, Image, View, TouchableOpacity, Modal } from 'react-native'
import { styles as defaultStyle, colors } from '../globalStyle.js'

import ImageViewer from 'react-native-image-zoom-viewer'
import ImageZoom from 'react-native-image-pan-zoom'

const deviceWidth = Dimensions.get('window').width
const deviceHeight = Dimensions.get('window').height*0.8
const FIXED_BAR_WIDTH = 100
const BAR_SPACE = 10

const images = [
  require('../image/plan/facilitiesPlan/facilitiesPlan1.png')
]

export default class HeaderIconExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
        facilitiesPlan:true,
        layoutPlan:false,
        selected: "A",
        unitPlan: true,
        image:require('../image/plan/layoutPlan/LayoutPlan_TypeA.png'),
        imageViewer:false,
        imageViewer2:false,
        imageZoom:[],
        layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeA.png')}}],
        page:0,
        disableRight:false,
        disableLeft:false,
        specification:false
    }  
    
  }
    
  numItems = images.length
  itemWidth = (FIXED_BAR_WIDTH / this.numItems) - ((this.numItems - 1) * BAR_SPACE)
  animVal = new Animated.Value(0)

  onValueChange(value: string) {
    switch(value){
        case 'A':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeA.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeA.png')}}]
            })
            break;
        case 'A1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeA1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeA1.png')}}]
            })
            break;
        case 'B':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeB.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeB.png')}}]
            })
            break;
        case 'B1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeB1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeB1.png')}}]
            })
            break;
        case 'C':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeC.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeC.png')}}]
            })
            break;
        case 'C1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeC1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeC1.png')}}]
            })
            break;
        case 'E':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeE.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeE.png')}}]
            })
            break;
        case 'H1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeH1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeH1.png')}}]
            })
            break;
        case 'J1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeJ1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeJ1.png')}}]
            })
            break;
    }
      
    this.setState({
      selected: value,
      unitPlan: true
    });
  }
    
  chgUnitPlan(){
    if(this.state.unitPlan == true){
        switch(this.state.selected){
      case 'A':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeA.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeA.png')}}]
            })
            break;
        case 'A1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeA1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeA1.png')}}]
            })
            break;
        case 'B':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeB.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeB.png')}}]
            })
            break;
        case 'B1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeB1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeB1.png')}}]
            })
            break;
        case 'C':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeC.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeC.png')}}]
            })
            break;
        case 'C1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeC1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeC1.png')}}]
            })
            break;
        case 'E':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeE.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeE.png')}}]
            })
            break;
        case 'H1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeH1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeH1.png')}}]
            })
            break;
        case 'J1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeJ1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeJ1.png')}}]
            })
            break;
        }
        this.setState({
            unitPlan:false,
            specification:false
        })
    }
    else{
        switch(this.state.selected){
      case 'A':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeA.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeA.png')}}]
            })
            break;
        case 'A1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeA1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeA1.png')}}]
            })
            break;
        case 'B':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeB.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeB.png')}}]
            })
            break;
        case 'B1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeB1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeB1.png')}}]
            })
            break;
        case 'C':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeC.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeC.png')}}]
            })
            break;
        case 'C1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeC1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeC1.png')}}]
            })
            break;
        case 'E':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeE.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeE.png')}}]
            })
            break;
        case 'H1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeH1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeH1.png')}}]
            })
            break;
        case 'J1':
            this.setState({
                image:require('../image/plan/layoutPlan/LayoutPlan_TypeJ1.png'),
                layoutZoom:[{props:{source:require('../image/plan/layoutPlan/LayoutPlan_TypeJ1.png')}}]
            })
            break;
        }
        this.setState({
            unitPlan:true,
            specification:false
        })
    }

  }
    
  render() {
  
    
    this.state.facilitiesPlan == true?
    shows = (
        <View
            style={styles.container}
            flex={1}
        >
            <ImageZoom 
                       cropWidth={Dimensions.get('window').width*0.95}
                       cropHeight={Dimensions.get('window').height*0.7}
                       imageWidth={Dimensions.get('window').width*0.95}
                       imageHeight={Dimensions.get('window').height*0.7}
                       maxScale={3}
                       minScale={1}
                       style={{backgroundColor:'transparent', alignSelf:'center'}}
            >                
                <Image     
                    style={{ 
                            width: '100%',
                            height: '100%',
                            resizeMode: 'contain',
                            }}
                    source={images[this.state.page]}
                />  
            </ImageZoom>
            {/*<Item style={{alignSelf:'center', borderColor:'transparent'}}>
                {this.state.page == 0?<View/>:<Button transparent disabled={this.state.disableLeft} 
                    onPress={()=>this.state.page<=0? 
                    this.setState({disableLeft:true, disableRight:false})
                    :this.setState((prev)=>({page:prev.page-1, disableRight:false}))}
                    >
                    <Icon name='doubleleft' type='AntDesign' style={{color:colors.btnColor}}/>
                </Button>}
                {this.state.page == 1?<View/>:<Button transparent disabled={this.state.disableRight} 
                    onPress={()=>this.state.page>=1?
                        this.setState({disableRight:true, disableLeft:false})
                        :this.setState((prev)=>({page:prev.page+1, disableLeft:false}))
                    }
                    >
                    <Icon name='doubleright' type='AntDesign' style={{color:colors.btnColor}}/>
                </Button>}
            </Item>*/}
        </View>
    ):
    shows = (
        <View flex={1} style={{flex:1, flexDirection:'column', justifyContent:'flex-start'}}>
            <ImageZoom 
               cropWidth={Dimensions.get('window').width*0.95}
               cropHeight={Dimensions.get('window').height*0.7}
               imageWidth={Dimensions.get('window').width*0.95}
               imageHeight={Dimensions.get('window').height*0.7}
               maxScale={3}
               minScale={1}
               style={{backgroundColor:'transparent', alignSelf:'center'}}
            >
                <Image     
                    style={{ 
                            width: '100%',
                            height: Dimensions.get('window').height-200,
                            resizeMode: 'contain',
                            }}
                    source={this.state.image}
                />
            </ImageZoom>
            <View style={{flex:1, flexDirection:'row', justifyContent:'flex-end'}}>
                {/*!this.state.specification?
                <Segment style={{backgroundColor:'transparent', height:40, marginRight:10}}>
                    <Button active={this.state.unitPlan} bordered style={{height:40, borderColor:colors.btnColor, backgroundColor:this.state.unitPlan?colors.btnColor:'transparent'}} onPress={this.chgUnitPlan.bind(this)}><Text style={{color:colors.bgColor, fontSize:13}}>Unit</Text></Button>
                    <Button active={!this.state.unitPlan} bordered style={{height:40, borderColor:colors.btnColor, backgroundColor:!this.state.unitPlan?colors.btnColor:'transparent'}} onPress={this.chgUnitPlan.bind(this)}><Text style={{color:colors.bgColor, fontSize:13}}>Layout</Text></Button>
                </Segment>                    
                 :<View/>
                */}
                 <Button bordered style={{height:40, borderColor:colors.btnColor, marginRight:10}} onPress={()=>!this.state.specification?this.setState({image:require('../image/plan/specification.png'), specification:true}):this.chgUnitPlan()}><Text style={{color:colors.bgColor, fontSize:13}}>{this.state.specification?'Back':'Specification'}</Text></Button>
                {!this.state.specification?
                <Item style={{width:80, borderLeftWidth:1, borderRightWidth:1, borderTopWidth:1, borderBottomWidth:1, borderColor:colors.btnColor, marginRight:10}}>
                    <Picker
                      mode="dropdown"
                      iosHeader="Select Unit Type"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined}}
                      selectedValue={this.state.selected}
                      onValueChange={this.onValueChange.bind(this)}
                    >
                      <Picker.Item label="A" value="A" />
                      <Picker.Item label="A1" value="A1" />
                      <Picker.Item label="B" value="B" />
                      <Picker.Item label="B1" value="B1" />
                      <Picker.Item label="C" value="C" />
                      <Picker.Item label="C1" value="C1" />
                      <Picker.Item label="E" value="E" />
                      <Picker.Item label="H1" value="H1" />
                      <Picker.Item label="J1" value="J1" />
                    </Picker>
               </Item>:<View/>}
            </View>
        </View>
    )
    
    const {goBack} = this.props.navigation;
    return (
        
      <Container >
        <Header style={defaultStyle.headerOverlap}>
          <Left>
            <TouchableOpacity onPress={() => goBack()}>
                <Icon name="back" type="Entypo" />
            </TouchableOpacity>                  
          </Left>
          <Body>
            <Title style={{color:colors.formTxtColor}}>Plan</Title>
          </Body>
          <Right>
            <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
              <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
            </Button>                     
          </Right>
        </Header>
        <Segment style={{backgroundColor:'transparent'}}>
          <Button style={{borderColor:colors.btnColor, backgroundColor:this.state.facilitiesPlan == true? colors.btnColor:'transparent'}} first active={this.state.facilitiesPlan} onPress={()=>this.setState({facilitiesPlan:true, layoutPlan:false})}>
            <Text style={{color:this.state.facilitiesPlan == true? '#000000':colors.bgColor}}>Facilities Plan</Text>
          </Button>
          <Button style={{borderColor:colors.btnColor, backgroundColor:this.state.layoutPlan == true? colors.btnColor:'transparent'}} last active={this.state.layoutPlan} onPress={()=>this.setState({facilitiesPlan:false, layoutPlan:true})}>
            <Text style={{color:this.state.layoutPlan == true? '#000000':colors.bgColor}}>Layout Plan</Text>
          </Button>
        </Segment>
        <Content>
          {shows}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  barContainer: {
    position: 'absolute',
    zIndex: 2,
    top: 10,
    flexDirection: 'row',
  },
  track: {
    backgroundColor: '#ccc',
    overflow: 'hidden',
    height: 2,
  },
  bar: {
    backgroundColor: '#5294d6',
    height: 2,
    position: 'absolute',
    left: 0,
    top: 0,
  },
})