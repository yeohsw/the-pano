import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Left, Right, Body, Title, Icon } from 'native-base'
import { View, ActivityIndicator, Dimensions, Image } from 'react-native'
import {styles, colors} from '../globalStyle.js'


import {connect} from 'react-redux'
import { Col, Row, Grid } from "react-native-easy-grid";

export default class projectNavigationPage extends Component {

    constructor(props){
        super(props)
    }
    
    render(){
        
        return(
          <Container style={{backgroundColor:colors.bgColor}}>
            <Header style={styles.headerOverlap}>
              <Left>
              </Left>
              <Body>
                <Title style={{color:colors.formTxtColor}}>Plan</Title>
              </Body>
              <Right>
                <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
                  <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
                </Button>                     
              </Right>
            </Header>
            <Content padder>
                <Grid>
                    <Row>
                        <Col>
                            <Button onPress={()=>this.props.navigation.navigate('intro', {hideTabBar:true})} style={{width:'90%', margin:5, height:Dimensions.get('window').width*0.4, backgroundColor:colors.btnColor, flexDirection:'column', justifyContent:'center'}}>
                                <Icon name="info" type="Entypo" style={{color:'#000000', fontSize:40}}/>
                                <Text style={{color:'#000000'}}>Intro</Text>
                            </Button>
                        </Col>
                        <Col>
                            <Button onPress={()=>this.props.navigation.navigate('usp', {hideTabBar:true})} style={{width:'90%', margin:5, height:Dimensions.get('window').width*0.4, backgroundColor:colors.btnColor, flexDirection:'column', justifyContent:'center'}}>
                                <Icon name="crown" type="Foundation" style={{color:'#000000', fontSize:40}}/>
                                <Text style={{color:'#000000'}}>USP</Text>
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button onPress={()=>this.props.navigation.navigate('gallery', {hideTabBar:true})} style={{width:'90%', margin:5, height:Dimensions.get('window').width*0.4, backgroundColor:colors.btnColor, flexDirection:'column', justifyContent:'center'}}>
                                <Icon name="image" type="Entypo" style={{color:'#000000', fontSize:40}}/>
                                <Text style={{color:'#000000'}}>Gallery</Text>
                            </Button>
                        </Col>
                        <Col>
                            <Button onPress={()=>this.props.navigation.navigate('plan', {hideTabBar:true})} style={{width:'90%', margin:5, height:Dimensions.get('window').width*0.4, backgroundColor:colors.btnColor, flexDirection:'column', justifyContent:'center'}}>
                                <Icon name="floor-plan" type="MaterialCommunityIcons" style={{color:'#000000', fontSize:40}}/>
                                <Text style={{color:'#000000'}}>Plan</Text>
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button onPress={()=>this.props.navigation.navigate('location', {hideTabBar:true})} style={{width:'90%', margin:5, height:Dimensions.get('window').width*0.4, backgroundColor:colors.btnColor, flexDirection:'column', justifyContent:'center'}}>
                                <Icon name="location" type="Entypo" style={{color:'#000000', fontSize:40}}/>
                                <Text style={{color:'#000000'}}>Location</Text>
                            </Button>
                        </Col>
                        <Col>
                            <Button onPress={()=>this.props.navigation.navigate('document', {hideTabBar:true})} style={{width:'90%', margin:5, height:Dimensions.get('window').width*0.4, backgroundColor:colors.btnColor, flexDirection:'column', justifyContent:'center'}}>
                                <Icon name="pdffile1" type="AntDesign" style={{color:'#000000', fontSize:40}}/>
                                <Text style={{color:'#000000'}}>Document</Text>
                            </Button>
                        </Col>
                    </Row>
                </Grid>    
            </Content>
        </Container>
        )
    }
}
