import React, { Component } from 'react'
import { Animated, View, StyleSheet, Image, Dimensions, ScrollView, Modal, TouchableOpacity, Text } from 'react-native'
import { Container, Header, Title, Right, Left, Body, Button, Icon, Item, Content} from 'native-base'
import { styles as defaultStyle, colors } from '../globalStyle.js'
import ImageZoom from 'react-native-image-pan-zoom'
import ImageViewer from 'react-native-image-zoom-viewer'

const deviceWidth = Dimensions.get('window').width
const deviceHeight = Dimensions.get('window').height*0.8
const FIXED_BAR_WIDTH = 100
const BAR_SPACE = 10

const images = [
  require('../image/intro/intro.png')
]
const i=0;

export default class App extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            imageViewer:false,
            page:0,
            disableLeft:false,
            disableRight:false
        }
    }

  render() {
    const {goBack} = this.props.navigation;
    return (
        <Container style={{backgroundColor:colors.bgColor}}>
            <Header style={defaultStyle.headerOverlap}>
              <Left>
                <TouchableOpacity onPress={() => goBack()}>
                    <Icon name="back" type="Entypo" />
                </TouchableOpacity>                  
              </Left>
              <Body>
                <Title style={{color:colors.formTxtColor}}>Intro</Title>
              </Body>
              <Right>
                <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
                  <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
                </Button>                     
              </Right>
            </Header>
            <Content bounces={false} showsVerticalScollIndicator={false}>
                <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>   
                    <ImageZoom 
                               cropWidth={Dimensions.get('window').width*0.95}
                               cropHeight={Dimensions.get('window').height*0.9}
                               imageWidth={Dimensions.get('window').width*0.95}
                               imageHeight={Dimensions.get('window').height*0.9}
                               maxScale={3}
                               minScale={1}
                               style={{backgroundColor:'transparent', alignSelf:'center'}}
                    >                
                        <Image     
                            style={{ 
                                    width: '100%',
                                    height: '100%',
                                    resizeMode: 'contain',
                                    }}
                            source={images[this.state.page]}
                        />  
                    </ImageZoom>
                    {/*<Item style={{alignSelf:'center', borderColor:'transparent'}}>
                        {this.state.page == 0?<View/>:
                        <Button transparent disabled={this.state.disableLeft} 
                            onPress={()=>this.state.page<=0? 
                            this.setState({disableLeft:true, disableRight:false})
                            :this.setState((prev)=>({page:prev.page-1, disableRight:false}))}
                            >
                            <Icon name='doubleleft' type='AntDesign' style={{color:colors.btnColor}}/>
                        </Button>}
                        {this.state.page==2?<View/>:<Button transparent disabled={this.state.disableRight} 
                            onPress={()=>this.state.page>=2?
                                this.setState({disableRight:true, disableLeft:false})
                                :this.setState((prev)=>({page:prev.page+1, disableLeft:false}))
                            }
                            >
                            <Icon name='doubleright' type='AntDesign' style={{color:colors.btnColor}}/>
                        </Button>}
                    </Item>*/}
                </View>
            </Content>
        </Container>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  barContainer: {
    position: 'absolute',
    zIndex: 2,
    top: 20,
    flexDirection: 'row',
  },
  track: {
    backgroundColor: '#ccc',
    overflow: 'hidden',
    height: 2,
  },
  bar: {
    backgroundColor: '#5294d6',
    height: 2,
    position: 'absolute',
    left: 0,
    top: 0,
  },
})