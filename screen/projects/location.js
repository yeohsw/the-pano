import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { Image, View, Dimensions, TouchableOpacity, Modal, Text } from 'react-native'
import { styles, colors } from '../globalStyle.js'
import ImageViewer from 'react-native-image-zoom-viewer'
import ImageZoom from 'react-native-image-pan-zoom'
export default class HeaderIconExample extends Component {
    constructor(props){
        super(props)
        this.state = {
            imageViewer:false,
            image:[{props:{source:require('../image/location/location.png')}}]
        }
    }
  render() {
      const {goBack} = this.props.navigation;
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            <TouchableOpacity onPress={() => goBack()}>
                <Icon name="back" type="Entypo" />
            </TouchableOpacity>                  
          </Left>
          <Body>
            <Title style={{color:colors.formTxtColor}}>Location</Title>
          </Body>
          <Right>
            <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
              <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
            </Button>                     
          </Right>
        </Header>
        <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>
            <ImageZoom 
                       cropWidth={Dimensions.get('window').width*0.95}
                       cropHeight={Dimensions.get('window').height*0.8}
                       imageWidth={Dimensions.get('window').width*0.95}
                       imageHeight={Dimensions.get('window').height*0.8}
                       maxScale={3}
                       minScale={1}
                       style={{backgroundColor:'transparent', alignSelf:'center'}}
            >    
                <Image     
                    style={{ 
                            width: '100%',
                            height: '100%',
                            resizeMode: 'contain'
                            }}
                    source={require('../image/location/location.png')}
                />
            </ImageZoom>
        </View>
        <Modal visible={this.state.imageViewer} transparent={false} style={{backgroundColor:colors.bgColor}} onRequestClose={() => {}}>
            <ImageViewer enablePreload={true} enableImageZoom={true} style={{backgroundColor:'#ffffff'}} imageUrls={this.state.image}/>
            <Button style={[styles.defaultBtn,{width:'100%'}]} block onPress={()=>{this.setState({imageViewer:false})}}><Text>Back</Text></Button>
        </Modal>
      </Container>
    );
  }
}