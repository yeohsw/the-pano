import React from 'react';
import { StyleSheet, Dimensions, View, TouchableOpacity, Platform} from 'react-native';
import {Container, Header, Left, Icon, Body, Title, Right, Button} from 'native-base'
import { styles as defaultStyle, colors } from '../globalStyle.js'

import Pdf from 'react-native-pdf';
 
export default class PDFExample extends React.Component {
    render() {
        let source;
        Platform.OS === 'ios'?
        source = require("./document/saleskit.pdf"):
        source = {uri:"bundle-assets://pdf/saleskit.pdf", cache:true}
        //const source = require('./test.pdf');  // ios only
        //const source = {uri:'bundle-assets://test.pdf'};
 
        //const source = {uri:'file:///sdcard/test.pdf'};
        //const source = {uri:"data:application/pdf;base64,..."};
        const {goBack} = this.props.navigation;
        return (
            <Container style={{backgroundColor:colors.bgColor}}>
                <Header style={defaultStyle.headerOverlap}>
                  <Left>
                    <TouchableOpacity onPress={() => goBack()}>
                        <Icon name="back" type="Entypo" />
                    </TouchableOpacity>                  
                  </Left>
                  <Body>
                    <Title style={{color:colors.formTxtColor}}>PDF</Title>
                  </Body>
                  <Right>
                    <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
                      <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
                    </Button>                     
                  </Right>
                </Header>                
                <View style={styles.container}>     
                    <Pdf
                        enablePaging={true}
                        maxScale={5}
                        source={source}
                        onLoadComplete={(numberOfPages,filePath)=>{
                            console.log(`number of pages: ${numberOfPages}`);
                        }}
                        onPageChanged={(page,numberOfPages)=>{
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error)=>{
                            console.log(error);
                        }}
                        style={[styles.pdf,{backgroundColor:colors.bgColor}]}/>
                </View>
            </Container>
        )
  }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    }
});