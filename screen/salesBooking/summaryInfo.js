import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Title, Icon, Button, Item, Label, Form, Input, Textarea, Picker, Badge } from "native-base";
import { TouchableOpacity, Alert, FlatList, Dimensions, View, ActivityIndicator, Image, Platform} from 'react-native'
import {styles, colors} from '../globalStyle.js'

import {api_getUnitSalesDetailForEdit, api_submitReservation, api_getUnitSaleForEdit, api_updatePartySignature, api_updateIC, api_uploadImage} from '../config.js'
import {connect} from 'react-redux'
import Moment from 'moment'
import SignatureCapture from 'react-native-signature-capture';
import ImagePicker from 'react-native-image-picker'

class summaryPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            uploadLoading:false,
            salePartyDetail:[],
            unitSale:[],
            phaseName:'',
            salesId:[],
            agentList:[],
            correspondAddress:'',
            principleAgent:'',
            coAgent:[],
            signatureUri:{},
            signatureData:'',
            icFrontUri:{},
            icFrontData:'',
            icBackUri:{},
            icBackData:'',
            uploadSignature:false
            
        }
    }
    
    componentDidMount(){
        let {navigation} = this.props;
        this.setState({
            salesId:navigation.getParam('salesId',[])
        })
        
        this.postRequestGetUnitSalesForEdit()
    }
    
    componentWillUnmount(){
        this.state ={}
    }
    
    proceed(){
        const {navigate} = this.props.navigation;
        navigate('salesPayment', {salesId:this.state.salesId, submitPayment:true})
    }
    
    validate(){
        let msg = '';
//        this.state.signatureData == ''? msg='Please upload signature before proceed.':''
        
        if(msg != ''){
            Alert.alert(
                'Info Incomplete',
                msg,
                [
                    {text:'Back', onPress:()=> console.log('cancel'), style:'cancel'}
                ],
                {cancelable:false}
            );
        }
        else{
            Alert.alert(
                'Information Saved',
                'You may review back all this informations in sale listing page',
                [
                    {text:'Noted', onPress:()=> this.proceed(), style:'cancel'}
                ],
                {cancelable:false}
            )
//            this.postRequestSubmitUnitSale();
        }
    }
    
    postRequestGetUnitSalesForEdit(){
        let {navigation} = this.props;
        this.setState({isLoading:true});
        return fetch(api_getUnitSalesDetailForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: navigation.getParam('salesId',[])
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;

                if(result != null){
                    let salesParties = result.unitSales;
                                                                                                    
                    this.setState({salePartyDetail:salesParties[0].parties})
                    let address = salesParties[0].correspondenceAddress;
                    this.setState({
                        correspondAddress: address.street+', '+address.city+', '+address.state+', '+address.postcode+', '+address.countryName
                    })
                     
                    this.setState({
                        unitSale:salesParties,
                        phaseName:result.phaseName,
                        agentList:result.agents,
                    })
                    
                    this.postRequestGetUnitSale();
                }  
                else{
                    alert(JSON.stringify(responseJson))
                }
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
//            console.error(error);
          });          
    } 
    
    postRequestUploadImage(image, front){
        
        this.setState({uploadLoading:true})
        
        const formdata = new FormData();
        formdata.append('file', {uri: Platform.OS === "android" ? image.uri : image.uri.replace("file://", ""), type: image.type, name: 'image1.jpeg'});
        formdata.append('objectType', 'NationalId');

        return fetch(api_uploadImage, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            Authorization: 'bearer '+this.props.token
          },            
          body:formdata
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
     
                if(result != null){
                    this.postRequestUpdateImage(result.fileName, front)
                                        
                }    
                else{
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    alert('Upload photo failed. Please try again.') 
                    front?
                    this.setState({icFrontData:''}):this.setState({icBackData:''})                  
                }

          })
          .catch((error) =>{
            this.setState({uploadLoading:false})
//            alert(error)
//            console.error(error);
          });          
    }    
    
    postRequestUpdateImage(id, front){
        
        let partyId = this.state.salePartyDetail[0];
        
        return fetch(api_updateIC, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              originalFileName: "image1.jpeg",
              fileName: id,
              unitSaleIds: this.state.salesId,
              unitSalePartyId: partyId.id,
              isFront: front
            })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({uploadLoading:false})
                let result = responseJson.success;
                if(result == true){
                    Alert.alert(
                        'Successfully Uploaded Photo',
                        'You may click the upload button again if you had uploaded the wrong photo.',
                        [
                            {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                        ],
                        {cancelable:false}
                    )
                }    
                else{
//                    alert(JSON.stringify(this.state.salePartyDetail))
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    alert('Upload photo failed. Please try again.') 
                    front?
                    this.setState({icFrontData:''}):this.setState({icBackData:''})                     
                } 
                

          })
          .catch((error) =>{
            this.setState({uploadLoading:false})
//            console.error(error);
          });          
    } 
    
    postRequestGetUnitSale(){
        return fetch(api_getUnitSaleForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id: this.state.salesId[0]
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;
                let section = result.sections;
                if(result != null){
                    let agentName = [];
                    
                    if(result.coSellerUserIds.length>0){ 
                        agentName = this.state.agentList.filter((x)=> result.coSellerUserIds.filter((y)=> y == x.value).length>0);
                        agentName = agentName.map((x)=>x.name);
                    }
                    this.setState({
                        sectionList:section[0] == undefined?[]:section[0].formAnswers,
                        principleAgent: this.state.agentList.filter((x)=> x.value == result.mainSellerUserId).map((x)=> x.name),
                        coAgent: agentName,
                        isLoading:false
                    })
                }    
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
//            console.error(error);
          });          
    }     
    
    postRequestSubmitUnitSale(){
        this.setState({isLoading:true});
        return fetch(api_submitReservation, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: this.state.salesId,
              additionalBankerBranchIds: []
            })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.success;
     
                if(result == true){
                    this.props.setRefresh(true);
                    this.props.setRefreshSaleList(true);
                    this.setState({isLoading:false})
                    Alert.alert(
                        'Sales Submitted',
                        'You may review back all this informations in sale listing page',
                        [
                            {text:'Noted', onPress:()=> this.proceed(), style:'cancel'}
                        ],
                        {cancelable:false}
                    )
                   
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
//            console.error(error);
          });          
    }  
    
    postRequestUpdatePartySignature(data){
        let partyId = this.state.salePartyDetail[0];
        this.setState({uploadLoading:true})
        return fetch(api_updatePartySignature, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: this.state.salesId,
              unitSalePartyId: partyId.id,
              dataUrl: "data:image/png;base64,"+data
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({uploadLoading:false})
                let result = responseJson.success;
                
                if(result == true){
                    Alert.alert(
                        'Successfully Uploaded Photo',
                        'You may click the upload button again if you had uploaded the wrong photo.',
                        [
                            {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                        ],
                        {cancelable:false}
                    )
                }    
                else{
//                    alert(JSON.stringify(this.state.salePartyDetail))
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    this.setState({signatureData:''})
                    alert('Upload signature failed. Please try again.')                    
                }

          })
          .catch((error) =>{
            this.setState({uploadLoading:false})
//            console.error(error);
          });          
    }    
      
    uploadSignatureImage(){
         ImagePicker.showImagePicker({
              title: 'Select Upload Option',
              takePhotoButtonTitle:'Take Photo',
              chooseFromLibraryButtonTitle:'Upload From Gallery',
              maxWidth:1000,
              maxHeight:1000,
              quality:1,
//              storageOptions: {
//                skipBackup: true,
//                path: 'images',
//              },
          }, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            const source = { uri: response.uri };

            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };

            this.setState({
              signatureUri: source,
              signatureData: response.data
            });
            this.postRequestUpdatePartySignature(response.data);
          }
        });        
    }
    
    uploadIcImage(isFront){
         ImagePicker.showImagePicker({
              title: 'Select Upload Option',
              takePhotoButtonTitle:'Take Photo',
              chooseFromLibraryButtonTitle:'Upload From Gallery',
              maxWidth:1000,
              maxHeight:1000,
              quality:1,
//              storageOptions: {
//                skipBackup: true,
//                path: 'images',
//              },
          }, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            const source = { uri: response.uri };

            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            !isFront?
            this.setState({
              icBackUri: source,
              icBackData: response.data
            }):
            this.setState({
              icFrontUri: source,
              icFrontData: response.data
            })
            this.postRequestUploadImage(response,isFront);
          }
        });         
    }
    
    saveSign() {
        this.refs["sign"].saveImage();
    }

    resetSign() {
        this.refs["sign"].resetImage();
    }

    _onSaveEvent(result, tenancy, id) {
        //result.encoded - for the base64 encoded png
        //result.pathName - for the file path name
        this.setState({
          signatureUri: {uri:'data:image/jpeg;base64,'+result.encoded},
          signatureData: result.encoded,
          uploadSignature:false
        });
        
        this.postRequestUpdatePartySignature(result.encoded);
      
    }
    _onDragEvent() {
         // This callback will be called when the user enters signature
//        console.log("dragged");
    }
        
    render(){
        const {goBack} = this.props.navigation;
        
        if(this.state.isLoading){
            return ( 
                <Container style={{backgroundColor:colors.bgColor}}>
                  <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                    <ActivityIndicator size="large" color="#ffffff" />
                    <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
                  </View>
                </Container>
            )
        }           
        
        return (
            <Container style={{backgroundColor:colors.bgColor}}>
                <Header style={styles.headerOverlap}>
                  <Left>
                    <TouchableOpacity onPress={() => goBack()}>
                        <Icon name="back" type="Entypo" />
                    </TouchableOpacity>
                  </Left>
                  <Body>
                    <Text style={{color:colors.formTxtColor, textAlign:'center'}}>Summary Review</Text>
                  </Body>
                  <Right>                     
                  </Right>
                </Header>
                {
                   this.state.uploadLoading == true?

                          <View style={{ width:Dimensions.get('window').width, height:Dimensions.get('window').height, zIndex:2, position:'absolute', paddingTop:'50%', alignSelf:'center', backgroundColor:'#ffffff', opacity:0.8}}>
                            <ActivityIndicator size="large" color="#000000" />
                            <Text style={{textAlign:'center', flex:1, color:'#000000'}}>Please Wait...</Text>
                          </View>
                      :<View />
                }                    
                <Content padder>
                    <Card>
                        <FlatList extraData={this.state.unitSale} bounces={false} data={this.state.unitSale}
                            renderItem={({item, index}) =>(
                                <CardItem style={{flexDirection:'column', flexWrap:'wrap'}}>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent'}}>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>Date</Text>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>{Moment(new Date()).format('D/M/YYYY')}</Text>
                                    </Item>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent', backgroundColor:colors.lightGray}}>
                                        <Text style={{width:'50%', justifyContent:'flex-start', flexDirection:'column', alignSelf:'flex-start'}}>Project</Text>
                                        <Text style={{width:'50%', justifyContent:'flex-start', flexWrap:'wrap'}}>{this.state.phaseName}</Text>
                                    </Item>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent'}}>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>Property</Text>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.unitNumber}</Text>
                                    </Item>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent', backgroundColor:colors.lightGray}}>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>Purchase Price</Text>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>RM {(item.spaPrice.toFixed(2)).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                                    </Item>
                                </CardItem>
                            )}
                            keyExtractor={(item, index) => 'unit' +index}
                        />
                    </Card> 
                    <FlatList extraData={this.state.salePartyDetail} bounces={false} data={this.state.salePartyDetail}
                        renderItem={({item, index}) =>(
                            <Card>
                                <CardItem header>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Owner {index+1}</Text>
                                </CardItem>
                                <CardItem style={{backgroundColor:colors.lightGray}}>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Title</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.title}</Text>
                                </CardItem>
                                <CardItem>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Name</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.fullName}</Text>
                                </CardItem>
                                <CardItem style={{backgroundColor:colors.lightGray}}>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Nationality</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.nationalityName}</Text>
                                </CardItem>
                                <CardItem>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>IC/Passport no.</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.nationalId}</Text>
                                </CardItem>
                                <CardItem style={{backgroundColor:colors.lightGray}}>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Birthdate</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{Moment(new Date(item.birthDate)).format('D/M/YYYY')}</Text>
                                </CardItem>
                                <CardItem>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Gender</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.gender == 1?"Male":item.gender == 2?"Female":"Unknown"}</Text>
                                </CardItem>
                                <CardItem style={{backgroundColor:colors.lightGray}}>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Race</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.raceName}</Text>
                                </CardItem>
                                <CardItem>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Email address</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.emailAddress}</Text>
                                </CardItem>
                                <CardItem style={{backgroundColor:colors.lightGray}}>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Relationship</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.relationship}</Text>
                                </CardItem>
                                <CardItem>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>Remark</Text>
                                    <Text style={{width:'50%', justifyContent:'flex-start'}}>{item.remarks}</Text>
                                </CardItem>
                            </Card>                        
                        )}
                        keyExtractor={(item, index) => 'purchaser' +index}
                    />                     
                    <Card>
                        <CardItem header>
                            <Text>Other Information</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'50%', justifyContent:'flex-start'}}>Principle Agent</Text>
                            <Text style={{width:'50%', justifyContent:'flex-start'}}>{this.state.principleAgent}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{width:'50%', justifyContent:'flex-start'}}>Co-Agents</Text>
                            <Text style={{width:'50%', justifyContent:'flex-start'}}>{JSON.stringify(this.state.coAgent).replace(/[\[\]"]+/g, '')}</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'50%', justifyContent:'flex-start', flexDirection:'column', alignSelf:'flex-start'}}>Correspondence Address</Text>
                            <Text style={{width:'50%', justifyContent:'flex-start'}}>{this.state.correspondAddress}</Text>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem style={{flexDirection:'column', justifyContent:'flex-start'}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>NRIC (Front)</Label>
                            {this.state.icFrontData !=''?
                                <Image source={this.state.icFrontUri} style={{width:150, height:150, marginTop:10, marginBottom:10}} />:<View/>
                                
                            }
                            <Button onPress={()=>this.uploadIcImage(true)} style={{alignSelf:'center'}}><Text>Upload NRIC Photo</Text></Button>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem style={{flexDirection:'column', justifyContent:'flex-start'}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>NRIC (Back)</Label>
                            {this.state.icBackData !=''?
                                <Image source={this.state.icBackUri} style={{width:150, height:150, marginTop:10, marginBottom:10}} />:<View/>
                            }
                            <Button onPress={()=>this.uploadIcImage(false)} style={{alignSelf:'center'}}><Text>Upload NRIC Photo</Text></Button>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem style={{flexDirection:'column', justifyContent:'flex-start'}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Signature</Label>
                            {this.state.signatureData !=''?
                                <Image source={this.state.signatureUri} style={{width:200, height:200, marginTop:10, marginBottom:10}} />:<View/>
                                
                            }
                            <Button onPress={()=>this.setState({uploadSignature:true})} style={{alignSelf:'center'}}><Text>Add Signature</Text></Button>
                        </CardItem>
                    </Card>
                    <Button style={styles.defaultBtn} onPress={()=>this.validate()}><Text style={{color:colors.btnTxtColor}}>Submit</Text></Button>
                </Content>
               {this.state.uploadSignature?
                <View style={{flex:1, backgroundColor:'#efefef', borderWidth:1, borderColor:'#efefef', top:'18%', bottom:"20%", left:'10%', right:'10%', flexDirection:'column', justifyContent:'center', position:'absolute'}}>
 
                    <Text style={{alignSelf:"center",justifyContent:"center", padding:'2%'}}>Please Sign For Confirmation</Text>
                  
                    <SignatureCapture
                        style={{flex: 1, borderColor: '#000', borderWidth: 1}}
                        ref="sign"
                        onSaveEvent={(result)=>this._onSaveEvent(result, this.props.tenant, this.state.handoverId)}
                        onDragEvent={this._onDragEvent}
                        saveImageFileInExtStorage={false}
                        showNativeButtons={false}
                        showTitleLabel={false}
                        viewMode={"portrait"}/>

                      <Item>
                          <Button onPress={() => { this.saveSign() }} transparent style={{width:'100%', justifyContent:'center'}}>
                             <Text>Submit</Text>
                          </Button>
                      </Item>
                      <Item>
                          <Button onPress={() => { this.resetSign() }} transparent style={{width:'100%', justifyContent:'center'}}>
                             <Text>Reset</Text>
                          </Button>
                      </Item>
                      <Item>
                          <Button onPress={()=> this.setState({uploadSignature:false})} transparent style={{width:'100%', justifyContent:'center'}}>
                             <Text>Cancel</Text>
                          </Button>
                      </Item>
                </View>:<View/>}                     
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token,        
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setRefresh: (tk) => dispatch({type: 'SET_REFRESH_UNIT', param:tk}),
        setRefreshSaleList: (id) => dispatch({type: 'SET_REFRESH_SALESLIST', param:id})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(summaryPage);