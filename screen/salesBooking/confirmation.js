import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Title, Icon, Button } from "native-base";
import { TouchableOpacity, FlatList, Image, Dimensions, Modal, View, ActivityIndicator} from 'react-native'
import {styles, colors} from '../globalStyle.js'
import ImageViewer from 'react-native-image-zoom-viewer'
import ImageZoom from 'react-native-image-pan-zoom'

import {api_getUnitTypeImage, getUnitTypePlan, phaseId} from '../config.js'
import {connect} from 'react-redux'
class confirmationPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            selectedUnit:[],
            zoomImg:[],
            imageViewer:false,
            disableScroll:false,
            unitTypeImage:[],
            refreshFlat:false
        }
    }
    
    componentDidMount(){
        const {navigation} = this.props;
        this.setState({
            selectedUnit:navigation.getParam('selected',[])
        })
        
        this.postRequestGetUnitTypeImage()       
    }
    
    
    postRequestGetUnitTypeImage(){
                    
        return fetch(api_getUnitTypeImage, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              phaseId:phaseId,
              maxResultCount:100000,
              skipCount:0       
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
        
                let result = responseJson.result;
                if(result.items.length > 0){
                   this.setState({unitTypeImage:result.items, refreshFlat:!this.state.refreshFlat})
                   
                }
                else{                          
                    alert('No unit type image found, kindly ensure the setup is complete. If this issue persist, please request assist from administrator.')
                }
            
            
          })
          .catch((error) =>{
               this.setState({
                    isLoading: false,
                });
               alert("Fail to retrieve unit type image, please check your internet connection and try again. If this issue persist, kindly contact the administrator.")
          });        
    }     
    
    proceed(){
        const {navigate} = this.props.navigation;
        navigate('pricing',{hideTabBar:true, selected:this.state.selectedUnit});
    }

    
    zoomImage(uri){

        let arr = []
        arr.push({
              url:uri,
              width:Dimensions.get('window').width,
              height:Dimensions.get('window').height,
              props:{
                  onError:()=>alert('Image fail to retrieve, please check your internet connection and try again later.'),
//                  source:uri,
                  resizeMode: 'contain',
              }
          })    
        
        this.setState({imageViewer:true, zoomImg:arr})
      
    }
    
    removeUnit(value){
        let unit = this.state.selectedUnit.filter((x)=>x.unitNumber != value);
        this.setState({
            selectedUnit:unit
        })
        const {goBack} = this.props.navigation;
        if(unit.length < 1){
            goBack();
        }
    }
    
  render() {
    const {goBack} = this.props.navigation
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            <TouchableOpacity onPress={() => goBack()}>
                <Icon name="back" type="Entypo" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={{color:colors.formTxtColor}}>Pricing</Title>
          </Body>
          <Right>                 
          </Right>
        </Header>
        <Content padder>
            <Modal visible={this.state.imageViewer} transparent={false} onRequestClose={() => {}}>
                 <ImageViewer renderIndicator={()=><View/>} renderImage={(props) => <Image {...props} />} loadingRender={()=><ActivityIndicator size="large" color="#ffffff" />} enablePreload={true} imageUrls={this.state.zoomImg} backgroundColor={'#fff'}/>
                <Button style={[styles.defaultBtn,{width:'100%'}]} block onPress={()=>{this.setState({imageViewer:false})}}><Text>Back</Text></Button>
            </Modal>              
            <FlatList extraData={this.state.refreshFlat} scrollEnabled={!this.state.disableScroll} bounces={false} data={this.state.selectedUnit}
                renderItem={({item, index}) =>(
                            <Card>
                                <CardItem header bordered>
                                  <Text style={{width:'90%'}}>{item.unitNumber}</Text>
                                  <TouchableOpacity onPress={this.removeUnit.bind(this, item.unitNumber)} >
                                    <Icon name='cancel' type='MaterialIcons' style={{fontSize:25}}/>
                                  </TouchableOpacity>
                                </CardItem>
                                <CardItem bordered>           
                                    <Text style={{width:'50%', flexDirection:'row'}}>
                                      Floor
                                    </Text>
                                    <Text>: {item.floor}</Text>  
                                </CardItem>
                                <CardItem bordered>           
                                    <Text style={{width:'50%', flexDirection:'row'}}>
                                      Floor Position
                                    </Text>
                                    <Text>: {item.floorPosition}</Text>  
                                </CardItem>
                                <CardItem bordered>          
                                    <Text style={{width:'50%', flexDirection:'row'}}>
                                      Size
                                    </Text>
                                    <Text>: {item.sizeSqf} ft{'\u00B2'}</Text>  
                                </CardItem>
                                <CardItem bordered>         
                                    <Text style={{width:'50%', flexDirection:'row'}}>
                                      Type
                                    </Text>
                                    <Text>: {item.unitType}</Text>          
                                </CardItem>
                                <CardItem bordered>         
                                    <Text style={{width:'50%', flexDirection:'row'}}>
                                      View
                                    </Text>
                                    <Text>: {item.unitView}</Text>          
                                </CardItem>
                                <CardItem bordered>         
                                    <Text style={{width:'50%', flexDirection:'row'}}>
                                      List Price
                                    </Text>
                                    <Text>: RM {(item.listPrice.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>          
                                </CardItem>
                                {this.state.unitTypeImage.filter((x)=>x.unitType == item.unitTypePlanId)[0] != undefined?
                                <CardItem bordered style={{flexDirection:'column', justifyContent:'flex-start'}}>         
                                    <Text style={{alignSelf:'flex-start', flex:1}}>
                                      Layout Plan
                                    </Text>
                                    <TouchableOpacity onPress={()=> this.zoomImage(getUnitTypePlan+this.state.unitTypeImage.filter((x)=>x.unitType == item.unitTypePlanId)[0].planImageId)} style={{width:Dimensions.get('window').width*0.9}}>
                                        <Image     
                                            style={{ 
                                                    width: '100%', 
                                                    height: 300, 
                                                    resizeMode: 'contain',
                                                    }}
                                            source={{uri:getUnitTypePlan+this.state.unitTypeImage.filter((x)=>x.unitType == item.unitTypePlanId)[0].planImageId}}
                                        />  
                                    </TouchableOpacity>
                                </CardItem>:<View/>}

                            </Card>
                        )} 
                keyExtractor={(item, index) => ''+index}
            />
        </Content>
        <Button onPress={this.proceed.bind(this)} style={[styles.defaultBtn, {margin:10}]}>
            <Text style={{color:colors.btnTxtColor}}>Confirm</Text>
        </Button>
      </Container>
    );
  }
}


function mapStateToProps(state) {
    return {
        token: state.token, 
        refresh: state.refreshUnit
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setRefresh: (tk) => dispatch({type: 'SET_REFRESH_UNIT', param:tk}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(confirmationPage);