import React, { Component } from 'react';
import { Container, Button, Item, Card, CardItem , DatePicker, Icon, Input, Content, Picker, Header, Left, Right, Body, Title} from 'native-base';
import { Text, View, FlatList, TouchableOpacity, Dimensions, Image, ScrollView, Alert, Platform, PanResponder} from 'react-native';

import {styles,fonts, colors} from '../globalStyle.js'
import {api_getUnitStatusStyle, api_getBlocks, api_getUnitTypes, api_getUnitView, api_getUnitSalesLayout, api_getUnits, phaseId, blockAId, blockBId, blockCId, blockDId} from '../config.js'

//import LinearGradient from 'react-native-linear-gradient';

import {connect} from 'react-redux';
import Moment from 'moment';
import ImageZoom from 'react-native-image-pan-zoom';
let refreshTime = true

let scrollX = 0
let scrollY = 0
class salesDashboard extends Component {

//Available = 0,
//Blocked = 1,
//Reserved = 2,
//Sold = 3,
//ConfirmedSold = 4,
//SpaSigned = 5    
    
    constructor(props){
        super(props)

        this.state = {
            isLoading:false,
            blocks:[],
            unitStatusStyles:[],
            unitType:[],
            unitView:[],
            unitSalesLayout:[],
            selectedBlockId:0,
            statusStyle:[],
            selectedStyle:{},
            showFilter:false,
            unitW:70,
            unitH:40,
            zoom:0.6,
            unitTxtSize:10,
            refreshDashBoard:false,
            filteredUnitLayout:[],
            selectedUnitId:[],
            currentTime:Moment(),
            unitDetail:[],
            showModel:false,
            selectedSummary:{},
            enableZoom:false,
            noPermission:false,
        }
        
//        this._panResponder = PanResponder.create({
//          onStartShouldSetPanResponder: (evt, gestureState) => gestureState.numberActiveTouches >1?true:false,
////          onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
////          onMoveShouldSetPanResponder: (evt, gestureState) => true,
////          onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,            
//          onPanResponderMove: (e, { dy, dx, moveX}) => {
//            
//            return alert(JSON.stringify(dy))
//          },
//          onMoveShouldSetPanResponder: (ev, { dx }) => {
//            
////            return alert(JSON.stringify(dx))
//          },
//          onPanResponderGrant: (ev, { dy, dx }) => {
////              if (ev.nativeEvent.changedTouches.length <= 1) {
////                  alert('haha')
////              }
////              else{
//                  
////              }
////            return alert(JSON.stringify(dy))
//          },
//          onPanResponderRelease: (x, {dx}) => {
////            return alert(JSON.stringify(dx))
//          },
//        })        
    }

    componentDidMount(){
        this.postRequestGetBlock()
    }
    
    componentDidUpdate(){

        if(Moment(this.state.currentTime).diff(Moment(new Date()), 'minutes') <= -10 && refreshTime){
            refreshTime = false
            this.postRequestGetUnitSalesLayout(this.state.selectedBlockId)
            
        }
    }
    
    postRequestGetBlock(){
              
        this.setState({isLoading: true});        
        return fetch(api_getBlocks, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            isActive: true
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {
            
                let result = responseJson.result;
                
                if(result.items.length > 0){
                    this.setState({blocks:result.items.filter((x)=>x.value == blockAId || x.value == blockBId || x.value == blockCId || x.value == blockDId), selectedBlockId:result.items.filter((x)=>x.phaseName == this.props.phaseName )[0].value})
//                    this.setState({blocks:result.items})
                    this.postRequestGetUnitStatusStyle()
                }
                else{
                    this.setState({
                        isLoading: false,
                    });                    
                    alert('No blocks found, kindly ensure the setup is complete. If this issue persist, please request assist from administrator.')
                }
          })
          .catch((error) =>{
               this.setState({
                    isLoading: false,
                });
//               alert("Fail to retrieve blocks, please check your internet connection and try again. If this issue persist, kindly contact the administrator.")
//            alert(JSON.stringify(this.props.phaseName))
          });        
    }   
    
    postRequestGetUnitStatusStyle(){
              
        return fetch(api_getUnitStatusStyle, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body:JSON.stringify({
            forSaleDashboard: true              
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {
            
                let result = responseJson.result;

                if(result != null){
//                    Available = 0,
//                    Blocked = 1,
//                    Reserved = 2,
//                    Sold = 3,
//                    ConfirmedSold = 4,
//                    SpaSigned = 5                              
                    this.setState({statusStyle:[
                                        {text:result.available.text, bgColor:result.available.backgroundColor, txtColor:result.available.textColor},
                                        {text:result.blocked.text, bgColor:result.blocked.backgroundColor, txtColor:result.blocked.textColor},
                                        {text:result.reserved.text, bgColor:result.reserved.backgroundColor, txtColor:result.reserved.textColor},
                                        {text:result.sold.text, bgColor:result.sold.backgroundColor, txtColor:result.sold.textColor},
                                        {text:result.confirmedSold.text, bgColor:result.confirmedSold.backgroundColor, txtColor:result.confirmedSold.textColor},
                                        {text:result.spaSigned.text, bgColor:result.spaSigned.backgroundColor, txtColor:result.spaSigned.textColor}
                                   ],
                                   selectedStyle:{text:result.selected.text, bgColor:result.selected.backgroundColor, txtColor:result.selected.textColor}
                    })
                    this.postRequestGetUnitTypes()
                }
                else{
                    this.setState({
                        isLoading: false,
                    });                    
                    alert('No unit status style found, kindly ensure the setup is complete. If this issue persist, please request assist from administrator.')
                }
          })
          .catch((error) =>{
               this.setState({
                    isLoading: false,
                });
               alert("Fail to retrieve unit status style, please check your internet connection and try again. If this issue persist, kindly contact the administrator.")
          });        
    }   
    
    postRequestGetUnitTypes(){
                      
        return fetch(api_getUnitTypes, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
             id:this.state.selectedBlockId
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {
            
                let result = responseJson.result;

                if(result.items.length > 0){
                    this.setState({unitType:result.items, noPermission:false})
                    
                }
                else{
                    this.setState({
//                        isLoading: false,
                        noPermission:true
                    });                    
//                    alert('No unit type found, kindly ensure the setup is complete. If this issue persist, please request assist from administrator.')
//                    alert('You had no permission to view this, any enquiries you may request assist from administrator.')
//                    alert(JSON.stringify(responseJson))
                }
                this.postRequestGetUnitView()
          })
          .catch((error) =>{
               this.setState({
                    isLoading: false,
                });
               alert("Fail to retrieve unit type, please check your internet connection and try again. If this issue persist, kindly contact the administrator.")
          });        
    }   
    
    postRequestGetUnitView(){
                  
        return fetch(api_getUnitView, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
             id:this.state.selectedBlockId
          })            
        })
          .then((response) => response.json())
          .then((responseJson) => {
            
                let result = responseJson.result;

                if(result.items.length > 0){
                    this.setState({unitView:result.items})

                }
            
                this.postRequestUnitDetail()            
          })
          .catch((error) =>{
               this.setState({
                    isLoading: false,
                });
               alert("Fail to retrieve unit view, please check your internet connection and try again. If this issue persist, kindly contact the administrator.")
          });        
    } 
    
    postRequestGetUnitSalesLayout(value){
        this.setState({isLoading:true, filteredUnitLayout:[], selectedUnitId:[], refreshDashBoard:!this.state.refreshDashBoard, selectedBlockId:value})
        return fetch(api_getUnitSalesLayout, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
             blockId:value
          })            
        })
          .then((response) => response.json())
          .then((responseJson) => {
            
                let result = responseJson.result;
//                alert(JSON.stringify(responseJson))
                if(responseJson.success == true && result.floors.length > 0){
                    this.setState({isLoading: false});                       
                    this.setState({unitSalesLayout:result.floors, filteredUnitLayout:result.floors, refreshDashBoard:!this.state.refreshDashBoard, currentTime:Moment(), showFilter:false})
                    refreshTime = true
//                    alert(JSON.stringify(result.floors[0]))
                }
//                else{
                    this.setState({
                        isLoading: false,
                    });   
//                    alert('You had no permission to view this, any enquiries you may request assist from administrator.')
//                    alert('No unit sales layout found, kindly ensure the setup is complete. If this issue persist, please request assist from administrator.')
//                }
                
          })
          .catch((error) =>{
               this.setState({
                    isLoading: false,
                });
               alert("Fail to retrieve unit sales layout, please check your internet connection and try again. If this issue persist, kindly contact the administrator.")
          });        
    }
    
    
    postRequestUnitDetail(){
        return fetch(api_getUnits, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
              body: JSON.stringify({
                  phaseId: phaseId,
                  blockId: null,
                  floor: [],
                  "unitType": [],
                  "unitView": [],
                  "status": null,
                  "isBumi": null,
                  "filter": "",
                  "sorting": "",
                  "skipCount": 0,
                  "maxResultCount": 2000000
              }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
               
//                this.setState({isLoading:false})
               
                
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        unitDetail:result.items
                    })
                    
                }
                this.state.noPermission?alert('You had no permission to view this, any enquiries you may request assist from administrator.'):''
                this.postRequestGetUnitSalesLayout(this.state.selectedBlockId)
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });  
    }      
    
    selectUnit(item){
//        if(item['selected'] != undefined){
//            item['selected'] = !item['selected']  
//        } 
//        else{
//            item['selected'] = true
//        }
        let layout = this.state.filteredUnitLayout
        let arr = this.state.selectedUnitId
        
        for(let i=0; i<layout.length; i++){
            let slot = layout[i].slots;
            for(let y=0; y<slot.length; y++){
                if(slot[y].unit != undefined && slot[y].unit.id == item.unit.id){
                    slot[y] = item
                    if(arr.filter((x)=>x.unit.id == item.unit.id).length >0){
                        arr = arr.filter((x)=>x.unit.id != item.unit.id)
                        this.setState({selectedUnitId:arr, showModal:false})
                        if(item['selected'] != undefined){
                            item['selected'] = !item['selected']  
                        } 
                        else{
                            item['selected'] = true
                        }                        
                    }
                    else{
//                        arr.push(item)    
                        this.setState({showModal:true, selectedSummary:item})
                    }
                    
//                    this.setState({selectedUnitId:arr, showModal:arr.filter((x)=>x.unit.id == item.unit.id).length >0?true:false, selectedSummary:item})
//                    this.setState({selectedUnitId:arr})
//                    alert(JSON.stringify(item))
                }                
            }
        }
//        this.setState({
////            refreshDashBoard:!this.state.refreshDashBoard,
//            filteredUnitLayout:layout
//        })
//        alert(JSON.stringify(item))
    }
    
    choose(item){
        if(item['selected'] != undefined){
            item['selected'] = !item['selected']  
        } 
        else{
            item['selected'] = true
        }
        let layout = this.state.filteredUnitLayout
        let arr = this.state.selectedUnitId
        
        for(let i=0; i<layout.length; i++){
            let slot = layout[i].slots;
            for(let y=0; y<slot.length; y++){
                if(slot[y].unit != undefined && slot[y].unit.id == item.unit.id){
                    slot[y] = item
                    if(arr.filter((x)=>x.unit.id == item.unit.id).length >0){
                        arr = arr.filter((x)=>x.unit.id != item.unit.id)
                        this.setState({selectedUnitId:arr, showModal:false})
                    }
                    else{
                        arr.push(item)    
                        this.setState({selectedUnitId:arr, showModal:false, selectedSummary:item})
                    }
                    
//                    this.setState({selectedUnitId:arr, showModal:arr.filter((x)=>x.unit.id == item.unit.id).length >0?true:false, selectedSummary:item})
//                    this.setState({selectedUnitId:arr})
//                    alert(JSON.stringify(item))
                }                
            }
        }
        this.setState({
            refreshDashBoard:!this.state.refreshDashBoard,
            filteredUnitLayout:layout
        })
//        alert(JSON.stringify(item))
    }
    
    
    bookNow(){
        let arr = this.state.unitDetail.filter((x)=>this.state.selectedUnitId.filter((y)=>y.unit.id == x.id).length>0)
        if(this.state.selectedUnitId.length > 0){
//            this.props.navigation.navigate('salesConfirmation', {selected:this.state.selectedUnitId})
             this.props.navigation.navigate('confirmation', {hideTabBar:true, selected:arr});
        }
        else{
            Alert.alert(
                'No unit had been selected yet',
                'Please selected a unit before proceed.',
                [
                    {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                ],
                {cancelable:false}
            )
        }
    }
    
    zoomIn(){
        this.setState({isLoading:true})
        
        setTimeout(()=>{
            this.setState({zoom:this.state.zoom+0.2, refreshDashBoard:!this.state.refreshDashBoard, isLoading:false})
        }, 500)
    }
    
    zoomOut(){
        this.setState({isLoading:true})
        
        setTimeout(()=>{
            this.setState({ zoom:this.state.zoom-0.2, refreshDashBoard:!this.state.refreshDashBoard, isLoading:false})
        }, 500)
    }

    render(){

        return(

            <Container>
                <View style={{width:Dimensions.get('window').width, height:Dimensions.get('window').height, flexDirection:'column', opacity:this.state.isLoading? 0.5:1}}>
                    <Header style={[styles.headerOverlap,{height:45, backgroundColor:'transparent'}]}>
                      <Left>
                        <Button transparent onPress={()=>this.props.navigation.navigate('prosales',{hideTabBar:true})}>
                          <Icon name='back' style={{color:'#000000', fontSize:20}} type={'Entypo'} />
                        </Button>
                      </Left>
                      <Body>
                        <Title style={{color:colors.formTxtColor}}>Booking</Title>
                      </Body>
                      <Right>

                      </Right>
                    </Header>          
                    <Content bounces={false} showsVerticalScrollIndicator={false} style={{height:Dimensions.get('window').height, paddingLeft:20, paddingRight:20, paddingTop:Platform.OS == 'ios'?20:0}}>
                        <View style={{flexDirection:'row', width:'100%', flexWrap:'wrap'}}>
                            {this.state.statusStyle[0] != undefined?
                            <Item rounded style={{height:15, backgroundColor:this.state.statusStyle[0].bgColor, paddingLeft:10, paddingRight:10, borderColor:'transparent', marginBottom:5}}>
                                <Text style={{fontSize:11, color:this.state.statusStyle[0].txtColor}}>{this.state.statusStyle[0].text}</Text>
                            </Item>:<View/>}
                            {this.state.statusStyle[1] != undefined?
                            <Item rounded style={{height:15, backgroundColor:this.state.statusStyle[1].bgColor, paddingLeft:10, paddingRight:10, borderColor:'transparent', marginBottom:5}}>
                                <Text style={{fontSize:11, color:this.state.statusStyle[1].txtColor}}>{this.state.statusStyle[1].text}</Text>
                            </Item>:<View/>}
                            {this.state.statusStyle[2] != undefined?
                            <Item rounded style={{height:15, backgroundColor:this.state.statusStyle[2].bgColor, paddingLeft:10, paddingRight:10, borderColor:'transparent', marginBottom:5}}>
                                <Text style={{fontSize:11, color:this.state.statusStyle[2].txtColor}}>{this.state.statusStyle[2].text}</Text>
                            </Item>:<View/>}
                            {this.state.statusStyle[3] != undefined?
                            <Item rounded style={{height:15, backgroundColor:this.state.statusStyle[3].bgColor, paddingLeft:10, paddingRight:10, borderColor:'transparent', marginBottom:5}}>
                                <Text style={{fontSize:11, color:this.state.statusStyle[3].txtColor}}>{this.state.statusStyle[3].text}</Text>
                            </Item>:<View/>}
                            {this.state.statusStyle[4] != undefined?
                            <Item rounded style={{height:15, backgroundColor:this.state.statusStyle[4].bgColor, paddingLeft:10, paddingRight:10, borderColor:'transparent', marginBottom:5}}>
                                <Text style={{fontSize:11, color:this.state.statusStyle[4].txtColor}}>{this.state.statusStyle[4].text}</Text>
                            </Item>:<View/>}
                            {this.state.statusStyle[5] != undefined?
                            <Item rounded style={{height:15, backgroundColor:this.state.statusStyle[5].bgColor, paddingLeft:10, paddingRight:10}}>
                                <Text style={{fontSize:11, color:this.state.statusStyle[5].txtColor}}>{this.state.statusStyle[5].text}</Text>
                            </Item>:<View/>}
                            {this.state.selectedStyle.txtColor != undefined?
                            <Item rounded style={{height:15, backgroundColor:this.state.selectedStyle.bgColor, paddingLeft:10, paddingRight:10, borderColor:'transparent', marginBottom:5}}>
                                <Text style={{fontSize:11, color:this.state.selectedStyle.txtColor}}>{this.state.selectedStyle.text}</Text>
                            </Item>:<View/>}
                            <TouchableOpacity onPress={()=>this.postRequestGetUnitSalesLayout(this.state.selectedBlockId)} style={{width:50, flexDirection:'row', justifyContent:'center'}}>
                                <Icon name="ios-refresh" type={'Ionicons'} style={{fontSize:15}}/>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Item style={{borderColor:'transparent'}}>
                                <Text style={{fontSize:fonts.contentFont}}>{Moment().format('dddd, DD MMMM, YYYY, h:mmA')}</Text>
                            </Item>
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'flex-end'}}>
                            <View style={{flexDirection:'row', width:'100%', justifyContent:'flex-end'}}>
                                {/*this.state.zoom >=1.6?<View style={{width:0}}/>:*/}
                                <TouchableOpacity onPressIn={()=>{this.imageZoom.reset()}} style={{backgroundColor:colors.btnColor, borderRadius:5, padding:3}}>
                                    {/*<Icon name="zoom-in" type={'MaterialIcons'} style={{fontSize:25}} />*/}
                                    <Text style={{fontSize:14}}>Show All</Text>
                                </TouchableOpacity>
                                {/*this.state.zoom<= 0.4?<View style={{width:0}}/>:
                                <TouchableOpacity onPressIn={()=>this.zoomOut()}>    
                                    <Icon name="zoom-out" type={'MaterialIcons'} style={{fontSize:25}}/>
                                </TouchableOpacity>*/}
                                {this.state.filteredUnitLayout.filter((x)=>x.slots.filter((y)=>y.floor != null && y.unit != undefined && y.unit.isBumi).length>0).length>0?
                                <View style={{flexDirection:'column', justifyContent:'center'}}>
                                    <Text style={{fontSize:11, color:'#f00', fontWeight:'bold'}}>*Bold border indicate bumi unit</Text>
                                </View>:<View/>}
                            </View>                            
                            {/*<TouchableOpacity style={{flexDirection:'row'}} onPress={()=>this.setState({showFilter:this.state.showFilter?false:true, compareUnit:this.state.showFilter?true:false, width:'30%'})}>
                                <Image source={require('../image/filter.png')} style={{width:20, height:20, resizeMode:'contain', marginRight:10}} />
                                <Text style={{fontSize:fonts.contentFont, marginRight:5}}>Filter</Text>
                                {this.state.showFilter?
                                <Icon name='up' type={'AntDesign'} style={{fontSize:13, alignSelf:'center'}}/>:
                                <Icon name='down' type={'AntDesign'} style={{fontSize:13, alignSelf:'center'}}/>}
                            </TouchableOpacity>*/}
                        </View>  
                        {/*<View style={{height:Dimensions.get('window').height*0.65}} {...this._panResponder.panHandlers}>*/}
                        <View style={{height:Dimensions.get('window').height*0.65}}>
                            {!this.state.showFilter?
                            <ImageZoom 
                                       ref={c => {this.imageZoom = c}}
                                       cropWidth={Dimensions.get('window').width*0.9}
                                       cropHeight={Dimensions.get('window').height*0.65}
                                       imageWidth={Dimensions.get('window').width*0.9}
                                       imageHeight={Dimensions.get('window').height*0.65}
                                       maxScale={7}
                                       maxOverflow={1000}
    //                                   pinchToZoom={false}
    //                                   panToMove={false}
    //                                   enableSwipeDown={true}
                                       enableDoubleClickZoom={false}
    //                                   longPressTime={2000}
    //                                   onLongPress={()=>this.setState({disableBtn:false})}
    //                                   onMove={(x)=>alert(JSON.stringify(x))}
                                       minScale={1}
                                       style={{backgroundColor:'transparent', alignSelf:'center', justifyContent:'center'}}

                            >                             
                                <ScrollView horizontal={true} bounces={false} showsHorizontalScrollIndicator={false} style={{width:'100%'}} >                                 
                                    <FlatList extraData={this.state.refreshDashBoard} bounces={false} data={this.state.filteredUnitLayout} showsVerticalScrollIndicator={false} onEndReached={() =>this.setState({isLoading:false})}
                                        renderItem={({item, index:index1}) =>(
                                            <View style={{width:"100%", flexDirection:'row'}}>
                                                <Item style={{borderColor:"transparent", flexDirection:'column', justifyContent:'center'}}>
                                                    <Text style={{width:30*this.state.zoom, fontSize:12*this.state.zoom}}>{item.name}</Text>
                                                </Item>
                                                {item.slots.map((x, index)=>(
                                                    x.floor != null && x.unit!=undefined?
                                                    <TouchableOpacity disabled={x.status != 0?true:false} key={item.name+index1+index} style={{borderColor:'#000', borderWidth:x.unit != undefined && x.unit.isBumi?3:0.5, width:this.state.unitW*this.state.zoom, height:this.state.unitH*this.state.zoom, backgroundColor:x.selected != undefined && x.selected?this.state.selectedStyle.bgColor:this.state.statusStyle[x.status].bgColor, flexDirection:'column', justifyContent:'center'}} onPress={()=>this.selectUnit(x)}>
                                                        <Text style={{fontSize:this.state.unitTxtSize*this.state.zoom, textAlign:'center', color:this.state.statusStyle[x.status].txtColor}}>{x.unit.unitNumber}{'\n'}({x.unit.unitType}){x.unit.sizeSqf}sf{'\n'}RM {x.unit.listPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                                                    </TouchableOpacity>:
                                                    <View key={item.name+index1+index} style={{width:this.state.unitW*this.state.zoom*x.width, height:this.state.unitH*this.state.zoom, backgroundColor:x.backgroundColor, flexDirection:'column', justifyContent:'center'}}>
                                                        <Text style={{color:x.textColor, fontSize:this.state.unitTxtSize*this.state.zoom, alignSelf:'center'}}>{x.name}</Text>
                                                    </View>                                           
                                                ))}

                                            </View>
                                        )}
                                        keyExtractor={(item, index) => 'list' +index}
                                    />
                                </ScrollView>
                            </ImageZoom>:
                            <View style={[styles.addBorder, {flexDirection:'column', backgroundColor:'#cedae3', paddingLeft:20, paddingRight:20, paddingBottom:10}]}>
                                <View style={{flexDirection:'row', paddingTop:10}}>
                                    <Text style={{fontSize:18, fontWeight:'bold'}}>Block :</Text>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <Picker
                                          mode="dropdown"
                                          iosIcon={<Icon name="arrow-down" style={{fontSize:18, color:colors.btnColor}}/>}                                
                                          iosHeader="Select Block"
                                          style={{ width:undefined}}
                                          textStyle={{fontSize:fonts.contentFont, color:'#000', paddingLeft:0}}                              
                                          selectedValue={this.state.selectedBlockId}
                                          onValueChange={(value)=> this.setState({selectedBlockId:value})}
                                        >
                                        {this.state.blocks.map((item, index)=>{
                                            return <Picker.Item key={index} value={item.value} label={item.phaseName+"("+item.name+')'} />
                                        })}
                                    </Picker>  
                                </View>
                                <View style={{flexDirection:'row', justifyContent:'center'}}>
                                    <Button style={[styles.defaultBtn, {height:35, justifyContent:'center', width:80}]} onPress={()=>this.postRequestGetUnitSalesLayout()}>
                                        <Text style={styles.defaultBtnTxt}>Apply</Text>
                                    </Button>
                                </View>                                        
                            </View>
                            }
                        </View>
                        <View style={{flexDirection:'row', width:'100%', paddingTop:5}}>
                            <ScrollView horizontal={true} style={{flexDirection:'row', width:'65%', top:3.5}} bounces={false} showsHorizontalScrollIndicator={false}>
                                {this.state.selectedUnitId.map((x, index)=>(
                                    <TouchableOpacity key={index} style={{height:35, backgroundColor:this.state.selectedStyle.bgColor, borderRadius:15, flexDirection:'column', justifyContent:'center', width:70, marginRight:5}} onPress={()=>this.selectUnit(x)}>
                                        <Text style={{fontSize:10, textAlign:'center'}}>{x.unit.unitNumber}</Text>
                                    </TouchableOpacity>
                                 ))}
                            </ScrollView>
                            <Item picker style={{flexDirection:'column', justifyContent:'center', borderColor:colors.bgColor, marginRight:5, paddingRight:0, borderTopWidth:1, borderLeftWidth:1, borderRightWidth:1, borderRadius:8, alignSelf:'flex-end', height:35}}>
                                <Picker
                                  mode="dropdown"
                                  iosIcon={<Icon name="arrow-down" style={{fontSize:20}}/>}                  
                                  iosHeader="Change Block"                  
                                  style={{ width: 100, alignSelf:'center'}}
                                  textStyle={{fontSize:14, alignSelf:'center', color:colors.bgColor}}
                                  selectedValue={this.state.selectedBlockId}
                                  onValueChange={(value)=> this.postRequestGetUnitSalesLayout(value)}
                                >
                                    {this.state.blocks.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.value} label={item.name} />
                                    })}
                                </Picker>            
                            </Item>                            
                                                 
                            <View style={{flexDirection:'row', justifyContent:'flex-end', width:'25%'}}>
                                <Button style={[styles.defaultBtn, {height:35, width:80}]} onPress={()=>this.bookNow()}>
                                    <Text style={styles.defaultBtnTxt}>Book{this.state.selectedUnitId.length!=0?"("+this.state.selectedUnitId.length+")":''}</Text>
                                </Button>
                            </View>
                        </View>
                    </Content>
                </View>
                {this.state.showModal && this.state.selectedSummary.unit != undefined?
                <View style={{width:Dimensions.get('window').width, height:Dimensions.get('window').height, flexDirection:'column', justifyContent:'center', position:'absolute', paddingLeft:'20%', paddingRight:'20%'}}>
                    <View style={{flexDirection:'column', padding:10, backgroundColor:'#fff', borderColor:colors.btnColor, borderRadius:14, borderWidth:1, alignSelf:'center'}}>
                        <View style={{flexDirection:'row'}}>
                            <Item style={{borderColor:'transparent', width:'90%', justifyContent:'center', flexDirection:'column'}}>
                                <Text style={{fontWeight:'bold'}}>{this.state.selectedSummary.unit.unitNumber}</Text>
                            </Item>
                            <TouchableOpacity style={{flexDirection:'column', justifyContent:'center', width:50}} onPress={()=>this.setState({showModal:false})}>
                                <Icon name='close-o' type={'EvilIcons'} style={{fontSize:20}}/>
                            </TouchableOpacity>
                        </View>    
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:fonts.contentFont, width:'35%'}}>Type</Text>    
                            <Text style={{fontSize:fonts.contentFont, width:'65%'}}>: {this.state.selectedSummary.unit.unitType}</Text>    
                        </View>    
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:fonts.contentFont, width:'35%'}}>View</Text>    
                            <Text style={{fontSize:fonts.contentFont, width:'65%'}}>: {this.state.selectedSummary.unit.unitView}</Text>    
                        </View>    
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:fonts.contentFont, width:'35%'}}>Size</Text>    
                            <Text style={{fontSize:fonts.contentFont, width:'65%'}}>: {this.state.selectedSummary.unit.sizeSqf.toFixed(1)} ft{"\u00B2"}</Text>    
                        </View>    
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:fonts.contentFont, width:'35%'}}>Price (ft{"\u00B2"})</Text>    
                            <Text style={{fontSize:fonts.contentFont, width:'65%'}}>: RM {this.state.selectedSummary.unit.listPricePerSizeSqf.toFixed(2)}</Text>    
                        </View>    
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:fonts.contentFont, width:'35%'}}>List Price</Text>    
                            <Text style={{fontSize:fonts.contentFont, width:'65%'}}>: RM {this.state.selectedSummary.unit.listPrice.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>    
                        </View>   
                        {this.state.selectedSummary.unit.packageValues.map((x, index)=>(
                            <View key={index} style={{flexDirection:'row'}}>
                                <Text style={{fontSize:fonts.contentFont, width:'35%'}}>{x.name}</Text>    
                                <Text style={{fontSize:fonts.contentFont, width:'65%'}}>: {x.value}</Text>    
                            </View>
                        ))}
                        <View style={{flexDirection:'row', justifyContent:'center'}}>
                            {/*<Button transparent style={{backgroundColor:'transparent', height:35, justifyContent:'center', width:'50%'}} onPress={()=>this.selectUnit(this.state.selectedSummary)}>
                                <Text style={{color:'#000'}}>Cancel</Text>
                            </Button>*/}
                            <Button style={{backgroundColor:colors.btnColor, height:35, justifyContent:'center', width:'50%'}} onPress={()=>this.choose(this.state.selectedSummary)}>
                                <Text style={{color:'#000'}}>Select</Text>
                            </Button>
                               
                        </View>   
                    </View>
                </View>:<View/>
                }
                {this.state.isLoading?
                <View style={{position:"absolute", width:Dimensions.get('window').width, height:Dimensions.get('window').height, flexDirection:'column', justifyContent:'center'}}>
                <View style={{width:400, height:300, alignSelf:'center'}}>
                    
                </View>
                <Text style={{color:'#000', alignSelf:'center', top:-160}}>Loading</Text>
                </View>:<View/>}
            </Container>

        );
    }

}
function mapStateToProps(state) {
    return {
        token: state.token,
        apiPrefix: state.apiPrefix,        
        phaseName: state.phaseName
    }
}


function mapDispatchToProps(dispatch) {
    return {
        
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(salesDashboard);