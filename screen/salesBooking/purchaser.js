import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Title, Icon, Button, Item, Label, Form, Input, Textarea, Picker, DatePicker, ListItem, CheckBox } from "native-base";
import { TouchableOpacity, View, Dimensions, Alert, FlatList, ActivityIndicator} from 'react-native'
import {styles, colors} from '../globalStyle.js'
import { api_searchIC, api_findCustomer, api_getCustomerTitle, api_getBusinessTitle, api_getUnitSalePartyForEdit, api_getCorporateTypeCode, api_getCustomer, api_claimCustomer, api_getCountriesNumber, api_createOrUpdateUnitSaleParty, api_createOrUpdateAddress, api_getUnitSale, api_getCorrespondAddress, api_getUnitSalesDetailForEdit, api_removeSaleParty } from '../config.js'
import {connect} from 'react-redux'
import Moment from 'moment'

//Sales.ManageSolicitor
//Sales.ManageSellers

class purchaserPage extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            isLoading:false,
            searchResult:[],
            purchaserOption:false,
            contactOption:false,
            purchaserDetail:false,
            contactDetail:false,
            individual:false,
            corporate:false,
            userExisted:false,
            cancelAdd:false,
            salesDetail:[],
            salePartyDetail:[],
            cusID:null,
            cusTitle:'',
            cusIC:'',
            cusName:'',
            cusBirthDate:'Select Date',
            cusGender:'0',
            cusRace:'',
            cusEmail:'',
            cusMaritalStatus:'0',
            cusOccupation:'',
            cusRelationship:'',
            cusRemark:'',
            cusCountry:'MY',
            cusPostcode:'',
            cusState:'',
            cusCity:'',
            cusStreet:'',
            cusAddressCountryCode:'MY',
            cusContact:'',
            cusContactName:'',
            cusContactIC:'',
            cusContactTitle:'',
            cusCorporateType:'SDN BHD',
            cusCallingCode:'60',
            correspond:true,
            correspondCity:'',
            correspondCountryCode:'MY',
            correspondState:'',
            correspondStreet:'',
            correspondPostcode:'',
            raceList:[],
            countryList:[],
            corporateList:[],
            titleList:[],
            eventList:[],
            agentList:[],
            solicitorList:[],
            emailField:'#000000',
            raceField:'#000000',
            nameField:'#000000',
            titleField:'#000000',
            icField:'#000000',
            countryField:'#000000',
            contactICField:'#000000',
            contactNameField:'#000000',
            corporateTypeField:'#000000',
            contactField:'#000000',
            streetField:'#000000',
            cityField:'#000000',
            maritalField:'#000000',
            msg:'',
            checkIC:true,
        }
    }
    
    componentDidMount(){
        const {navigation} = this.props;
        this.setState({
            salesDetail:navigation.getParam('unitSaleDetail', [])
        });
        this.postRequestGetCustomerTitle();
    }
    
    postRequestFindCustomer(){
        this.setState({isLoading:true});
        return fetch(api_findCustomer, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            filter: this.state.cusIC,
            sorting: null,
            skipCount: 0,
            maxResultCount: 200000
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    this.setState({
                        searchResult:result.items,
                        isLoading:false
                    })
                    if(result.items.length == 0){
                        Alert.alert(
                            'No Record Found',
                            "This user is either not yet register or you have key in the wrong info.",
                            [
                                {text:'Back', onPress:()=> console.log('cancel'), style:'cancel'},
                                {text:'Register New', onPress:()=> {this.setState({purchaserOption:true});this.postRequestGetUnitSaleParty()}}
                            ],
                            {cancelable:false}
                        )                        
                    }
                }
                else{
                    this.setState({
                        isLoading:false
                    })
                    let errorMsg = responseJson.error;
                    if(errorMsg.message == "Required permissions are not granted. At least one of these permissions must be granted: Customer"){
                        this.setState({purchaserOption:true});
                        this.postRequestSearchIC()
                    }
                    
//                    Alert.alert(
//                        'No Record Found',
//                        "This user is either not yet register or you have key in the wrong info.",
//                        [
//                            {text:'Back', onPress:()=> console.log('cancel'), style:'cancel'},
//                            {text:'Register New', onPress:()=> {this.setState({purchaserOption:true});this.postRequestGetUnitSaleParty()}}
//                        ],
//                        {cancelable:false}
//                    )                     
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestSearchIC(){
        this.setState({isLoading:true})
        return fetch(api_searchIC, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            nationalId: this.state.cusIC,
            nationalityCode:this.state.cusCountry
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    let detail = result;
                    let contact = detail.mailingAddress;
                    let phoneNum = detail.phones;
                    
                    this.setState({
                        cusID:detail.customerId,
                        cusTitle:detail.title,
                        cusIC:detail.nationalId,
                        cusName:detail.fullName,
                        cusBirthDate:detail.birthDate == null?'Select Date':Moment(detail.birthDate).format('M/D/YYYY'),
                        cusGender:''+detail.gender,
                        cusRace:detail.raceId,
                        cusEmail:detail.emailAddress,
                        cusMaritalStatus:''+detail.maritalStatus,
                        cusOccupation:detail.occupation,
                        cusRelationship:detail.relationship,
                        cusRemark:detail.remarks,
                        cusCountry:detail.nationalityCode,
                        cusPostcode:contact.postcode,
                        cusState:contact.state,
                        cusCity:contact.city,
                        cusStreet:contact.street,
                        cusAddressCountryCode:contact.countryCode,
                        cusContact:phoneNum[0].number,
                        cusContactName:detail.contactName,
                        cusContactIC:detail.contactNationalId,
                        cusContactTitle:detail.contactTitle,
                        cusCorporateType:detail.corporateType,
                        cusCallingCode:this.getCountryCall(contact.countryCode),
                 
                    })
                    alert('Existing record found with this NRIC.')
                    this.postRequestGetUnitSaleParty();
                }   
                else{
                    this.setState({
                        cusID:null,
                        cusTitle:'',
                        cusName:'',
                        cusBirthDate:'Select Date',
                        cusGender:'0',
                        cusRace:'',
                        cusEmail:'',
                        cusMaritalStatus:'0',
                        cusOccupation:'',
                        cusRelationship:'',
                        cusRemark:'',
                        cusCountry:'MY',
                        cusPostcode:'',
                        cusState:'',
                        cusCity:'',
                        cusStreet:'',
                        cusAddressCountryCode:'MY',
                        cusContact:'',
                        cusContactName:'',
                        cusContactIC:'',
                        cusContactTitle:'',
                        cusCorporateType:'SDN BHD',
                        cusCallingCode:'60',                        
                    })
                    alert('No existing record found with this NRIC.')
                    this.postRequestGetUnitSaleParty()
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetCustomerDetail(id){
        return fetch(api_getCustomer, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            id:id
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    let detail = result.customer;
                    let contact = detail.mailingAddress;
                    let phoneNum = detail.phones;
                    
                    this.setState({
                        cusID:id,
                        cusTitle:detail.title,
                        cusIC:detail.nationalId,
                        cusName:detail.fullName,
                        cusBirthDate:detail.birthDate == null?"Select Date":Moment(detail.birthDate).format('M/D/YYYY'),
                        cusGender:''+detail.gender,
                        cusRace:detail.raceId,
                        cusEmail:detail.emailAddress,
                        cusMaritalStatus:''+detail.maritalStatus,
                        cusOccupation:detail.occupation,
                        cusRelationship:detail.relationship,
                        cusRemark:detail.remarks,
                        cusCountry:detail.nationalityCode,
                        cusPostcode:contact.postcode,
                        cusState:contact.state,
                        cusCity:contact.city,
                        cusStreet:contact.street,
                        cusAddressCountryCode:contact.countryCode,
                        cusContact:phoneNum[0].number,
                        cusContactName:detail.contactName,
                        cusContactIC:detail.contactNationalId,
                        cusContactTitle:detail.contactTitle,
                        cusCorporateType:detail.corporateType,
                        cusCallingCode:this.getCountryCall(contact.countryCode),
                        purchaserOption:true
                    })
                    this.postRequestGetUnitSaleParty();
                }   
                else{
                    alert("Internal error, can't get customer id"+id+"detail")
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetUnitSaleParty(){
        this.setState({isLoading:true})
        return fetch(api_getUnitSalePartyForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            unitSaleId: this.state.salesDetail[0],
            type: "Purchaser",
            unitSalePartyId: null,
            isCorporate: this.state.corporate
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
                this.setState({isLoading:false})
                if(result != null){
                    this.setState({
                        raceList:result.races,
                        checkIC:true,
                        contactField:'#000',
                        streetField:'#000',
                        cityField:'#000',
                        emailField:'#000',
                        raceField:'#000',
                        titleField:'#000',
                        icField:'#000',
                        nameField:'#000',
                        countryField:'#000',
                        contactICField:'#000',
                        contactNameField:'#000',
                        corporateTypeField:'#000',                        
                    })  
                }  
                else{
                    alert(JSON.stringify(responseJson))
                }
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetUnitSalesForEdit(){
        this.setState({isLoading:true})
        return fetch(api_getUnitSalesDetailForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: this.state.salesDetail
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false, salePartyDetail:[]})
                let result = responseJson.result;
                if(result != null){
                    let salesParties = result.unitSales;
                    if(salesParties.length >0){
                        for(let i=0; i<salesParties.length; i++){
                            this.setState((prevState)=>({salePartyDetail:[...prevState.salePartyDetail, salesParties[i]]}))
                            if(salesParties[i].parties.length>0){
                                this.setState({
                                    userExisted:true,
                                    cancelAdd:true
                                })
                               
                            }
                            else{
                                this.setState({corporate:false, individual:false, userExisted:false, cancelAdd:false})
                            }
                        }
                        
      
//                        alert(JSON.stringify(this.state.salePartyDetail.length))
                    }
                    
                    this.setState({
                        eventList:result.events,
                        agentList:result.agents,
                        solicitorList:result.solicitors    
                    })
                  
                    
                }     
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetCustomerTitle(){
        this.setState({isLoading:true})
        return fetch(api_getCustomerTitle, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          }
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    this.setState({
                        titleList:result.items,
                       
                    })
                    this.postRequestGetCountryNum()
                }     

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetCorporateTypeCode(){
        return fetch(api_getCorporateTypeCode, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          }
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    this.setState({
                        corporateList:result.items,
                    })
                }     
                this.postRequestGetUnitSalesForEdit()

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetCountryNum(){
        return fetch(api_getCountriesNumber, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          }
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    this.setState({
                        countryList:result.items,
                       
                    })
//                    alert(JSON.stringify(result.items))
                    this.postRequestGetCorporateTypeCode()
                }     

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestCreateUnitSaleParty(){
        this.setState({isLoading:true})
        return fetch(api_createOrUpdateUnitSaleParty, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              party: {
                unitSaleId: this.state.salesDetail[0],
                type: "Purchaser",
                title: this.state.cusTitle,
                fullName: this.state.cusName,
                nationalId: this.state.cusIC,
                nationalityCode: this.state.cusCountry,
                raceId: this.state.cusRace,
                gender: this.state.cusGender,
                birthDate: this.state.cusBirthDate == 'Select Date'? '':new Date(this.state.cusBirthDate),
                occupation: this.state.cusOccupation,
                relationship: this.state.cusRelationship,
                maritalStatus: this.state.cusMaritalStatus,
                emailAddress: this.state.cusEmail,
                remarks: this.state.cusRemark,
                customerId: this.state.cusID,
                contactTitle: this.state.cusContactTitle,
                contactName: this.state.cusContactName,
                contactNationalId: this.state.cusContactIC,
                isCorporate: this.state.corporate,
                corporateType: this.state.cusCorporateType,
                mailingAddress: {
                  description: null,
                  street: this.state.cusStreet,
                  city: this.state.cusCity,
                  state: this.state.cusState,
                  postcode: this.state.cusPostcode,
                  countryCode: this.state.cusAddressCountryCode,
                  id: 0
                },
                phones: [
                  {
                    number: this.cusCallingCode+this.state.cusContact,
                    description: "H/P",
                    id: 0
                  }
                ],
                id: 0
              },
              unitSaleIds: this.state.salesDetail,
              shouldUpdateCustomer: true
            })
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.success;
        
                if(result == true){
                    this.postRequestGetUnitSale();
                }     
                else{
                    this.setState({isLoading:false});
                    alert(JSON.stringify(responseJson.error))
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetCorrespondAdd(){
        return fetch(api_getCorrespondAddress, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id: 0
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.success;
                
                if(result == true){
                    this.postRequestGetUnitSale();
                }     

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetUnitSale(){
        return fetch(api_getUnitSale, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id: this.state.salesDetail[0]
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    let address = result.parties;
                    
                    this.postRequestUpdateCorrespondAdd(address[0].mailingAddress.id);
                }     

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestRemove(item){
        this.setState({isLoading:true});
        return fetch(api_removeSaleParty, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: this.state.salesDetail,
              unitSalePartyId: item.id
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.success;

                if(result){
                    Alert.alert(
                        "Remove Successfully",
                        "To add back this owner to this unit you would required assist from admin.",
                        [
                            {text:'Noted', onPress:()=>this.postRequestGetUnitSalesForEdit(), style:'cancel'}
                        ],
                        {cancelable:false}
                    )
                }     

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestUpdateCorrespondAdd(id){
        this.state.correspondStreet == ''? this.setState({correspond:true}):''
        this.state.correspondCity == ''? this.setState({correspond:true}):''
        
        if(this.state.correspond){
            this.setState({
                correspondCity:this.state.cusCity,
                correspondCountryCode:this.state.cusAddressCountryCode,
                correspondPostcode:this.state.cusPostcode,
                correspondState:this.state.cusState,
                correspondStreet:this.state.cusStreet
            })
        }
        
        return fetch(api_createOrUpdateAddress, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: this.state.salesDetail,
              address: {
                description: "",
                street: this.state.correspondStreet,
                city: this.state.correspondCity,
                state: this.state.correspondState,
                postcode: this.state.correspondPostcode,
                countryCode: this.state.correspondCountryCode,
                id: id
              }
            })
        })
          .then((response) => response.json())
          .then((responseJson) => {
               
                let result = responseJson.success;
                if(result == true){
                   
         
//                    this.proceed();
                    this.setState({
                        userExisted:true,
                        individual:false,
            
                        searchResult:[],
                        purchaserOption:false,
                        cusID:null,
                        cusTitle:'',
                        cusIC:'',
                        cusName:'',
                        cusBirthDate:'Select Date',
                        cusGender:'0',
                        cusRace:'',
                        cusEmail:'',
                        cusMaritalStatus:'0',
                        cusOccupation:'',
                        cusRelationship:'',
                        cusRemark:'',
                        cusCountry:'MY',
                        cusPostcode:'',
                        cusState:'',
                        cusCity:'',
                        cusStreet:'',
                        cusAddressCountryCode:'MY',
                        cusContact:'',
                        cusContactName:'',
                        cusContactIC:'',
                        cusContactTitle:'',
                        cusCorporateType:'SDN BHD',
                        cusCallingCode:'60',
                        correspond:true,
                        correspondCity:'',
                        correspondCountryCode:'MY',
                        correspondState:'',
                        correspondStreet:'',
                        correspondPostcode:'',  
                        contactOption:false,
                        salePartyDetail:[]
                    })
                   this.postRequestGetUnitSalesForEdit();
                }     

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    proceed(){
        const {navigate} = this.props.navigation;
        navigate('salesInfo',{event:this.state.eventList, agent:this.state.agentList, solicitor:this.state.solicitorList, saleID:this.state.salesDetail});
    }
    
    purchaserInfoValidation(){
        let proceed = false
        this.setState({msg:''})
        if(this.state.individual){

                this.state.cusContact == ''? this.setState({contactField:'#ff0000', msg:"Contact field can't be empty."}): this.setState({contactField:'#000000'})
                this.state.cusStreet == ''? this.setState({streetField:'#ff0000', msg:"You can't leave the street field blank."}): this.setState({streetField:'#000000'})
                this.state.cusCity == ''? this.setState({cityField:'#ff0000', msg:"You can't leave the city field blank."}): this.setState({cityField:'#000000'})
                this.state.checkIC?"":this.setState({msg:"Please click the search icon to verify this IC before proceed."})
                this.state.cusEmail == ''? this.setState({emailField:'#ff0000', msg:'Please enter your email address.'}): this.setState({emailField:'#000000'})
                this.state.cusRace == ''? this.setState({raceField:'#ff0000', msg:'Please select at least one race.'}): this.setState({raceField:'#000000'})        
                this.state.cusTitle == ''? this.setState({titleField:'#ff0000', msg:'Please select at least one title.'}): this.setState({titleField:'#000000'})            
                this.state.cusIC == ''? this.setState({icField:'#ff0000', msg:'Please provide your IC/passport number.'}): this.setState({icField:'#000000'})
                this.state.cusName == ''? this.setState({nameField:'#ff0000', msg:'Please provide your full name.'}): this.setState({nameField:'#000000'})   
                this.state.cusCountry == ''? this.setState({countryField:'#ff0000', msg:'Please select which country you from.'}): this.setState({countryField:'#000000'})  
 
         
            
        }
        else{
         
                this.state.cusContact == ''? this.setState({contactField:'#ff0000', msg:"Contact field can't be empty."}): this.setState({contactField:'#ff0000'})
                this.state.cusStreet == ''? this.setState({streetField:'#ff0000', msg:"You can't leave the street field blank."}): this.setState({streetField:'#ff0000'})
                this.state.cusCity == ''? this.setState({cityField:'#ff0000', msg:"You can't leave the city field blank."}): this.setState({cityField:'#ff0000'})
                this.state.checkIC?"":this.setState({msg:"Please click the search icon to verify this IC before proceed."})
                this.state.cusEmail == ''? this.setState({emailField:'#ff0000', msg:'Please enter your email address.'}): this.setState({emailField:'#000000'})
                this.state.cusRace == ''? this.setState({raceField:'#ff0000', msg:'Please select at least one race.'}): this.setState({raceField:'#000000'})
                this.state.cusContactIC == ''? this.setState({contactICField:'#ff0000', msg:'Please provide your IC or passport number.'}): this.setState({contactICField:'#000000'})
                this.state.cusContactName == ''? this.setState({contactNameField:'#ff0000', msg:'Please provide your name.'}): this.setState({contactNameField:'#000000'})
                this.state.cusCorporateType == ''? this.setState({corporateTypeField:'#ff0000', msg:'Please select at least one corporate type'}): this.setState({corporateTypeField:'#000000'})
                this.state.cusName == ''? this.setState({nameField:'#ff0000', msg:'Please enter your company name.'}): this.setState({nameField:'#000000'})            
                this.state.cusIC == ''? this.setState({icField:'#ff0000', msg:'Please enter your company registration number.'}): this.setState({icField:'#000000'});
                this.state.cusCountry == ''? this.setState({countryField:'#ff0000', msg:'Please select which country you from.'}): this.setState({countryField:'#000000'});
            
        }

        setTimeout(()=>{
            this.state.msg !=''?
            Alert.alert(
                'Info not complete',
                this.state.msg,
                [
                    {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                ],
                {cancelable:false}
            ): this.postRequestCreateUnitSaleParty()
        },500);
    }
    
    getCountryCall(code){
        let id = this.state.countryList.filter((x)=>x.value==code);
        return id[0].callingCode;
    }
    
    getContactNumber(num){
        if(num.length>0){
            return num[0].number;
        }
        else{
            return '-';
        }
    }
    
    removeSalePartyAlert(item){
        Alert.alert(
            "Remove this owner?",
            "Please ensure that this action is irreversible, if you want to add this purchaser back should request assist from admin.",
            [
                {text:'Cancel', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Remove', onPress:()=>this.postRequestRemove(item)}
            ],
            {cancelable:false}
        )
    }
    
  render() {
      const {goBack} = this.props.navigation
      
        if(this.state.isLoading){
            return ( 
                <Container style={{backgroundColor:colors.bgColor}}>
                  <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                    <ActivityIndicator size="large" color="#ffffff" />
                    <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
                  </View>
                </Container>
            )
        }       
      
        contact = (
            <Content padder>
                <Item style={{marginTop:10}}><Label style={{alignSelf:'flex-start', color:colors.formTxtColor, fontWeight:'bold'}}>Address</Label></Item>
                <Item style={{flexDirection:'column', marginTop:10}}>
                    <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Country*</Label>
                    <Item style={{paddingLeft:5, borderColor:'transparent'}}>
                        <Picker
                          mode="dropdown"
                          iosHeader="Select Country"
                          iosIcon={<Icon name="arrow-down" />}
                          style={{ width: undefined }}
                          selectedValue={this.state.cusAddressCountryCode}
                          onValueChange={(value)=>this.setState({cusAddressCountryCode:value})}
                        >
                            <Picker.Item value='' label='Select Country' />
                            {this.state.countryList.map((item, index)=>{
                                return <Picker.Item key={index} value={item.value} label={item.name} />
                            })}
                        </Picker>
                    </Item> 
                </Item>                
                <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                    <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Postcode</Label>
                    <Input onChangeText={(value) => {this.setState({cusPostcode:value})}} value={this.state.cusPostcode} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                </Item>
                <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                    <Label style={{alignSelf:'flex-start', color:this.state.cityField == '#000000'?colors.formTxtColor:this.state.cityField}}>City*</Label>
                    <Input onChangeText={(value) => {this.setState({cusCity:value, cityField:value==''?'#f00':'#000'})}} value={this.state.cusCity} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                </Item>
                <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                    <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>State/Province</Label>
                    <Input onChangeText={(value) => {this.setState({cusState:value})}} value={this.state.cusState} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                </Item>
                <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                    <Label style={{alignSelf:'flex-start', color:this.state.streetField == '#000000'?colors.formTxtColor:this.state.streetField}}>Street Address*</Label>
                    <Form style={{width:'100%'}}>
                        <Textarea onChangeText={(value) => {this.setState({cusStreet:value, streetField:value==''?'#f00':'#000'})}} value={this.state.cusStreet} rowSpan={5} style={{borderColor:colors.formTxtColor}} bordered />
                    </Form>
                </Item>
                <Item style={{borderColor:'transparent', marginTop:10, width:'100%', justifyContent:'flex-end', paddingRight:15, marginBottom:15}}>  
                    <Text>Same as Correspondence Address</Text>
                    <CheckBox checked={this.state.correspond} onPress={()=>this.state.correspond? this.setState({correspond:false}):this.setState({correspond:true})}/>      
                </Item>
                {!this.state.correspond? 
                    <View style={{flex:1}}>
                        <Item style={{marginTop:10}}><Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Correspondence Address</Label></Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Country</Label>
                            <Item style={{paddingLeft:5, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Country"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.correspondCountryCode}
                                  onValueChange={(value)=>this.setState({correspondCountryCode:value})}
                                >
                                    <Picker.Item value='' label='Select Country' />
                                    {this.state.countryList.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.value} label={item.name} />
                                    })}
                                </Picker>
                            </Item> 
                        </Item>                
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Postcode</Label>
                            <Input onChangeText={(value) => {this.setState({correspondPostcode:value})}} value={this.state.correspondPostcode} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>City</Label>
                            <Input onChangeText={(value) => {this.setState({correspondCity:value})}} value={this.state.correspondCity} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>State/Province</Label>
                            <Input onChangeText={(value) => {this.setState({correspondState:value})}} value={this.state.correspondState} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Street Address</Label>
                            <Form style={{width:'100%'}}>
                                <Textarea onChangeText={(value) => {this.setState({correspondStreet:value})}} value={this.state.correspondStreet} rowSpan={5} style={{borderColor:colors.formTxtColor}} bordered />
                            </Form>
                        </Item> 
                    </View>:<View/>                     
                }

                <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                    <Label style={{alignSelf:'flex-start', color:this.state.contactField == '#000000'?colors.formTxtColor:this.state.contactField}}>Contact Number*</Label>
                    <Item style={{paddingLeft:5, borderColor:'transparent'}}>
                        <Picker
                          mode="dropdown"
                          iosIcon={<Icon name="arrow-down"/>}
                          style={{ width: undefined }}
                          headerTitleStyle={{fontSize:15}}
                          selectedValue={this.state.cusCallingCode}
                          onValueChange={(value)=>{this.setState({cusCallingCode:''+value, contactField:value ==''?'#f00':'#000'})}}
                        >                                                 
                            <Picker.Item value='' label='Select Country Code' />
                            {this.state.countryList.map((item, index)=>{ 
                                return <Picker.Item key={index} value={''+item.callingCode} label={'(+'+item.callingCode+' '+item.name+')'} />
                            })}
                        </Picker> 
                    </Item>       
                    <Item>
                        <Input placeholder='e.g 1234123' keyboardType='numeric' value={this.state.cusContact} onChangeText={(value) => {this.setState({cusContact:value.replace(/[^0-9]/g, '')})}} style={{borderWidth:1, width:'100%'}}/>
                    </Item>                    
                </Item>                     
            </Content>      
      )      
      
      if(this.state.purchaserOption){
          this.state.individual?
              purchasers = (
                  <Container style={{backgroundColor:'#ffffff'}}>
                    <Item style={{paddingLeft:5, backgroundColor:colors.btnColor, height:50, borderColor:this.state.cusCountry != '' && this.state.cusTitle != '' && this.state.cusName != '' && this.state.cusIC != '' && this.state.cusRace != '' && this.state.cusEmail != ''? '#0f0':'#f00'}}>
                        <Item style={{width:'90%', borderColor:'transparent'}}>
                            <Label style={{color:colors.bgColor}}>Particulars*</Label>
                        </Item>
                        <TouchableOpacity >
                            {this.state.purchaserDetail?<Icon onPress={()=>this.state.purchaserDetail?this.setState({purchaserDetail:false}):this.setState({purchaserDetail:true, contactDetail:false})} style={{alignSelf:'flex-end'}} name='minus' type='EvilIcons' />:<Icon onPress={()=>this.state.purchaserDetail?this.setState({purchaserDetail:false}):this.setState({purchaserDetail:true, contactDetail:false})} style={{alignSelf:'flex-end'}} name='plus' type='EvilIcons'/>}
                        </TouchableOpacity>
                    </Item>
                    {this.state.purchaserDetail?
                    <Content padder>
                        <Item style={{flexDirection:'column'}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.corporateTypeField == '#000000'?colors.formTxtColor:this.state.corporateTypeField}}>Country*</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Country"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusCountry}
                                  onValueChange={(value)=>this.setState({cusCountry:value, corporateTypeField:value == ''?'#f00':'#000'})}
                                >
                                    <Picker.Item value='' label='Select Country' />
                                    {this.state.countryList.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.value} label={item.name} />
                                    })}
                                </Picker>
                            </Item>
                        </Item>
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.titleField == '#000000'?colors.formTxtColor:this.state.titleField}}>Title*</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Title"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusTitle}
                                  onValueChange={(value)=>this.setState({cusTitle:value, titleField:value==''?'#f00':'#000'})}
                                >
                                    {this.state.titleList.map((item, index)=>{
                                        return index!=0?<Picker.Item key={index} value={item.name} label={item.name} />:<Picker.Item key={index} value='' label='Select Title' />
                                    })}
                                </Picker>
                            </Item>   
                        </Item>                         
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.nameField == '#000000'?colors.formTxtColor:this.state.nameField}}>Full Name*</Label>
                            <Input onChangeText={(value) => {this.setState({cusName:value, nameField:value==''?'#f00':'#000'})}} value={this.state.cusName} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item> 
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.icField == '#000000'?colors.formTxtColor:this.state.icField}}>IC/Passport no.*</Label>
                            {this.state.checkIC?<View/>:<Label style={{alignSelf:'flex-start', color:'#f00'}}>Click the search icon to verify this IC *</Label>}
                            <Item style={{flexDirection:'row', borderColor:'transparent'}}>
                                <Input placeholder='e.g. 910123244321' onChangeText={(value) => {this.setState({cusIC:value, icField:value==''?'#f00':'#000', checkIC:false})}} value={this.state.cusIC} style={{borderColor: colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%', color:this.state.checkIC?'#000':'#f00'}}/>
                                <Icon name="search" type={'EvilIcons'} style={{fontSize:40, color:this.state.checkIC?'#000':'#f00'}} onPress={()=>this.postRequestSearchIC()} />
                            </Item>
                        </Item>           
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Birth Date</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent', alignSelf:'flex-end'}}> 
                                <DatePicker 
                                    defaultDate={new Date()}
                                    minimumDate={new Date(1900, 1, 1)}
                                    maximumDate={new Date()}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}  
                                    androidMode={"default"}
                                    placeHolderText={this.state.cusBirthDate == "Select Date"?"Select Date":Moment(new Date(this.state.cusBirthDate)).format('D/M/YYYY')}
                                    textStyle={{ color: "#000000" }}
                                    placeHolderTextStyle={{ color: "#000000" }}
                                    onDateChange={(newDate)=> this.setState({cusBirthDate:''+newDate})}
                                    disabled={false}
                                />
                            </Item>
                        </Item>
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Gender</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Gender"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusGender}
                                  onValueChange={(value)=>this.setState({cusGender:value})}
                                >
                                  <Picker.Item label="Unknown" value="0" />
                                  <Picker.Item label="Male" value="1" />
                                  <Picker.Item label="Female" value="2" />
                                </Picker>
                            </Item>
                        </Item>
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.raceField == '#000000'?colors.formTxtColor:this.state.raceField}}>Race*</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Race"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusRace}
                                  onValueChange={(value)=>this.setState({cusRace:value, raceField:value==''?'#f00':'#000'})}
                                >
                                    <Picker.Item key={'default'} value={''} label={'Select Race'} />
                                    {this.state.raceList.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.value} label={item.name} />
                                    })}
                                </Picker>
                            </Item>                        
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.emailField == '#000000'?colors.formTxtColor:this.state.emailField}}>Email Address*</Label>
                            <Input onChangeText={(value) => {this.setState({cusEmail:value, emailField:value==''?'#f00':'#000'})}} value={this.state.cusEmail} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Marital status</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Marital Status"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusMaritalStatus}
                                  onValueChange={(value)=>this.setState({cusMaritalStatus:value})}
                                >
                                  <Picker.Item label="Unknown" value="0" />
                                  <Picker.Item label="Single" value="1" />
                                  <Picker.Item label="Married" value="2" />
                                </Picker>
                            </Item>
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Occupation</Label>
                            <Input onChangeText={(value) => {this.setState({cusOccupation:value})}} value={this.state.cusOccupation} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>How are you affiliated with us?</Label>
                            <Input onChangeText={(value) => {this.setState({cusRelationship:value})}} value={this.state.cusRelationship} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Remarks</Label>
                            <Form style={{width:'100%'}}>
                                <Textarea onChangeText={(value) => {this.setState({cusRemark:value})}} value={this.state.cusRemark} rowSpan={5} style={{borderColor:colors.formTxtColor}} bordered />
                            </Form>
                        </Item>    
                    </Content>:<View/>}
                    <Item style={{marginTop:5, paddingLeft:5, backgroundColor:colors.btnColor, height:50, borderColor:this.state.cusAddressCountryCode != '' && this.state.cusCity != '' && this.state.cusStreet != '' && this.state.cusCallingCode != ''? '#0f0':'#f00'}}> 
                        <Item style={{width:'90%', borderColor:'transparent'}}>
                            <Label style={{color:colors.bgColor}}>Contact*</Label>
                        </Item>
                        <TouchableOpacity >
                             {this.state.contactDetail?
                             <Icon onPress={()=>this.state.contactDetail?this.setState({contactDetail:false}):this.setState({contactDetail:true, purchaserDetail:false})} style={{alignSelf:'flex-end'}} name='minus' type='EvilIcons' />:<Icon onPress={()=>this.state.contactDetail?this.setState({contactDetail:false}):this.setState({contactDetail:true, purchaserDetail:false})} style={{alignSelf:'flex-end'}} name='plus' type='EvilIcons'/>}
                        </TouchableOpacity>
                    </Item>
                    {this.state.contactDetail?
                      <View style={{flex:1}}>{contact}</View>:<View/>
                    }                 
                  </Container> 
              ):
              purchasers = (
                  <Container style={{backgroundColor:'#ffffff'}}>
                        <Item style={{paddingLeft:5, backgroundColor:colors.btnColor, height:50, borderColor:this.state.cusCountry != '' && this.state.cusIC != '' && this.state.cusName != '' && this.state.cusCorporateType != '' && this.state.cusContactName != '' && this.state.cusContactIC != '' && this.state.cusRace != '' && this.state.cusEmail != ''?'#0f0':'#f00'}}>
                            <Item style={{width:'90%', borderColor:'transparent'}}>
                                <Label style={{color:colors.bgColor}}>Particulars*</Label>
                            </Item>
                            <TouchableOpacity >
                                {this.state.purchaserDetail?<Icon onPress={()=>this.state.purchaserDetail?this.setState({purchaserDetail:false}):this.setState({purchaserDetail:true, contactDetail:false})} style={{alignSelf:'flex-end'}} name='minus' type='EvilIcons' />:<Icon onPress={()=>this.state.purchaserDetail?this.setState({purchaserDetail:false}):this.setState({purchaserDetail:true, contactDetail:false})} style={{alignSelf:'flex-end'}} name='plus' type='EvilIcons'/>}
                            </TouchableOpacity>
                        </Item> 
                        {this.state.purchaserDetail?
                        <Content padder>
                          <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.countryField == '#000000'?colors.formTxtColor:this.state.countryField}}>Country*</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Country"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusCountry}
                                  onValueChange={(value)=>this.setState({cusCountry:value, countryField:value==''?'#f00':'#000'})}
                                >
                                    <Picker.Item value='' label='Select Country' />
                                    {this.state.countryList.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.value} label={item.name} />
                                    })}
                                </Picker>
                            </Item> 
                        </Item>  
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.icField == '#000000'?colors.formTxtColor:this.state.icField}}>Company Reg. No.*</Label>
                            <Input onChangeText={(value) => {this.setState({cusIC:value, icField:value==''?'#f00':'#000'})}} value={this.state.cusIC} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>  
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.nameField == '#000000'?colors.formTxtColor:this.state.nameField}}>Company Name*</Label>
                            <Input onChangeText={(value) => {this.setState({cusName:value, nameField:value==''?'#f00':'#000'})}} value={this.state.cusName} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>                   
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.corporateTypeField == '#000000'?colors.formTxtColor:this.state.corporateTypeField}}>Type*</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Corporate Type"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusCorporateType}
                                  onValueChange={(value)=>this.setState({cusCorporateType:value, corporateTypeField:value == ''?'#f00':'#000'})}
                                >
                                    <Picker.Item value='' label='Select Type' />
                                    {this.state.corporateList.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.value} label={item.name} />
                                    })}
                                </Picker>
                            </Item> 
                        </Item>     
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Title</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Title"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusContactTitle}
                                  onValueChange={(value)=>this.setState({cusContactTitle:value})}
                                >
                                    {this.state.titleList.map((item, index)=>{
                                        return index != 0?<Picker.Item key={index} value={item.value} label={item.name} />: <Picker.Item key={index} value='' label='Select Title' />
                                    })}
                                </Picker>
                            </Item>   
                        </Item>                      
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.contactNameField == '#000000'? colors.formTxtColor:this.state.contactNameField}}>Contact Person*</Label>
                            <Input onChangeText={(value) => {this.setState({cusContactName:value, contactNameField:value==''?'#f00':'#000'})}} value={this.state.cusContactName} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item> 
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Business Title</Label>
                            <Input onChangeText={(value) => {this.setState({cusOccupation:value})}} value={this.state.cusOccupation} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item> 
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.contactICField == '#000000'?colors.formTxtColor:this.state.contactICField}}>IC/Passport no.*</Label>
                            <Input onChangeText={(value) => {this.setState({cusContactIC:value, contactICField:value == ''?'#f00':'#000'})}} value={this.state.cusContactIC} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>           
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Birth Date</Label>
                            <Item style={{alignSelf:'flex-end', borderColor:'transparent'}}> 
                                <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(1900, 1, 1)}
                                    maximumDate={new Date()}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}  
                                    androidMode={"default"}
                                    placeHolderText={this.state.cusBirthDate == "Select Date"?"Select Date":Moment(new Date(this.state.cusBirthDate)).format('D/M/YYYY')}
                                    textStyle={{ color: "#000000" }}
                                    placeHolderTextStyle={{ color: "#000000" }}
                                    onDateChange={(newDate)=> this.setState({cusBirthDate:''+newDate})}
                                    disabled={false}
                                />
                            </Item>
                        </Item>
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Gender</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Gender"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusGender}
                                  onValueChange={(value)=>this.setState({cusGender:value})}
                                >
                                  <Picker.Item label="Unknown" value="0" />
                                  <Picker.Item label="Male" value="1" />
                                  <Picker.Item label="Female" value="2" />
                                </Picker>
                            </Item>
                        </Item>
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.raceField == '#000000'?colors.formTxtColor:this.state.raceField}}>Race*</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Race"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusRace}
                                  onValueChange={(value)=>this.setState({cusRace:value, raceField:value==''?'#f00':'#000'})}
                                >
                                    <Picker.Item key={'default_race'} value={''} label={'Select Race'} />
                                    {this.state.raceList.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.value} label={item.name} />
                                    })}
                                </Picker>
                            </Item>                        
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:this.state.emailField == '#000000'?colors.formTxtColor:this.state.emailField}}>Email Address*</Label>
                            <Input onChangeText={(value) => {this.setState({cusEmail:value})}} value={this.state.cusEmail} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>
                        <Item style={{flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Marital status</Label>
                            <Item style={{paddingLeft:10, borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Marital Status"
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.cusMaritalStatus}
                                  onValueChange={(value)=>this.setState({cusMaritalStatus:value})}
                                >
                                  <Picker.Item label="Unknown" value="0" />
                                  <Picker.Item label="Single" value="1" />
                                  <Picker.Item label="Married" value="2" />
                                </Picker>
                            </Item>
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>How are you affiliated with us?</Label>
                            <Input onChangeText={(value) => {this.setState({cusRelationship:value})}} value={this.state.cusRelationship} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Remarks</Label>
                            <Form style={{width:'100%'}}>
                                <Textarea onChangeText={(value) => {this.setState({cusRemark:value})}} value={this.state.cusRemark} rowSpan={5} style={{borderColor:colors.formTxtColor}} bordered />
                            </Form>
                        </Item>
                        {/*<Item style={{alignSelf:'center', paddingTop:10, paddingBottom:10, borderColor:'transparent'}}>
                            <Button style={styles.defaultBtn} onPress={()=>this.purchaserInfoValidation()}><Text>Next</Text></Button>
                        </Item>*/} 
                    </Content>:<View/>
                    }
                    <Item style={{marginTop:5, paddingLeft:5, backgroundColor:colors.btnColor, height:50, borderColor:this.state.cusAddressCountryCode != '' && this.state.cusCity != '' && this.state.cusStreet != '' && this.state.cusCallingCode != ''? '#0f0':'#f00'}}> 
                        <Item style={{width:'90%', borderColor:'transparent'}}>
                            <Label style={{color:colors.bgColor}}>Contact*</Label>
                        </Item>
                        <TouchableOpacity >
                             {this.state.contactDetail?
                             <Icon onPress={()=>this.state.contactDetail?this.setState({contactDetail:false}):this.setState({contactDetail:true, purchaserDetail:false})} style={{alignSelf:'flex-end'}} name='minus' type='EvilIcons' />:<Icon onPress={()=>this.state.contactDetail?this.setState({contactDetail:false}):this.setState({contactDetail:true, purchaserDetail:false})} style={{alignSelf:'flex-end'}} name='plus' type='EvilIcons'/>}
                        </TouchableOpacity>
                    </Item>
                    {this.state.contactDetail?
                      <View style={{flex:1}}>{contact}</View>:<View/>
                    }                                    
                  </Container>      
              )
      }
    
      !this.state.individual && !this.state.corporate?
      searchCus = (
          <View style={{flex:1, flexDirection:'column', justifyContent:'center', padding:'5%', backgroundColor:colors.bgColor}}>
            <Button style={styles.defaultBtn} onPress={()=>this.setState({individual:true})}><Text style={{color:colors.btnTxtColor, textAlign:'center'}}>Individual Purchaser</Text></Button>
            {this.state.individual?<View/>:<Button style={[styles.defaultBtn, {marginTop:10}]} onPress={()=>this.setState({corporate:true})}><Text style={{color:colors.btnTxtColor, textAlign:'center'}}>Corporate Purchaser</Text></Button>}   
          </View>
      ):
      searchCus = (this.state.searchResult.length>0?
          <View style={{flex:1, flexDirection:'column', justifyContent:'flex-start', padding:10, backgroundColor:colors.bgColor}}>
              <FlatList extraData={this.state.searchResult} bounces={false} data={this.state.searchResult}
                renderItem={({item}) =>(
                  <TouchableOpacity onPress={()=>this.postRequestGetCustomerDetail(item.value)} >
                      <Card>
                        <CardItem> 
                            <Text style={{width:'30%'}}>Name</Text>  
                            <Text style={{width:'70%'}}>: {item.name}</Text>  
                        </CardItem>    
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>IC/Passport</Text>  
                            <Text style={{width:'70%'}}>: {item.nationalId}({item.nationalityCode})</Text>  
                        </CardItem>    
                        <CardItem>
                            <Text style={{width:'30%'}}>Contact</Text>  
                            <Text style={{width:'70%'}}>: {item.phoneNumbers}</Text>  
                        </CardItem>    
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%', alignSelf:'flex-start'}}>Role</Text>  
                            <Text style={{width:'70%'}}>: {item.serverNames}</Text>  
                        </CardItem> 
                      </Card> 
                  </TouchableOpacity>
                )}
                keyExtractor={(item, index) => 'discount' +index}
              />                                                                
              <Button onPress={()=>this.setState({individual:false, corporate:false, searchResult:[]})} transparent style={{marginTop:10, alignSelf:'center'}}>
                  <Text style={{color:colors.btnColor}}>Back</Text>
              </Button>                                 
          </View>               
          :<View style={{flex:1, flexDirection:'column', justifyContent:'center', backgroundColor:colors.bgColor}}>
              <Item rounded style={{borderColor:colors.btnColor, marginLeft:'10%', marginRight:'10%'}}>
                <Icon name="ios-search" style={{color:colors.btnColor}}/>
                <Input placeholder="NRIC, phone, email..." placeholderTextColor='#ffffff' style={{color:colors.txtColor}}  value={this.state.cusIC} onChangeText={(value) => {this.setState({cusIC:value})}}/>
              </Item>
              <Button style={{alignSelf:'center'}} transparent onPress={this.postRequestFindCustomer.bind(this)}>
                <Text style={{color:colors.btnColor}}>Search</Text>
              </Button>
              {this.state.cancelAdd?
              <Button transparent style={{alignSelf:'center'}} onPress={()=>this.setState({userExisted:true})}><Text style={{color:colors.btnColor}}>Back</Text></Button>:
              <Button style={{alignSelf:'center'}} transparent onPress={()=>this.setState({corporate:false, individual:false})}>
                <Text style={{color:colors.btnColor}}>Back</Text>
              </Button>}                
          </View>
      )
  
    return (
      <Container>
        <Header style={styles.headerOverlap}>
          <Left>
            <TouchableOpacity onPress={() => this.state.purchaserOption? this.setState({purchaserOption:false}):goBack()}>
                <Icon name="back" type="Entypo" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={{color:colors.formTxtColor}}>Purchasers</Title>
          </Body>
          <Right>                     
          </Right>
        </Header>
        {this.state.userExisted?
          <View style={{flex:1, flexDirection:'column', justifyContent:'flex-start', padding:10, backgroundColor:colors.bgColor}}>
              <FlatList extraData={this.state.salePartyDetail} bounces={false} data={this.state.salePartyDetail}
                renderItem={({item}) =>(
                <FlatList extraData={this.state.salePartyDetail} bounces={false} data={item.parties}
                    renderItem={({item:item2, index}) =>(
                      <Card>
                        <CardItem>
                            <Text style={{width:'90%'}}>Owner {index+1}</Text> 
                            <Icon name='cancel' type='MaterialIcons' style={{fontSize:25}} onPress={()=>this.removeSalePartyAlert(item2)}/>
                        </CardItem>
                        <CardItem style={{backgroundColor:colors.lightGray}}> 
                            <Text style={{width:'30%'}}>Unit Number </Text>  
                            <Text style={{width:'70%'}}>: {item.unitNumber}</Text>  
                        </CardItem> 
                        <CardItem> 
                            <Text style={{width:'30%'}}>Name</Text>  
                            <Text style={{width:'70%'}}>: {item2.fullName}</Text>  
                        </CardItem>    
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>IC/Passport</Text>  
                            <Text style={{width:'70%'}}>: {item2.nationalId}({item2.nationalityName})</Text>  
                        </CardItem>    
                        <CardItem>
                            <Text style={{width:'30%'}}>Contact</Text>  
                            <Text style={{width:'70%'}}>: {this.getContactNumber(item2.phones)}</Text>
                        </CardItem>    
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Email Address</Text>  
                            <Text style={{width:'70%'}}>: {item2.emailAddress}</Text>  
                        </CardItem> 
                      </Card> 
                    )}
                    keyExtractor={(item, index) => 'item' +index}
                  />   
                )}
                keyExtractor={(item, index) => 'saleParty' +index}
              />                                                                
              <Button onPress={()=>this.proceed()} style={[styles.defaultBtn,{marginTop:10, alignSelf:'center'}]}>
                  <Text style={{color:colors.btnTxtColor}}>Continue</Text>
              </Button>            
              {this.state.corporate?<View/>:
              <Button onPress={()=>this.setState({userExisted:false, individual:true})} transparent style={{marginTop:10, alignSelf:'center'}}>
                  <Text style={{color:colors.btnColor}}>Add Purchaser</Text>
              </Button>}                                 
          </View>:
          <View style={{flex:1}}>
            {this.state.purchaserOption?
                    <View style={{flex:1}}>{purchasers}           
                        <Item style={{alignSelf:'center', paddingTop:10, paddingBottom:10, borderColor:'transparent'}}>
                            <Button style={styles.defaultBtn} onPress={()=>this.purchaserInfoValidation()}><Text style={{color:colors.btnTxtColor}}>Add</Text></Button>
                        </Item>
                    </View>:<View style={{flex:1}}>{searchCus}</View>
            }
                  
          </View>    
        }
      </Container>
    );
  }
}

                            
function mapStateToProps(state) {
    return {
        token: state.token, 
        permission:state.permission  
    }
}

export default connect(mapStateToProps)(purchaserPage);