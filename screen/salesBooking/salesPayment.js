import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Title, Icon, Button, Item, Label, Form, Input, Textarea, Picker, Badge, DatePicker } from "native-base";
import { TouchableOpacity, Alert, FlatList, Dimensions, View, ActivityIndicator, Image, Platform, Modal} from 'react-native'
import {styles, colors} from '../globalStyle.js'
import ImageViewer from 'react-native-image-zoom-viewer';
import {api_getUnitSalePayment, api_submitReservation, api_getUnitSalesSummaries, api_createSalesPayment, api_deleteSalesPayment, api_uploadImage, getPaymentImage, api_sendSalesSubmmisionEmail} from '../config.js'
import {connect} from 'react-redux'
import Moment from 'moment'
import SignatureCapture from 'react-native-signature-capture';
import ImagePicker from 'react-native-image-picker'

const images = []

//3200 * 3200
class salesPayment extends Component {
    
    constructor(props){
        super(props)    
        this.state = {
            imageViewer:false,
            newPayment:true,
            salesId:[],
            unitDetail:[],
            purposeList:['Deposit','Progress Payment'],
            methodList:['null','Cash','Cheque','Credit Card', 'Debit Card', 'Online Transfer', 'Telegraphic Transfer', 'Payment Gateway', 'Credit Note', 'Bank Draft', 'Alliance 3D Secure', 'PayPal', 'SinoPay'],
            cardType:['Master Card', 'Visa', 'Amex', 'UnionPay', 'Other'],
            sourceList:['Purchaser', 'End Financier', 'Credit Note'],
            approvalList:['Pending Approval', 'Approved'],
            addPayment:false,
            selectedDate:new Date(),
            selectedBankInDate:new Date(),
            selectedPurpose:1,
            selectedMethod:1,
            selectedCardType:1,
            referenceNumber:'',
            instrumentNumber:'',
            cardHolderName:'',
            cardExpiry:'',
            cardExpMon:'',
            cardExpYear:'',
            bankAccountList:[],
            bankAccountId:0,
            paymentAmount:'',
            selectedSource:0,
            selectedApprovalStatus:false,
            remark:'',
            imageData:'',
            showImage:false,
            imageUri:{},
            imageFileName:'',
            editable:true,
            selectedSaleId:0,
            paymentId:0,
            updatePaymentImage:false,
            submitPayment:false
            
            
        }
    }
    
    componentDidMount(){
        let idList = this.props.navigation.getParam('salesId', []);
        this.setState({
            salesId:idList,
            submitPayment:this.props.navigation.getParam('submitPayment', false)
        })
        if(this.props.navigation.getParam('submitPayment', false)){
            this.setState({
                addPayment:true,
                selectedSaleId:idList[0],
            })          
            this.postRequestGetPayment(idList[0], null);
        }
        else{
            this.postRequestGetUnitSalesSummary()
        }

    }
    
    componentWillUnmount(){
        this.state = {}    
    }
    
    postRequestGetUnitSalesSummary(){
        let {navigation} = this.props;
  
        this.setState({isLoading:true});
        return fetch(api_getUnitSalesSummaries, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: navigation.getParam('salesId',[])
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;

                if(result != null){
                    let unitSales = result.unitSales;
                    let units = []
//                    alert(JSON.stringify(unitSales))
                    for(let i=0; i<unitSales.length; i++){
                        units.push({id:unitSales[i].id,
                                    unitNumber:unitSales[i].unitNumber,
                                    listPrice:unitSales[i].listPrice,
                                    discount:unitSales[i].totalDiscount,
                                    spaPrice:unitSales[i].spaPrice,
                                    paymentList:unitSales[i].payments,
                                    totalPayment:unitSales[i].totalPayment,
                                    totalDeposit:unitSales[i].totalDeposit,
                           
                                   })
                    }
                    this.setState({
                        unitDetail:unitSales.length > 0?units:[]
                    })
                }  
                else{
//                    alert(JSON.stringify(responseJson))
                    alert('Internal Server fail to retrieve unit sale summary for sales payment.')
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
//            console.error(error);
          });          
    }    
    
    postRequestGetPayment(id, payment){

        this.setState({isLoading:true});
        return fetch(api_getUnitSalePayment, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleId: id,
              unitSalePaymentId:payment == null?payment:payment.id 
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
                
                if(result != null){
             
                    let paymentDetail = result.unitSalePayment;
//                           alert(JSON.stringify(paymentDetail.cardExpiry))
                    this.setState({
                        bankAccountList:result.bankAccounts,
                        paymentAmount:paymentDetail.amount == 0?'':''+paymentDetail.amount,
                        bankAccountId:paymentDetail.bankAccountId,
                        editable:!paymentDetail.isApproved,
                        selectedBankInDate:new Date(paymentDetail.bankInDate),
                        cardExpMon:paymentDetail.cardExpiry == null?'':Moment(new Date(paymentDetail.cardExpiry)).format('MM'),
                        cardExpYear:paymentDetail.cardExpiry == null?'':Moment(new Date(paymentDetail.cardExpiry)).format('YY'),
                        cardHolderName:paymentDetail.cardholderName,
                        instrumentNumber:paymentDetail.instrumentNumber,
                        selectedApprovalStatus:paymentDetail.isApproved,
                        selectedMethod:paymentDetail.method == 0?1:paymentDetail.method,
                        selectedDate:new Date(paymentDetail.paymentDate),
                        selectedPurpose:paymentDetail.purpose,
                        selectedCardType:paymentDetail.cardType == null? 1:paymentDetail.cardType,
                        referenceNumber:paymentDetail.referenceNumber,
                        remark:paymentDetail.remarks,
                        selectedSource:paymentDetail.source,
                        imageUri:result.instrumentPictureId == null?{}:{uri:getPaymentImage+result.instrumentPictureId},
                        imageData:result.instrumentPictureId,
                        paymentId:paymentDetail.id,
                        showImage:result.instrumentPictureId == null?false:true,
                        updatePaymentImage:false
                        
                    })
                    
//                    alert(JSON.stringify(result.instrumentPictureId))
                }  
                else{
//                    alert(JSON.stringify(responseJson))
                    alert('Internal Server fail to retrieve unit sale summary for sales payment.')
//                    alert(payment)
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
//            console.error(error);
          });          
    }   
    
    postRequestCreatePayment(){
        this.setState({isLoading:true});
        return fetch(api_createSalesPayment, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              payment: {
                unitSaleId: this.state.selectedSaleId,
                amount: this.state.paymentAmount,
                canEdit: false,
                purpose: this.state.selectedPurpose,
                method: this.state.selectedMethod,
                cardType: this.state.selectedCardType,
                source: this.state.selectedSource,
                instrumentNumber: this.state.instrumentNumber,
                paymentDate: this.state.selectedDate,
                cardholderName: this.state.cardHolderName,
                cardExpiry: this.state.cardExpMon != '' && this.state.cardExpYear != ''?Moment(new Date("20"+this.state.cardExpYear, this.state.cardExpMon , 1)).subtract(1, 'days'):null,
                bankAccountId: this.state.bankAccountId,
                bankInDate: this.state.selectedBankInDate.getFullYear() == 1970?null:this.state.selectedBankInDate,
                referenceNumber: this.state.referenceNumber,
                remarks: this.state.remark,
                isApproved: this.state.selectedApprovalStatus,
                id: this.state.paymentId
              },
              paymentPicture:this.state.updatePaymentImage?{
                originalFileName: "image1.jpeg",
                fileName: this.state.imageFileName,
                height: 0,
                width: 0,
                x: 0,
                y: 0
              }:null,
              stageId: -1
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.success;
                
                if(result){
                    if(this.state.submitPayment){

                        this.postRequestSendSaleSubmissionEmail()
                         
//                        this.props.navigation.navigate('salesList', {hideTabBar:true})
                    }
                    else{
                        this.postRequestGetUnitSalesSummary()
                        this.setState({isLoading:false}) 
                        Alert.alert(              
                         'Add Successfully',
                         'Payment had been added in, you may edit or view back later.',
                         [
                             {text:'Noted', onPress:this.setState({addPayment:false}), style:'cancel'}
                         ],
                         {cancelable:false}
                        )                        
                    }


                }
                else{
                     this.setState({isLoading:false})                 
//                    alert(JSON.stringify(responseJson))
                    alert('Fail to create payment, please try again.')
                    this.props.navigation.navigate('salesList', {hideTabBar:true})
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            alert('Internal Server fail to create sales payment, please contact the administrator.')
//            console.error(error);
          });          
    }

    postRequestSendSaleSubmissionEmail(){
        this.setState({isLoading:true});
        return fetch(api_sendSalesSubmmisionEmail, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds:this.state.salesId
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.success;
                
                if(result){
                    this.props.setRefresh(true);
                    this.props.setRefreshSaleList(true);    
                    this.setState({addPayment:false, submitPayment:false, isLoading:false});
                    Alert.alert(
                        'Sales Submitted',
                        'You may review back all this informations in sale listing page',
                        [
                            {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                        ],
                        {cancelable:false}
                    )    
                    this.postRequestGetUnitSalesSummary();
                }
                else{
//                    this.setState({isLoading:false})                 
                    let err = responseJson.error
                    alert('Fail to send sales submission email, kindly contact the administrator. '+err.message!= undefined?err.message:'')
                    this.props.setRefresh(true);
                    this.props.setRefreshSaleList(true);    
                    this.setState({addPayment:false, submitPayment:false, isLoading:false});
                    Alert.alert(
                        'Sales Submitted',
                        'You may review back all this informations in sale listing page',
                        [
                            {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                        ],
                        {cancelable:false}
                    )    
                    this.postRequestGetUnitSalesSummary();                    
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            alert('Internal Server fail to send sales submission email, please contact the administrator.')
//            console.error(error);
          });          
    } 
    
    postRequestSubmitUnitSale(){
        this.setState({isLoading:true});
        return fetch(api_submitReservation, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: this.state.salesId,
              additionalBankerBranchIds: []
            })
        })
          .then((response) => response.json())
          .then((responseJson) => {
//                this.setState({isLoading:false})
                let result = responseJson.success;
     
                if(result == true){

//                    this.setState({isLoading:false})
                    this.postRequestCreatePayment()

                   
                }    
                else{
                    this.setState({isLoading:false});
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
//            console.error(error);
            alert('Fail to submit sales, kindly check your internet status and try again, if this issue still persist you may request assist from administrator.')
          });          
    }      
      
    postRequestUploadImage(image){
        
        this.setState({isLoading:true})
        
        const formdata = new FormData();
        formdata.append('file', {uri: Platform.OS === "android" ? image.uri : image.uri.replace("file://", ""), type: image.type, name: 'image1.jpeg'});
        formdata.append('objectType', 'PaymentPicture');

        return fetch(api_uploadImage, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            Authorization: 'bearer '+this.props.token
          },            
          body:formdata
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
     
                if(result != null){
                    Alert.alert(
                        'Uploaded Image',
                        'To reupload new image just simply click upload button again.',
                        [
                            {text:'Noted', onPress:console.log('cancenl'), style:'cancel'}
                        ],
                        {cancelable:false}
                    )
                    this.setState({imageFileName:result.fileName, updatePaymentImage:true, showImage:true})
                                        
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            alert(JSON.stringify(error))
//            console.error(error);
          });          
    }      
    
    configData(item, payment){
        this.setState({
            addPayment:true,
            selectedSaleId:item.id,
        })
        payment != null?this.setState({newPayment:false}):this.setState({newPayment:true})
        this.postRequestGetPayment(item.id, payment);
    }
    
    uploadImage(){
         ImagePicker.showImagePicker({
              title: 'Select Upload Option',
              takePhotoButtonTitle:'Take Photo',
              chooseFromLibraryButtonTitle:'Upload From Gallery',
              maxWidth:1000,
              maxHeight:1000,
              quality:1,
//              storageOptions: {
//                skipBackup: true,
//                cameraRoll: false
//              },
          }, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            const source = { uri: response.uri };

            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      
            this.setState({
              imageUri: source,
              imageData: response.data
            })
            
            this.postRequestUploadImage(response);
          }
        });         
    }    
    
    validation(){
        let msg = "";
        if(this.state.paymentAmount == ""){
            msg = "Amount field cannot be blank."
        }
        else if(this.state.imageData == '' || this.state.imageData == null){
            msg = "Please provide at least one supporting document image."
        }
        else if(this.state.bankAccountId == 0){
            msg = "Please choose at least one bank account before submit."
        }
        else if(this.state.selectedMethod == 2 || this.state.selectedMethod == 6 || this.state.selectedMethod == 8 || this.state.selectedMethod == 9){
            if(this.state.instrumentNumber == '' || this.state.instrumentNumber == null){
                msg = "Please provide cheque/card/instrument number before submit."
            }
        }
        else if(this.state.selectedMethod == 3 || this.state.selectedMethod == 4){
//            let cardExp = this.state.cardExpiry.replace(/\D/g,'')
            
            if(this.state.cardHolderName == '' || this.state.cardHolderName == null){
                msg = "Please provide cardholder name before submit."
            }
            else if(this.state.instrumentNumber == '' || this.state.instrumentNumber == null){
                msg = "Please provide cheque/card/instrument number before submit."
            }
            else if(this.state.cardExpMon == '' || this.state.cardExpYear == '' || this.state.cardExpMon == null || this.state.cardExpYear == null){
                msg = "Please provide card expiry date."
            }
        }
        else if(this.state.selectedMethod == 5){
            if(this.state.cardHolderName == '' || this.state.cardHolderName == null){
                msg = "Please provide account holder name before submit."
            }
            else if(this.state.instrumentNumber == '' || this.state.instrumentNumber == null){
                msg = "Please provide cheque/card/instrument number before submit."
            }            
        }
        else if(this.state.referenceNumber == '' || this.state.referenceNumber == null){
            msg = "Please provide reference number before submit."
        }
        
        if(msg == ''){
            Alert.alert(
                this.state.submitPayment?'Submit Payment?':'Update Payment?',
                '',
                [
                    {text:'Cancel', onPress:()=>console.log('cancel'), style:'cancel'},
                    {text:'Yes', onPress:()=>this.state.submitPayment?this.postRequestSubmitUnitSale():this.postRequestCreatePayment()}
                ],
                {cancelable:false}
            )

        }
        else{
            Alert.alert(
                "Invalid information",
                msg,
                [
                    {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                ],
                {cancelable:false}
            )            
        }

    }
    
    viewImage(uri){
        images = [];
        images.push({
              url:uri,
              width:300,
              height:300,
              props:{
                  onError:()=>alert('Image fail to retrieve, please check your internet connection and try again later.')
              }
          })
//        alert(JSON.stringify(images))
        this.setState({imageViewer:true})
    }
    
    salesPaymentBackNav(){
        if(this.state.addPayment && !this.state.submitPayment){
            this.setState({addPayment:false})
        }
        else{
            this.props.navigation.navigate('salesList', {hideTabBar:true})
        }
    }
    
  render() {
    let {navigate, goBack} = this.props.navigation;
    
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    }       
      
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            <TouchableOpacity onPress={() => this.salesPaymentBackNav()}>
                <Icon name="back" type="Entypo" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={{color:colors.formTxtColor, textAlign:'center'}}>Sales Payment</Text>
          </Body>
          <Right>                     
          </Right>
        </Header>
        {!this.state.addPayment?
        <Content padder>
           <Modal visible={this.state.imageViewer} transparent={false}  onRequestClose={() => {}}>
                <ImageViewer renderImage={(props) => <Image {...props} />} loadingRender={()=><ActivityIndicator size="large" color="#ffffff" />} enablePreload={true} imageUrls={images} style={{backgroundColor:colors.bgColor}}/>
                <Button style={{backgroundColor:colors.btnColor}} primary block onPress={()=>{this.setState({imageViewer:false})}}><Text style={{color:colors.btnTxtColor}}>Back</Text></Button>
            </Modal>             
            <FlatList extraData={this.state.unitDetail} bounces={false} data={this.state.unitDetail}
                renderItem={({item}) =>(
                    <Card>
                        <CardItem header style={{backgroundColor:colors.btnColor}}>
                            <Text>{item.unitNumber}</Text>
                        </CardItem>
                        <CardItem style={{flexDirection:'column', flexWrap:'wrap'}}>
                            <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent'}}>
                                <Text style={{width:'50%', justifyContent:'flex-start'}}>List Price</Text>
                                <Text style={{width:'50%', justifyContent:'flex-start'}}>RM {item.listPrice.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                            </Item>
                            <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent'}}>
                                <Text style={{width:'50%', justifyContent:'flex-start', flexDirection:'column', alignSelf:'flex-start'}}>Discount</Text>
                                <Text style={{width:'50%', justifyContent:'flex-start', flexWrap:'wrap'}}>RM {item.discount == 0?0:item.discount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                            </Item>
                            <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent'}}>
                                <Text style={{width:'50%', justifyContent:'flex-start', backgroundColor:'transparent'}}>SPA Price</Text>
                                <Text style={{width:'50%', justifyContent:'flex-start', backgroundColor:'transparent'}}>RM {item.spaPrice.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                            </Item>
                        </CardItem>
                        <CardItem header style={{backgroundColor:colors.btnColor}}>
                            <Text>Payments</Text>
                        </CardItem>
                        <CardItem style={{flexDirection:'column', flexWrap:'wrap'}}>
                            <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent'}}>
                                <Text style={{width:'50%', justifyContent:'flex-start'}}>Total Payment(s)</Text>
                                <Text style={{width:'50%', justifyContent:'flex-start'}}>RM {item.totalPayment.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                            </Item>
                            <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent', backgroundColor:'transparent'}}>
                                <Text style={{width:'50%', justifyContent:'flex-start', flexDirection:'column', alignSelf:'flex-start'}}>Total Deposit</Text>
                                <Text style={{width:'50%', justifyContent:'flex-start', flexWrap:'wrap'}}>RM {item.totalDeposit.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                            </Item>

                        </CardItem>
                        <CardItem header style={{backgroundColor:colors.btnColor}}>
                            <Text>Payments List</Text>
                        </CardItem> 
                        {this.state.addPayment?<View/>:
                        <Button style={[styles.defaultBtn,{margin:10}]} onPress={()=>this.configData(item, null)}>
                            <Text style={{color:colors.btTxtColor, textAlign:'center'}}>Add Offline Payment</Text>
                        </Button>        
                        }                             
                        <FlatList extraData={item.paymentList} bounces={false} data={item.paymentList}
                            renderItem={({item:item2, index}) =>(
                                <CardItem style={{flexDirection:'column', flexWrap:'wrap', backgroundColor:index%2>0?colors.lightGray:'#fff'}}>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent'}}>
                                        <Text style={{backgroundColor:item2.isApproved?'#0f0':'#f00', padding:5, width:'100%'}}>{item2.isApproved?this.state.approvalList[1]+' by':this.state.approvalList[0]} {item2.approvedByDisplayName}</Text>
                                    </Item>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'#000'}}>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>Date</Text>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>{Moment(new Date(item2.paymentDate)).format('DD/MM/YYYY')}</Text>
                                    </Item>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'#000', backgroundColor:'transparent'}}>
                                        <Text style={{width:'50%', justifyContent:'flex-start', flexDirection:'column', alignSelf:'flex-start'}}>Purpose</Text>
                                        <Text style={{width:'50%', justifyContent:'flex-start', flexWrap:'wrap'}}>{this.state.purposeList[item2.purpose]}</Text>
                                    </Item>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'#000'}}>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>Instrument</Text>
                                        <Text style={{width:'50%', justifyContent:'flex-start'}}>{this.state.methodList[item2.method]}</Text>
                                    </Item>
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'#000', backgroundColor:'transparent'}}>
                                        <Text style={{width:'50%', justifyContent:'flex-start', flexDirection:'column', alignSelf:'flex-start'}}>Amount</Text>
                                        <Text style={{width:'50%', justifyContent:'flex-start', flexWrap:'wrap'}}>RM {item2.amount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} ({item2.amount % item.spaPrice > 0?(item2.amount/item.spaPrice).toFixed(2):(item2.amount/item.spaPrice)}%)</Text>
                                    </Item>
                                    {item2.instrumentPictureId != null?
                                    <Item style={{width:Dimensions.get('window').width, paddingLeft:'5%', paddingRight:'5%', borderColor:'transparent', backgroundColor:'transparent', justifyContent:'center', flexDirection:'column'}}>
                                        <Text style={{alignSelf:'flex-start'}}>Supporting Doc</Text>
                                        <TouchableOpacity onPress={()=>this.viewImage(getPaymentImage+item2.instrumentPictureId)}>
                                            <Image source={{uri:getPaymentImage+item2.instrumentPictureId}} style={{width:150, height:150, marginTop:10, marginBottom:10}} />
                                        </TouchableOpacity>
                                    </Item>:<View/>}
                                         
                                    <Item style={{width:Dimensions.get('window').width, padding:'3%', borderColor:'transparent', backgroundColor:'transparent', justifyContent:'center'}}>
                                        <Button style={[styles.defaultBtn, {height:35}]} onPress={()=>this.configData(item, item2)}>
                                            <Text style={{color:colors.btnTxtColor}}>Show Detail</Text>
                                        </Button>
                                    </Item>                                        
                                </CardItem> 
                            )}
                            keyExtractor={(item, index) => 'payment' +index}
                        />                        
                    </Card>                        
                )}
                keyExtractor={(item, index) => 'unit' +index}
            />
        </Content>:
        <Content padder style={{backgroundColor:'#fff'}} bounces={false}>
           <Modal visible={this.state.imageViewer} transparent={false}  onRequestClose={() => {}}>
                <ImageViewer renderImage={(props) => <Image {...props} />} loadingRender={()=><ActivityIndicator size="large" color="#ffffff" />} enablePreload={true} imageUrls={images} style={{backgroundColor:colors.bgColor}}/>
                <Button style={{backgroundColor:colors.btnColor}} primary block onPress={()=>{this.setState({imageViewer:false})}}><Text style={{color:colors.btnTxtColor}}>Back</Text></Button>
            </Modal>             
            <Item style={{width:'100%', borderColor:'transparent'}}>
                <Text>Payment Date* (DD/MM/YYYY)</Text>
            </Item>
            <Item style={{width:'100%', justifyContent:'flex-start', borderColor:'transparent', backgroundColor:colors.lightGray, height:30}}>
                <DatePicker
                    disabled={!this.state.editable}
                    defaultDate={new Date()}
                    minimumDate={new Date(2019, 1, 1)}
                    maximumDate={new Date(2109, 1, 1)}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={Moment(new Date()).format('DD/MM/YYYY')}
                    textStyle={{ color: "#000" }}
                    placeHolderTextStyle={{ color: "#000" }}
                    onDateChange={(newDate)=>this.setState({selectedDate:newDate})}
                    disabled={false}
                    />  
            </Item> 
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start'}}>Purpose*</Text>
            </Item>  
            <Item style={{width:'100%', justifyContent:'flex-end', borderColor:'transparent', backgroundColor:colors.lightGray, height:30}}>
                <Picker
                  enabled={this.state.editable}
                  mode="dropdown"
                  iosHeader="Select Purpose"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.selectedPurpose}
                  onValueChange={(value)=>this.setState({selectedPurpose:value})}
                >
                    <Picker.Item value={0} label='Deposit' />
                    <Picker.Item value={1} label='Progress Payment' />
                </Picker>                    
            </Item>
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start'}}>Method*</Text>
            </Item>  
            <Item style={{width:'100%', justifyContent:'flex-end', borderColor:'transparent', backgroundColor:colors.lightGray, height:30}}>
                <Picker
                  enabled={this.state.editable}
                  mode="dropdown"
                  iosHeader="Select Method"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.selectedMethod}
                  onValueChange={(value)=>this.setState({selectedMethod:value})}
                >
                    <Picker.Item value={1} label='Cash' />
                    <Picker.Item value={2} label='Cheque' />
                    <Picker.Item value={3} label='Credit Card' />
                    <Picker.Item value={4} label='Debit Card' />
                    <Picker.Item value={5} label='Online Transfer' />
                    <Picker.Item value={6} label='Telegraphic Transfer' />
                    <Picker.Item value={7} label='Payment Gateway' />
                    <Picker.Item value={8} label='Credit Note' />
                    <Picker.Item value={9} label='Bank Draft' />
                    <Picker.Item value={10} label='Alliance 3D Secure' />
                    <Picker.Item value={11} label='PayPal' />
                    <Picker.Item value={12} label='SinoPay' />
                </Picker>                    
            </Item>
            {this.state.selectedMethod == 3 || this.state.selectedMethod == 4?(
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start', color:this.state.selectedCardType == null || this.state.selectedCardType == ''?'#f00':'#000'}}>Card Type*</Text>
            </Item>):<View/>}  
            {this.state.selectedMethod == 3 || this.state.selectedMethod == 4?(
            <Item style={{width:'100%', justifyContent:'flex-end', borderColor:'transparent', backgroundColor:colors.lightGray, height:30}}>
                <Picker
                  enabled={this.state.editable}
                  mode="dropdown"
                  iosHeader="Select Card Type"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.selectedCardType}
                  onValueChange={(value)=>this.setState({selectedCardType:value})}
                >
                    <Picker.Item value={1} label='Master Card' />
                    <Picker.Item value={2} label='Visa' />
                    <Picker.Item value={3} label='Amex' />
                    <Picker.Item value={4} label='UnionPay' />
                    <Picker.Item value={255} label='Other' />
                </Picker>                    
            </Item>):<View/>}                

            {this.state.selectedMethod == 3 || this.state.selectedMethod == 4 || this.state.selectedMethod == 5?
            (<Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start', color:this.state.cardHolderName == '' || this.state.cardHolderName == null?'#f00':'#000'}}>{this.state.selectedMethod == 5?'Account Holder Name*':'Cardholder Name*'}</Text>
            </Item>):<View/>}  
            {this.state.selectedMethod == 3 || this.state.selectedMethod == 4 || this.state.selectedMethod == 5?(                    
            <Item regular style={{backgroundColor:colors.lightGray, height:30}}>
                <Input editable={this.state.editable} onChangeText={(value) => {this.setState({cardHolderName:value})}} value={this.state.cardHolderName} style={{textAlign:'left'}} placeholder='Type here...' />
            </Item>):<View/>} 
            {this.state.selectedMethod != 1 && this.state.selectedMethod != 7 && this.state.selectedMethod != 10 && this.state.selectedMethod != 11 && this.state.selectedMethod != 12?
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start', color:this.state.instrumentNumber == '' || this.state.instrumentNumber == null?'#f00':'#000'}}>Cheque/Card/Instrument Number/Account Number*</Text>
            </Item>:<View/>} 
            {this.state.selectedMethod != 1 && this.state.selectedMethod != 7 && this.state.selectedMethod != 10 && this.state.selectedMethod != 11 && this.state.selectedMethod != 12? 
            <Item regular style={{backgroundColor:colors.lightGray, height:30}}>
                <Input editable={this.state.editable} onChangeText={(value) => {this.setState({instrumentNumber:value})}} value={this.state.instrumentNumber} style={{textAlign:'left'}} placeholder='Type here...' />
            </Item>:<View/>} 
            {this.state.selectedMethod == 4 || this.state.selectedMethod == 3?
            (<Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start', color:this.state.cardExpMon == '' || this.state.cardExpYear == ''?'#f00':'#000'}}>Card expiry*</Text>
            </Item>):<View/>}  
            {this.state.selectedMethod == 4 || this.state.selectedMethod == 3?(
            <Item regular style={{backgroundColor:colors.lightGray, height:30, width:100, flexDirection:'row'}}>
                <Input editable={this.state.editable} maxLength={2} onChangeText={(value) => {this.setState({cardExpMon:value.replace(/\D/g,'')})}} value={this.state.cardExpMon} style={{textAlign:'left'}} placeholder='mm' />
                <Text>/</Text>
                <Input editable={this.state.editable} maxLength={2} onChangeText={(value) => {this.setState({cardExpYear:value.replace(/\D/g,'')})}} value={this.state.cardExpYear} style={{textAlign:'left'}} placeholder='yy' />
            </Item>):<View/>}                 
            {this.state.selectedMethod == 2 || this.state.selectedMethod == 5 || this.state.selectedMethod == 6 || this.state.selectedMethod == 8 || this.state.selectedMethod == 9?
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start'}}>Bank-in Date* (DD/MM/YYYY)</Text>
            </Item>:<View/>} 
            {this.state.selectedMethod == 2 || this.state.selectedMethod == 5 || this.state.selectedMethod == 6 || this.state.selectedMethod == 8 || this.state.selectedMethod == 9?    
            <Item style={{width:'100%', justifyContent:'flex-start', borderColor:'transparent', backgroundColor:colors.lightGray, height:30}}>
                <DatePicker
                    disabled={!this.state.editable}
                    defaultDate={new Date()}
                    minimumDate={new Date(2019, 1, 1)}
                    maximumDate={new Date(2109, 1, 1)}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={Moment(new Date()).format('DD/MM/YYYY')}
                    textStyle={{ color: "#000" }}
                    placeHolderTextStyle={{ color: "#000" }}
                    onDateChange={(newDate)=>this.setState({bankInDate:newDate})}
                    disabled={false}
                    />  
            </Item>:<View/>}   
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start', color:this.state.referenceNumber == '' || this.state.referenceNumber == null?'#f00':'#000'}}>Reference Number*</Text>
            </Item>  
            <Item regular style={{backgroundColor:colors.lightGray, height:30}}>
                <Input  editable={this.state.editable}onChangeText={(value) => {this.setState({referenceNumber:value})}} value={this.state.referenceNumber} style={{textAlign:'left'}} placeholder='Type here...' />
            </Item>                
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start', color:this.state.bankAccountId == 0?'#f00':'#000'}}>Bank Account*</Text>
            </Item>  
            <Item style={{width:'100%', justifyContent:'flex-end', borderColor:'transparent', backgroundColor:colors.lightGray, height:30}}>
                <Picker
                  enabled={this.state.editable}
                  mode="dropdown"
                  iosHeader="Select Bank Acc"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.bankAccountId}
                  onValueChange={(value)=>this.setState({bankAccountId:value})}
                >
                    <Picker.Item value={0} label='Select Bank Acc' />
                    {this.state.bankAccountList.map((item, index)=>{
                        return <Picker.Item key={index} value={item.value} label={item.name} />
                    })}
                </Picker>                
            </Item>
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start', color:this.state.paymentAmount == '' || this.state.paymentAmount == 0?'#f00':'#000'}}>Amount (RM)*</Text>
            </Item>  
            <Item regular style={{backgroundColor:colors.lightGray, height:30}}>
                <Input editable={this.state.editable} onChangeText={(value) => {this.setState({paymentAmount:value.replace(/[^0-9]/g, '')})}} value={this.state.paymentAmount} keyboardType={'numeric'} style={{textAlign:'left'}} placeholder='Type here...' />
            </Item> 
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start'}}>Source*</Text>
            </Item>  
            <Item style={{width:'100%', justifyContent:'flex-end', borderColor:'transparent', backgroundColor:colors.lightGray, height:30}}>
                <Picker
                  enabled={this.state.editable}
                  mode="dropdown"
                  iosHeader="Select Purpose"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.selectedPurpose}
                  onValueChange={(value)=>this.setState({selectedPurpose:value})}
                >
                    <Picker.Item value={0} label='Purchaser' />
                    <Picker.Item value={1} label='End Financier' />
                    <Picker.Item value={3} label='Credit Note' />
                </Picker>                    
            </Item>   
            {this.state.newPayment == true || this.props.permission.filter((x)=>x == "Sales.ManageApprovedPayments").length>0?<Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start'}}>Approval Status*</Text>
            </Item>:<View/>} 
            {this.state.newPayment == true || this.props.permission.filter((x)=>x == "Sales.ManageApprovedPayments").length>0?<Item style={{width:'100%', justifyContent:'flex-end', borderColor:'transparent', backgroundColor:colors.lightGray, height:30}}>
                <Picker
                  enabled={this.state.editable}
                  mode="dropdown"
                  iosHeader="Select Approval Status"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.selectedApprovalStatus}
                  onValueChange={(value)=>this.setState({selectedApprovalStatus:value})}
                >
                    <Picker.Item value={false} label='Pending Approval' />
                    <Picker.Item value={true} label='Approved' />
                </Picker>                    
            </Item>:<View/>}
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start'}}>Remarks</Text>
            </Item>  
            <Item regular style={{backgroundColor:colors.lightGray}}>
                <Textarea editable={this.state.editable} rowSpan={5} style={{width:'100%'}}placeholder='Type here...' onChangeText={(value) => {this.setState({remark:value})}} value={this.state.remark}/>
            </Item>
            <Item style={{width:'100%', borderColor:'transparent', height:50, flexDirection:'column', justifyContent:'flex-end'}}>
                <Text style={{alignSelf:'flex-start', color:this.state.imageData == "" || this.state.imageData == null?'#f00':'#000'}}>Supporting Image*</Text>
            </Item>
            <Item regular style={{backgroundColor:colors.lightGray, flexDirection:'column', justifyContent:'flex-start'}}>
                {this.state.showImage?
                    <TouchableOpacity onPress={()=>this.viewImage(this.state.imageUri.uri)}>                 
                        <Image source={this.state.imageUri} style={{width:150, height:150, marginTop:10, marginBottom:10}} />
                    </TouchableOpacity>:<View/>
                }
                <Button disabled={!this.state.editable} onPress={()=>this.uploadImage()} style={{alignSelf:'center', width:'100%', justifyContent:'center'}}><Text>Upload</Text></Button>
                  
            </Item>    
            <Item style={{borderColor:'#000', justifyContent:'center', paddingTop:10, marginTop:10, borderTopWidth:1, borderBottomWidth:0}}>
                {this.state.submitPayment?<View/>:<Button onPress={()=>this.setState({addPayment:false})} style={{width:'40%', justifyContent:'center', backgroundColor:colors.lightGray}}>
                    <Text style={{color:colors.btnTxtColor}}>{this.state.editable?'Cancel':'Back'}</Text>
                </Button>}
                {this.state.editable?<Button onPress={()=>this.validation()} style={[styles.defaultBtn,{width:'40%'}]}>
                    <Text style={{color:colors.btnTxtColor}}>{this.state.newPayment?'Submit':'Update'}</Text>
                </Button>:<View/>}
            </Item>    
        </Content>
        }
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token,    
        permission:state.permission        
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setRefresh: (tk) => dispatch({type: 'SET_REFRESH_UNIT', param:tk}),
        setRefreshSaleList: (id) => dispatch({type: 'SET_REFRESH_SALESLIST', param:id})
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(salesPayment);