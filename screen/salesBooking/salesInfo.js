import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Radio, CheckBox, DatePicker, Title, ListItem, Icon, Button, Item, Label, Form, Input, Textarea, Picker, Badge } from "native-base";
import { TouchableOpacity, TouchableHighlight, Alert, FlatList, Dimensions, View, ActivityIndicator, Image} from 'react-native'
import {styles, colors} from '../globalStyle.js'

import {api_getUnitSaleForEdit, api_updateUnitSale, api_submitReservation, api_updateSaleFormSignature, api_updateSaleFormImage, 
api_uploadImage} from '../config.js'
import ImagePicker from 'react-native-image-picker';
//import SignaturePad from 'react-native-signature-pad'
//import SignatureCapture from 'react-native-signature-capture';
import {connect} from 'react-redux'
import Moment from 'moment'

class salesInfoPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            salesId:[],
            eventList:[],
            agentList:[],
            solicitorList:[],
            sectionList:[],
            sectionReply:[],
            spaSignDueDate:'',
            bookingDate:'',
            event:0,
            principleAgent:'',
            selectAgent:'',
            coAgent:[],
            solicitor:0,
            referrerName:'',
            referrerIC:'',
            emergencyName:'',
            emergencyContact:'',
            salesRemark:'',
            remark:'',
            unitBlockPhaseId: 0,
            withInteriorDesign: false,
            signatureImageUri:{},
            signatureImageData:'',
            signatureMapId:'',
            formImageUri:{},
            formImageData:'',
            formImageMapId:''
            
        }
    }
    
    componentDidMount(){
        const {navigation} = this.props;
        this.setState({
            eventList:navigation.getParam('event',[]),
            agentList:navigation.getParam('agent',[]),
            solicitorList:navigation.getParam('solicitor',[]),    
            salesId:navigation.getParam('saleID',[]),
            isLoading:true  
        });
        setTimeout(()=>{
            this.postRequestGetUnitSale();

        },500)
        
    }
    
    postRequestGetUnitSale(){
        return fetch(api_getUnitSaleForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id: this.state.salesId[0]
//              id: 41413
          })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
                let section = result.sections;
                if(result != null){
                    let agentName = [];
//                    alert(JSON.stringify(responseJson))
                    if(result.coSellerUserIds.length>0){ 
                        agentName = this.state.agentList.filter((x)=> result.coSellerUserIds.filter((y)=> y == x.value).length>0);
                        agentName = agentName.map((x)=>x.name);
                    }
                    this.setState({
                        spaSignDueDate:Moment(new Date(result.spaSignDueDate)).format('D/M/YYYY'),
                        bookingDate:Moment(new Date(result.salesDate)).format('D/M/YYYY'),
                        salesRemark:result.saleRemarks, 
                        sectionList:section[0] == undefined?[]:section[0].formAnswers,
                        principleAgent: result.mainSellerUserId,
                        unitBlockPhaseId: result.unitBlockPhaseId,
                        withInteriorDesign: result.withInteriorDesign,
                        solicitor: result.solicitorId == null?0:result.solicitorId,
                        event: result.eventId == null?0:result.eventId,
                        referrerName:result.referrerName,
                        referrerIC:result.referrerNationalId,
                        coAgent: agentName
                    })
                }    
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    } 
    
//    postRequestSubmitUnitSale(){
//        return fetch(api_submitReservation, {
//          method: 'POST',
//          headers: {
//            Accept: 'application/json',
//            'Content-Type': 'application/json',
//            Authorization: 'bearer '+this.props.token
//          },
//          body: JSON.stringify({
//              unitSaleIds: this.state.salesId,
//              additionalBankerBranchIds: []
//            })
//        })
//          .then((response) => response.json())
//          .then((responseJson) => {
//                this.setState({isLoading:false})
//                let result = responseJson.success;
//     
//                if(result == true){
//                    this.props.setRefresh(true);
//                    this.proceed();
//                }    
//                else{
//                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
//                }
//
//          })
//          .catch((error) =>{
//            this.setState({isLoading:false})
//            console.error(error);
//          });          
//    }    
    
    postRequestUpdateUnitSale(){
        this.setState({isLoading:true})
        let agentId = this.state.agentList.filter((x)=>this.state.coAgent.filter((y)=>y == x.name).length > 0)
        agentId = agentId.map((x)=> x.value)

        return fetch(api_updateUnitSale, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSale: {
                salesDate: this.state.bookingDate,
                spaSignDueDate: this.state.spaSignDueDate,
                solicitorId: this.state.solicitor==0?'':this.state.solicitor,
                eventId: this.state.event==0?'':this.state.event,
                saleRemarks: this.state.salesRemark,
                newSaleRemarks: this.state.remark,
                referrerName: this.state.referrerName,
                referrerNationalId: this.state.referrerIC,
                referrerCommission: 0,
                isReferrerCommissionPercentage: false,
                isReferrerCommissionIndependent: false,
                referrerCommissionBaseType: 0,
                mainSellerUserId: this.state.principleAgent,
                unitBlockPhaseId: this.state.unitBlockPhaseId,
                withInteriorDesign: this.state.withInteriorDesign,
                coSellerUserIds: agentId,
                sections: [
                  {
                    label: "string",
                    "formAnswers": this.state.sectionList
                  }
                ],
                id: this.state.salesId[0]
              },
              unitSaleIds: this.state.salesId,
              withInteriorDesignUnitSaleIds: []
            })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.success;
                if(result == true){
                    this.props.setRefresh(true);
                    
                    
                    this.proceed();
//                    this.postRequestSubmitUnitSale();
                }
                else{
                    alert("Fail to submit sales info, please contact administrator.")
//                    alert(JSON.stringify(responseJson))
                }
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }  
    
    postRequestUploadSaleFormSignature(){
        return fetch(api_updateSaleFormSignature, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitSaleIds: this.state.salesId,
              mappingId: this.state.signatureMapId,
              dataUrl: "data:image/png;base64,"+this.state.signatureImageData
            })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
     
                if(result != null){
                    let saleForm = this.state.sectionList;
                    for(let i=0; i<saleForm.length; i++){
                        if(saleForm[i].mappingId == this.state.signatureMapId){
                            saleForm[i].value = result
                        }
                    }
                    this.setState({
                        sectionList:saleForm
                    })
                    
                    Alert.alert(
                        'Successfully Uploaded',
                        '',
                        [
                            {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                        ],
                        {cancelable:false}
                    )
                }    
                else{
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    alert('Upload signature failed. Please try again.')                    
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestUploadSaleFormImage(){
  
        
        const formdata = new FormData();
        formdata.append('file', {uri: "data:image/png;base64,"+this.state.formImageData, type: 'image/jpeg', name: 'image1.jpeg'});
        formdata.append('objectType', 'FieldImage');

        return fetch(api_uploadImage, {
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'bearer '+this.props.token
          },            
          body:formdata
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
     
                if(result != null){
                    let saleForm = this.state.sectionList;
                    for(let i=0; i<saleForm.length; i++){
                        if(saleForm[i].mappingId == this.state.formImageMapId){
                            saleForm[i].value = result.fileName
                        }
                    }
                    this.setState({
                        sectionList:saleForm
                    })
                    
                    Alert.alert(
                        'Successfully Uploaded',
                        '',
                        [
                            {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                        ],
                        {cancelable:false}
                    )

                    
                }    
                else{
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    alert('Upload photo failed. Please try again.')
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            alert(JSON.stringify(error))
//            console.error(error);
          });          
    }
    
    validation(){
        let msg = '';
        this.state.principleAgent == ''? msg='Please select atleast one principle agent.':'';
        let saleForm = this.state.sectionList;
        for(let i=0; i<saleForm.length; i++){
            if(saleForm[i].isMandatory == true){
                saleForm[i].value == null? msg='Please do not leave '+saleForm[i].label+' field blank.':''
            }
        }
        if(msg != ''){     
            Alert.alert(
                'Info Not Completed',
                msg,
                [
                    {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'}
                ],
                {cancelable:false}
            )
        }
        else{
            this.postRequestUpdateUnitSale();
        }
//        this.state.signatureMapId == ''? this.postRequestUpdateUnitSale():this.postRequestUploadSaleFormSignature()
//        alert(JSON.stringify(this.state.sectionList))
        
    }
    
    proceed(){
        const {navigate} = this.props.navigation;
//        Alert.alert(
//            'Successfully Booked',
//            'Thanks you for the booking.',
//            [
//                {text:'Proceed', onPress:()=> navigate('summaryInfo'), style:'cancel'}
//            ],
//            {cancelable:false}
//        )
        navigate('summaryInfo', {salesId:this.state.salesId})
       
    }
    

  uploadSignatureImage(mapID){
      
    ImagePicker.showImagePicker({
          title: 'Select Upload Option',
          takePhotoButtonTitle:'Take Photo',
          chooseFromLibraryButtonTitle:'Upload From Gallery',
          maxWidth:1000,
          maxHeight:1000,
          quality:1,        
//          storageOptions: {
//            skipBackup: true,
//            path: 'images',
//          },
      }, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
        this.setState({
          signatureImageUri: source,
          signatureImageData: response.data,
          signatureMapId:mapID
        });
        
        this.postRequestUploadSaleFormSignature();
        
      }
    });      
  }
    
  uploadFormImage(mapID){
      
    ImagePicker.showImagePicker({
          title: 'Select Upload Option',
          takePhotoButtonTitle:'Take Photo',
          chooseFromLibraryButtonTitle:'Upload From Gallery',
          maxWidth:1000,
          maxHeight:1000,
          quality:1,        
//          storageOptions: {
//            skipBackup: true,
//            path: 'images',
//          },
      }, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
        this.setState({
          formImageUri: source,
          formImageData: response.data,
          formImageMapId:mapID
        });
          
        this.postRequestUploadSaleFormImage();
      }
    });      
  }
    
  render() {
      const {goBack} = this.props.navigation;
      
        if(this.state.isLoading){
            return ( 
                <Container style={{backgroundColor:colors.bgColor}}>
                  <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                    <ActivityIndicator size="large" color="#ffffff" />
                    <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
                  </View>
                </Container>
            )
        }          
      
      purchasers = (
          <Form style={{backgroundColor:'#fff', paddingRight:10, paddingBottom:10}}>
            <Item style={{flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formformTxtColor}}>Booking Date*</Label>
                <Text style={{alignSelf:'flex-end', color:colors.formTxtColor, marginRight:10}}>{this.state.bookingDate}</Text>
            </Item>           
            <Item style={{flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>SPA Sign Due Date</Label>
                <Text style={{alignSelf:'flex-end', color:colors.formTxtColor, marginRight:10}}>{this.state.spaSignDueDate}</Text>
            </Item>           
            <Item style={{flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Principal agent*</Label>
                <Item style={{borderColor:'transparent'}}>
                    <Picker
                      mode="dropdown"
                      iosHeader="Select Race"
                      enabled={this.props.permission.filter((x)=>x=="Sales.ManageSellers").length>0?true:false}
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined }}
                      selectedValue={this.state.principleAgent}
                      onValueChange={(value)=>this.setState({principleAgent:value})}
                    >
                        <Picker.Item value='' label='Select Agent' />
                        {this.state.agentList.map((item, index)=>{
                            return <Picker.Item key={index} value={item.value} label={item.name+'\n'+item.teamName} />
                        })}
                    </Picker>
                </Item> 
            </Item>
            {this.props.permission.filter((x)=> x == 'Sales.ManageSellers').length>0?
            <Item style={{flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Co-agents</Label>
                <FlatList extraData={this.state.coAgent} bounces={false} data={this.state.coAgent}
                    renderItem={({item, index}) =>(
                        <Item style={{alignSelf:'flex-end', borderColor:'transparent', width:Dimensions.get('window').width, marginRight:10}}>
                            <Text style={{color:'#1E90FF', marginRight:10}}>{item}</Text>
                            <TouchableOpacity onPress={()=>this.setState({coAgent:this.state.coAgent.filter((x)=> x != item)})}>
                                <Icon name='cancel' type='MaterialIcons' style={{fontSize:20}}/>
                            </TouchableOpacity>                            
                        </Item>
                    )}
                    keyExtractor={(item, index) => 'agents' +index}
                />
                <Item style={{borderColor:'transparent'}}>
                    <Picker
                      mode="dropdown"
                      iosHeader="Select Agent"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined }}
                      selectedValue={this.state.selectAgent}
                      onValueChange={(value)=>this.setState((prev)=>({coAgent:prev.coAgent.filter((x)=>x == value).length >0?[...prev.coAgent]:[...prev.coAgent, value]}))}
                    >
                        <Picker.Item value='' label='Add Agent' />
                        {this.state.agentList.map((item, index)=>{
                            return <Picker.Item key={index} value={item.name} label={item.name+'\n'+item.teamName} />
                        })}
                    </Picker>
                </Item>                 
            </Item>:<View/>}
            {this.props.permission.filter((x)=> x == 'Sales.ManageSolicitor').length>0?
            <Item style={{flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Solicitor</Label>
                <Item style={{borderColor:'transparent'}}>
                    <Picker
                      mode="dropdown"
                      iosHeader="Select Solicitor"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined }}
                      selectedValue={this.state.solicitor}
                      onValueChange={(value)=>this.setState({solicitor:value})}
                    >
                        <Picker.Item value={0} label='Select Solicitor' />
                        {this.state.solicitorList.map((item, index)=>{
                            return <Picker.Item key={index} value={item.value} label={item.name} />
                        })}
                    </Picker>
                </Item>
            </Item>:<View/>}
            <Item style={{flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Event</Label>
                <Item style={{borderColor:'transparent'}}>
                    <Picker
                      mode="dropdown"
                      iosHeader="Select Event"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined }}
                      selectedValue={this.state.event}
                      onValueChange={(value)=>this.setState({event:value})}
                    >
                        <Picker.Item value={0} label='Select Event' />
                        {this.state.eventList.map((item, index)=>{
                            return <Picker.Item key={index} value={item.value} label={item.name} />
                        })}
                    </Picker>
                </Item>                
            </Item>
            <Item style={{flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Referrer Name</Label>
                <Input onChangeText={(value) => {this.setState({referrerName:value})}} value={this.state.referrerName} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
            </Item>
            <Item style={{flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Referrer IC/Passport No.</Label>
                <Input onChangeText={(value) => {this.setState({referrerIC:value})}} value={this.state.referrerIC} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
            </Item> 
            <FlatList extraData={this.state} bounces={false} data={this.state.sectionList}
                renderItem={({item, index}) => {
                    if(item.type == 0){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Input onChangeText={(value) => {let list = [...this.state.sectionList];list[index].value = value; this.setState({sectionList:list})}} value={this.state.sectionList[index].value} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>)
                    }
                    else if(item.type == 1){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Form style={{width:'100%'}}>
                                <Textarea onChangeText={(value) => {let list = [...this.state.sectionList];list[index].value = value; this.setState({sectionList:list})}} value={this.state.sectionList[index].value} rowSpan={5} style={{borderColor:colors.formTxtColor}} bordered />
                            </Form>
                        </Item>)
                    }
                    else if(item.type == 2){
                        return (
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <FlatList extraData={this.state.sectionList} bounces={false} data={item.choices}
                                renderItem={({item:item2}) => (
                                     <Item style={{width:Dimensions.get('window').width*0.85, borderColor:'transparent', marginTop:5}}>
                                        <Left>
                                          <Text>{item2}</Text>
                                        </Left>
                                        <Right>
                                           <CheckBox checked={this.state.sectionList[index].value == item2?true:false} onPress={()=>{let list = [...this.state.sectionList];list[index].value = item2; this.setState({sectionList:list})}}/>
                                        </Right>
                                      </Item>      
                                )}
                                keyExtractor={(item, index) => 'radio' +index}
                            />  
                        </Item>)
                    }
                    else if(item.type == 3){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Item style={{borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader={"Select "+item.label}
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.sectionList[index].value == null?'':this.state.sectionList[index].value}
                                  onValueChange={
                                        (value)=>{
                                            let list = [...this.state.sectionList];
                                            list[index].value = value; 
                                            this.setState({sectionList:list})
                                        }
                                    }
                                >
                                    <Picker.Item value='' label={'Select '+item.label} />
                                    {item.choices.map((item, index)=>{
                                        return <Picker.Item key={index} value={item} label={item} />
                                    })}
                                </Picker>
                            </Item>
                        </Item>)
                    }
                    else if(item.type == 5){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            {this.state.signatureImageData !=''?
                                <Image source={this.state.signatureImageUri} style={{width:150, height:150}} />:<View/>
                            }
                            <Button onPress={()=>this.uploadSignatureImage(item.mappingId)}><Text>Upload Signature Photo</Text></Button>
                            
                        </Item>)
                    }
                    else if(item.type == 6){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <FlatList extraData={this.state.sectionList} bounces={false} data={item.choices}
                                renderItem={({item:item4}) => (
                                     <Item style={{width:Dimensions.get('window').width*0.85, borderColor:'transparent', marginTop:5}}>
                                        <Left>
                                          <Text>{item4}</Text>
                                        </Left>
                                        <Right>
                                           <CheckBox checked={this.state.sectionList[index].value == null?false:this.state.sectionList[index].value.indexOf(item4) >= 0?true:false} 
                                               onPress={()=>{
                                                    let list = [...this.state.sectionList];
                                                    if(list[index].value != null){
                                                        if(list[index].value.indexOf(item4) < 0){

                                                            list[index].value = item4+','+list[index].value
                                                        }else{
                                                            list[index].value.lastIndexOf(item4+',') < 0?
                                                            list[index].value = list[index].value.replace(item4, ''):
                                                            list[index].value = list[index].value.replace(item4+',', '')

                                                        }
                                                    }
                                                    else{
                                                        list[index].value = item4;
                                                    }
                                                    this.setState({sectionList:list})
                                                }
                                               }/>
                                        </Right>
                                      </Item>      
                                )}
                                keyExtractor={(item, index) => 'radio' +index}
                            />
                        </Item>)
                    }
                    else if(item.type == 7){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            {this.state.formImageData !=''?
                                <Image source={this.state.formImageUri} style={{width:150, height:150}} />:<View/>}
                                <Button onPress={()=>this.uploadFormImage(item.mappingId)}><Text>Upload Signature Photo</Text></Button>
                            
                        </Item>)
                    }
                    else if(item.type == 100){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Item style={{alignSelf:'flex-end', borderColor:'transparent'}}>
                            <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(1918, 1, 1)}
                                    maximumDate={new Date(2108, 12, 31)}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText="Select Date"
                                    textStyle={{ color: "green" }}
                                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                                    onDateChange={(newDate)=>{
                                            let list = this.state.sectionList; 
                                            list[index].value = newDate; 
                                            this.setState({sectionList:list})
                                        }
                                    }
                                    disabled={false}
                            />
                            </Item>
                        </Item>)
                    }
                    else{
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Input onChangeText={(value) => {let list = this.state.sectionList;list[index].value = value; this.setState({sectionList:list})}} value={this.state.sectionList[index].value} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>                               
                        </Item>)
                    }}
                }
                keyExtractor={(item, index) => 'section' +index}
            />     
            
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>Sale remarks</Label>
                <Text style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{this.state.salesRemark}</Text>
                <Form style={{width:'100%'}}>
                    <Textarea onChangeText={(value) => {this.setState({remark:value})}} value={this.state.remark} rowSpan={5} style={{borderColor:colors.formTxtColor}} bordered />
                </Form>                
            </Item>
          </Form>
      )
        
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            <TouchableOpacity onPress={() => goBack()}>
                <Icon name="back" type="Entypo" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={{color:colors.formTxtColor}}>Sales Info</Title>
          </Body>
          <Right>                     
          </Right>
        </Header>
        <Content padder >
            {purchasers}
        </Content>
        <Button onPress={this.validation.bind(this)} style={[styles.defaultBtn, {margin:10}]}>
            <Text style={{color:colors.btnTxtColor}}>Submit</Text>
        </Button>
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token, 
        permission:state.permission  
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setRefresh: (tk) => dispatch({type: 'SET_REFRESH_UNIT', param:tk}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(salesInfoPage);