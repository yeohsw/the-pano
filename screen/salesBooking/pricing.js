import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Title, Icon, Button, Item, Picker, Segment, Input, ActionSheet } from "native-base";
import { TouchableOpacity, FlatList, Dimensions, View, ActivityIndicator, Alert} from 'react-native'
import {styles, colors} from '../globalStyle.js'
import {api_reserveUnit, 
        api_getUnitSalesDetailForEdit, 
        api_getUnitSale, 
        api_findRebate, 
        api_deleteRebate, 
        api_createUnitSaleRebate, 
        api_createOrUpdateDiscount, 
        api_getUnitDiscountForEdit, 
        api_deleteDiscount,
        api_getGrrRebate,
        api_createGrrRebate} from '../config.js'
import {connect} from 'react-redux'

class pricingPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            selectedUnit:[],
            isLoading:false,
            unitSalesID:[],
            unitSale:[],
            discountList:[],
            showDiscount:false,
            unitSaleValue:0,
            percentage:false,
            discountValue:'0',
            discountName:'',
            isBumi:false,
            discountId:0,
            discountUnitSale:0,
            type:0
            
        }
    }
    
    componentDidMount(){
//        alert(JSON.stringify(this.props.navigation.getParam('selected',[])))
       
        this.postRequestReserve()
    }
    
    postRequestReserve(){
        const {navigation} = this.props;
        this.setState({isLoading:true});
        return fetch(api_reserveUnit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              unitIds: navigation.getParam('selected',[]).map((x)=>x.id)
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        unitSalesID:result.unitSaleIds
                    })
                    this.postRequestGetUnitSaleDetail()
                }
                else{

                    this.setState({isLoading:false})
                    let errorMsg = responseJson.error;
                    if(errorMsg.message == 'Invalid unit'){
                        //resume reserve 
                        this.setState({
                            unitSalesID:navigation.getParam('selected',[]).map((x)=>x.id)
                        })
                        this.postRequestGetUnitSaleDetail()                        
                    }
                    else{
                        alert(JSON.stringify(errorMsg.message));
                    }
                    
                }
        
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    }
    
    postRequestGetUnitSaleDetail(){
        this.setState({isLoading:true});
        
        return fetch(api_getUnitSalesDetailForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            unitSaleIds: this.state.unitSalesID
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                this.setState({isLoading:false});
                
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        unitSale:result.unitSales,
                    })
                }
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }
                
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    }
    
    postRequestGetDiscountForEdit(id, listPrice){
        
        return fetch(api_getUnitDiscountForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            unitSaleId: id,
            unitSaleDiscountId: null,
            salesDate: new Date()
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
                let setting = result.unitSaleDiscount
                if(result != null){
                   this.setState({
                       unitSaleValue:listPrice,
                       discountList:result.discounts,
                       showDiscount:true,
                       discountValue:''+setting.value,
                       discountName:setting.name,
                       isBumi:setting.isBumi,
                       discountId:setting.id,
                       discountUnitSale:setting.unitSaleId,
                       type:setting.type
                   })            
                   
                }
               
               
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    }
    
    postRequestSaveDiscount(value){
        
        return fetch(api_createOrUpdateDiscount, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            unitSaleId: this.state.discountUnitSale,
            name: this.state.discountName,
            value: value,
            isBumi: this.state.isBumi,
            type: this.state.type,
            id: this.state.discountId
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
               
                this.setState({
                    showDiscount:false,
                })            
                
                this.postRequestGetUnitSaleDetail();
                
               
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    }
    
    postRequestDeleteDiscount(id){
        
        return fetch(api_deleteDiscount, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            id: id
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
               
                this.postRequestGetUnitSaleDetail();
                      
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    }
    
    postRequestFindRebate(id){
        
        return fetch(api_findRebate, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            unitSaleId: id,
            filter: "",
            sorting: "",
            skipCount: 0,
            maxResultCount: 200000
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
               
                if(result != null){
                    let btn = result.items.map((x)=> x.name);
                    let btnList = [...btn, 'Cancel']
                    ActionSheet.show(
                      {
                        options: btnList,
                        cancelButtonIndex: btnList.length,
                        title: "Select Rebate"
                      },
                      buttonIndex => {
                          if(buttonIndex != (btnList.length-1) && buttonIndex != btnList.length){
                            this.postRequestCreateRebate(id, result.items[buttonIndex].value);
//                              alert(btnList.length)
                          }
                        
                      }
                    )
                }
                      
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    }
    
    postRequestCreateRebate(id, value){
        
        return fetch(api_createUnitSaleRebate, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            unitSaleId: id,
            rebateId: value
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
               
                this.postRequestGetUnitSaleDetail();
                      
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    } 
    
    postRequestFindGrrRebate(id){
        
        return fetch(api_getGrrRebate, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            unitSaleId: id,
            filter: "",
            sorting: "",
            skipCount: 0,
            maxResultCount: 200000
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
               
                if(result != null){
                    let btn = result.items.map((x)=> x.name);
                    let btnList = [...btn, 'Cancel']
                    ActionSheet.show(
                      {
                        options: btnList,
                        cancelButtonIndex: btnList.length,
                        title: "Select Rebate"
                      },
                      buttonIndex => {
                          if(buttonIndex != (btnList.length-1) && buttonIndex != btnList.length){
                            this.postRequestCreateRebate(id, result.items[buttonIndex].value);
//                              alert(btnList.length)
                          }
                        
                      }
                    )
                }
                      
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    }
    
    postRequestCreateGrrRebate(id, value){
        
        return fetch(api_createGrrRebate, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            unitSaleId: id,
            rebateId: value
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
               
                this.postRequestGetUnitSaleDetail();
                      
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    } 
    
    postRequestRemoveRebate(id){
        
        return fetch(api_deleteRebate, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            id: id
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                
                let result = responseJson.result;
               
                this.postRequestGetUnitSaleDetail();
                      
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    
    }    
    
    proceed(){
        const {navigate} = this.props.navigation;
        navigate('purchaser', {hideTabBar:true, unitSaleDetail:this.state.unitSalesID});
    }
    
    pickerSelected(value){
        let discountDetail = this.state.discountList.filter((x)=>x.name == value);
        if(discountDetail.length>0){
            this.setState({
                discountName:value,
                discountValue:''+discountDetail[0].value,
                percentage:discountDetail[0].isPercentage,
                isBumi:discountDetail[0].isBumi
            })  
        }
        else{
            this.setState({
                discountName:value,
                discountValue:'',
                percentage:false,
                isBumi:false
            })
        }
 
    }
    
    saveDiscount(){
       let afterDiscountValue = 0
       if(this.state.discountValue != 0){
           if(this.state.percentage){
               afterDiscountValue = this.state.unitSaleValue * parseInt(this.state.discountValue)/100;
           }
           else{
               afterDiscountValue = parseInt(this.state.discountValue);
           }
           
       if(afterDiscountValue >= this.state.unitSaleValue){
           Alert.alert(
            'Invalid Discount Value',
            'Discount value cannot be more than list price.',
            [
                {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
            ],
            {cancelable:false}
           )
       }
       else{
           this.postRequestSaveDiscount(afterDiscountValue) 
       }           
       }
       else{
           Alert.alert(
            'Invalid Discount Value',
            'Discount value cannot be zero',
            [
                {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
            ],
            {cancelable:false}
           )           
       }       
    }
    
    removeDiscount(id){
       this.postRequestDeleteDiscount(id)
    }
    
    removeRebate(id, items){
//        alert(JSON.stringify(items))
        if(items.filter((x)=>x.issueOnType == 1 && x.id == id).length>0 || items.filter((x)=>x.issueOnType == 1).length ==0){
            this.postRequestRemoveRebate(id);    
        }
        else{
            alert('Please remove grr rebate before remove rebate.')
        }
        
    }
    
    warning(){
        const {goBack} = this.props.navigation
        Alert.alert(
            'Are You Sure?',
            'Upon aborting in half way you would need to click on sales listing option to resume your process later.',
            [
                {text:'Cancel', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Proceed', onPress:()=>goBack()}
            ],
            {cancelable:false}
        )
    }
        
  render() {
      
      
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    }  
      
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            <TouchableOpacity onPress={() =>this.warning()}>
                <Icon name="back" type="Entypo" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={{color:colors.formTxtColor}}>Unit's Detail</Title>
          </Body>
          <Right>                 
          </Right>
        </Header>
        <ActionSheet
            ref={c => {
                ActionSheet.actionsheetInstance = c;
            }}
        />            
        {this.state.showDiscount?
                <View style={{flex:1, position:'absolute', flexDirection:'column', justifyContent:'center' , paddingLeft:'10%', paddingRight:'10%', width:Dimensions.get('window').width, height:Dimensions.get('window').height, zIndex:2, backgroundColor:'transparent', opacity:1}}>
                        <Card>
                            <CardItem style={{backgroundColor:'#F7F9F9'}} header bordered>
                                <Text style={{color:colors.btnTxtColor}}>Add Discount</Text>
                            </CardItem>
                            {this.state.discountList.length>0?
                            <CardItem bordered style={{flexDirection:'column', justifyContent:'flex-start', backgroundColor:'#F7F9F9'}}>    
                                <Text style={{alignSelf:'flex-start'}}>
                                  Discount Template
                                </Text>
                                <Item style={{alignSelf:'flex-end', borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosIcon={<Icon name="arrow-down"/>}
                                  style={{ width: undefined }}
                                  headerTitleStyle={{fontSize:15}}
                                  selectedValue={this.state.discountName}
                                  onValueChange={this.pickerSelected.bind(this)}
                                >
                                    <Picker.Item value='' label='Select Discount' />
                                    {this.state.discountList.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.name} label={item.name} />
                                    })}
                                </Picker>
                                </Item>
                            </CardItem>:<View/>}
                            <CardItem bordered style={{backgroundColor:'#F7F9F9'}}>           
                                <Text style={{width:'30%', flexDirection:'row'}}>
                                  Type
                                </Text>
                                <Text>: </Text>
                                 <Segment style={{backgroundColor:'transparent'}}>
                                  <Button first active={this.state.percentage} style={{borderColor:colors.btnColor, backgroundColor:this.state.percentage?colors.btnColor:'transparent'}} onPress={()=>this.setState({percentage:true, discountValue:''})}>
                                    <Text  style={{color:this.state.percentage?'#000':colors.btnColor}}>%</Text>
                                  </Button>
                                  <Button last active={!this.state.percentage} style={{borderColor:colors.btnColor, backgroundColor:!this.state.percentage?colors.btnColor:'transparent'}} onPress={()=>this.setState({percentage:false})}>
                                    <Text style={{color:!this.state.percentage?'#000':colors.btnColor}}>RM</Text>
                                  </Button>
                                </Segment> 
                            </CardItem>
                            <CardItem bordered style={{backgroundColor:'#F7F9F9'}}>           
                                <Text style={{width:'30%', flexDirection:'row'}}>
                                  Value
                                </Text>
                                {!this.state.percentage?
                                    <Item style={{width:'70%', borderColor:'transparent'}}>
                                        <Text style={{width:'30%'}}>: RM </Text>
                                        <Item style={{width:'65%'}} regular>
                                            <Input onChangeText={(value) => {this.setState({discountValue:value.replace(/[^0-9.]/g, '')})}} value={this.state.discountValue} />
                                        </Item>
                                    </Item>:
                                    <Item style={{width:'50%', borderColor:'transparent'}}>
                                        <Text>: </Text>
                                        <Item style={{width:'70%'}} regular>
                                            <Input onChangeText={(value) => {this.setState({discountValue:value.replace(/[^0-9]/g, '')})}} value={this.state.discountValue} />
                                        </Item>
                                        <Text style={{width:'25%'}}> %</Text>
                                    </Item>
                                }
                            </CardItem>
                            <CardItem bordered style={{backgroundColor:'#F7F9F9'}}>           
                                <Text style={{width:'30%', flexDirection:'row'}}>
                                  Name
                                </Text>
                                <Item style={{width:'70%', borderColor:'transparent'}}>
                                    <Text>: </Text>
                                    <Item style={{width:'90%'}} regular>
                                        <Input style={{fontSize:15}} onChangeText={(value) => {this.setState({discountName:value})}} value={this.state.discountName}/> 
                                    </Item>
                                </Item>
                            </CardItem>
                            <CardItem bordered style={{justifyContent:'flex-end', backgroundColor:'#F7F9F9'}}>           
                                <Button transparent style={{width:'30%', marginRight:10}} onPress={()=>this.setState({showDiscount:false, percentage:false, discountValue:'0', discountName:'', isBumi:false,})}>
                                    <Text style={{color:colors.btnTxtColor, width:100}}>Cancel</Text>
                                </Button>
                                <Button style={{width:'30%', backgroundColor:colors.btnColor}} onPress={this.saveDiscount.bind(this)}>
                                    <Text style={{color:colors.btnTxtColor}}>Save</Text>
                                </Button>
                            </CardItem>
                        </Card>
                </View>:<View/>
            }
        <Content padder>
            <FlatList listKey="unitSales" extraData={this.state.unitSale} bounces={false} data={this.state.unitSale}
                renderItem={({item}) =>(
                      <Card>
                        <CardItem header bordered>
                          <Text>{item.unitNumber}</Text>
                        </CardItem>
                        <CardItem bordered>           
                            <Text style={{width:'50%', flexDirection:'row'}}>
                              List Price
                            </Text>
                            <Text>: RM {(item.listPrice.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>  
                        </CardItem>
                        <CardItem bordered>          
                            <Text style={{width:'50%', flexDirection:'row'}}>
                              Discount
                            </Text>
                            <TouchableOpacity onPress={this.postRequestGetDiscountForEdit.bind(this, item.id, item.listPrice)}>
                                <Text style={{color:'#1E90FF'}}>: + Add discount</Text>  
                            </TouchableOpacity>
                        </CardItem> 
                        <FlatList extraData={this.state.unitSale} bounces={false} data={item.discounts}
                            renderItem={({item:item2}) =>(
                                 
                                <CardItem bordered>          
                                    <Text style={{marginLeft:'50%', color:'#0000CD', fontSize:13, width:'40%'}}>: -RM {(item2.value.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}({(item2.value*100)%item.listPrice == 0?item2.value/item.listPrice*100:(item2.value/item.listPrice*100).toFixed(2)}%) {item2.name}</Text> 
                                    <TouchableOpacity onPress={this.removeDiscount.bind(this, item2.id)} >
                                        <Icon name='cancel' type='MaterialIcons' style={{fontSize:25}}/>
                                    </TouchableOpacity>
                                    
                                </CardItem>
                                
                                 )}
                             listKey={(item, index) => 'discount' + index.toString()}
                             keyExtractor={(item, index) => 'discount' +index}
                        />
                        <CardItem bordered>         
                            <Text style={{width:'50%', flexDirection:'row'}}>
                              SPA Price
                            </Text>
                            <Text>: RM {(item.spaPrice.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>          
                        </CardItem>
                        <CardItem bordered>          
                            <Text style={{width:'50%', flexDirection:'row'}}>
                              Rebate
                            </Text>
                            <TouchableOpacity onPress={this.postRequestFindRebate.bind(this, item.id)}>
                                <Text style={{color:'#1E90FF'}}>: + Add Rebate</Text>  
                            </TouchableOpacity>
                        </CardItem>
                        <FlatList extraData={this.state.unitSale} bounces={false} data={item.rebates}
                            renderItem={({item:item3}) =>(
                                item3.issueOnType == 0?
                                <CardItem bordered>          
                                    <Text style={{marginLeft:'50%', color:'#0000CD', fontSize:13, width:'40%'}}>: -RM {(item3.amount.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} ({(item3.amount*100)%item.listPrice == 0? (item3.amount/item.listPrice*100).toFixed(2): (item3.amount/item.listPrice*100).toFixed(2)}%) {item3.name}</Text> 
                                    <TouchableOpacity onPress={this.removeRebate.bind(this, item3.id, item.rebates)} >
                                        <Icon name='cancel' type='MaterialIcons' style={{fontSize:25}}/>
                                    </TouchableOpacity>
                                </CardItem>:<View/>
                                
                                 )}
                             listKey={(item, index) => 'rebate' + index.toString()}
                             keyExtractor={(item, index) => 'rebate' +index}
                        />
                        {item.rebates.length > 0?
                        <CardItem bordered>         
                            <Text style={{width:'50%', flexDirection:'row'}}>
                              Nett Purchase Price{item.rebates.filter((x)=>x.issueOnType == 0).length >0?"(After Rebate)":''}
                            </Text>
                            <Text style={{alignSelf:'flex-start'}}>: RM {(item.netPurchasePrice.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>          
                        </CardItem>:<View/>} 
                        {item.rebates.filter((x)=>x.issueOnType == 0).length >0?
                        <CardItem bordered>          
                            <Text style={{width:'50%', flexDirection:'row'}}>
                              Grr Rebate
                            </Text>
                            <TouchableOpacity onPress={this.postRequestFindGrrRebate.bind(this, item.id)}>
                                <Text style={{color:'#1E90FF'}}>: + Add Grr Rebate</Text>  
                            </TouchableOpacity>
                        </CardItem>:<View/>}
                        <FlatList extraData={this.state.unitSale} bounces={false} data={item.rebates}
                            renderItem={({item:item4}) =>(
                                item4.issueOnType == 1?
                                <CardItem bordered>          
                                    <Text style={{marginLeft:'50%', color:'#0000CD', fontSize:13, width:'40%'}}>: -RM {(item4.amount.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} ({(item4.amount*100)%item.listPrice == 0? (item4.amount/item.listPrice*100).toFixed(2): (item4.amount/item.listPrice*100).toFixed(2)}%) {item4.name}</Text> 
                                    <TouchableOpacity onPress={this.removeRebate.bind(this, item4.id, item.rebates)} >
                                        <Icon name='cancel' type='MaterialIcons' style={{fontSize:25}}/>
                                    </TouchableOpacity>
                                </CardItem>:<View/>
                                
                                 )}
                             listKey={(item, index) => 'rebate' + index.toString()}
                             keyExtractor={(item, index) => 'rebate' +index}
                        />
                        {item.rebates.filter((x)=>x.issueOnType == 0).length >0?
                        <CardItem bordered>         
                            <Text style={{width:'50%', flexDirection:'row'}}>
                              Nett Purchase Price(After Grr Rebate)
                            </Text>
                            <Text style={{alignSelf:'flex-start'}}>: RM {(item.guaranteedRentalRefundPrice.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>          
                        </CardItem>:<View/>}                        
                      </Card>
                    )}
                 keyExtractor={(item, index) => 'unit' +index}
            />
          
        </Content>
        {this.state.showDiscount?<View/>:
        <Button onPress={this.proceed.bind(this)} style={[styles.defaultBtn, {margin:10}]}>
            <Text style={{color:colors.btnTxtColor}}>Next</Text>
        </Button>}
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token,        
    }
}

export default connect(mapStateToProps)(pricingPage);