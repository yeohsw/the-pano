import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Title, Icon, Button, Item, DatePicker} from "native-base";
import { TouchableOpacity, FlatList, Image, Dimensions, Modal, View, ActivityIndicator} from 'react-native'
import {Pie} from 'react-native-pathjs-charts'
import {connect} from 'react-redux'
import {styles, colors} from '../globalStyle.js';
import Moment from 'moment'
import {api_getUnitTypeDetail, api_getPhaseStatusDetail, api_agentSalesRanking, api_getTeamSalesRanking, phaseId, blockId} from '../config.js'

class dashboardPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            phaseName:'',
            configStatus:[],
            unitTypeColor:['#CD6155', '#EC7063', '#F8C471', '#C39BD3', '#2471A3', '#5DADE2', '#48C9B0', '#45B39D', '#52BE80', '#58D68D', '#F4D03F', '#F5B041', '#EB984E', '#DC7633', '#F0F3F4', '#CACFD2', '#AAB7B8', '#99A3A4', '#5D6D7E', '#566573'],
            total:0,
            totalTeamUnitCount:0,
            totalUnitTypeCount:0,
            piePhaseStatusColor:[],
            pieTeamStatusColor:[],
            pieUnitTypeColor:[],
            phaseStatusDetail:[],
            teamStatusDetail:[],
            unitTypeDetail:[],
            tableStatusSum:[],
            tableTeamSum:[],
            tableUnitTypeSum:[],
            filterDate:false,
            startDate:null,
            endDate:null
        }
        
    }
    
    componentDidMount(){
//        alert(this.hexToRgbA('#f00'))
        this.postRequestGetPhaseStatus()
    }

    postRequestGetPhaseStatus(){
        const {navigation} = this.props;
        this.setState({isLoading:true});
        return fetch(api_getPhaseStatusDetail, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              phaseIds: [
                phaseId
              ],
              fromSalesDate: this.state.startDate == null? new Date(1990,1,1):this.state.startDate,
              toSalesDate: this.state.endDate == null? new Date():this.state.endDate
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
//                this.setState({isLoading:false})
                let result = responseJson.result;
                if(result != null){
                    let sum = result.salesSummary;
                    let tol = 0;
                    let data = []
                    for(let i=0; i<sum.length; i++){
                        tol+=sum[i].unitCount;   
                    }
                    
                    for(let i=0; i<sum.length; i++){
                        data.push({name:'', population:sum[i].unitCount})
                    }
                    
                    this.props.setStatus(result.statuses)
                    this.setState({
                        configStatus:result.statuses,
                        piePhaseStatusColor:[this.hexToRgb(result.statuses[0].bgColor), this.hexToRgb(result.statuses[1].bgColor), this.hexToRgb(result.statuses[2].bgColor), this.hexToRgb(result.statuses[3].bgColor), this.hexToRgb(result.statuses[4].bgColor), this.hexToRgb(result.statuses[5].bgColor)],
                        tableStatusSum:result.salesSummary,
                        total:tol,
                        phaseName:result.phaseSales[0].phaseName,
                        phaseStatusDetail: data
                    })
                    this.postRequestGetTeamSales()
                }
                else{
                    this.setState({isLoading:false})
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          }); 
    }
    
    postRequestGetTeamSales(){
        const {navigation} = this.props;
        this.setState({isLoading:true});
        return fetch(api_getTeamSalesRanking, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              startSalesDate: this.state.startDate,
              endSalesDate: this.state.endDate,
              includeSales: true,
              teamType: '',
              phaseId: [phaseId]
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
                if(result != null){
                    let sum = result.activeSalesSummary
                    let tol = 0;
                    let data = [];
                    let st = [];
                    for(let i=0; i<sum.length; i++){
                        tol+=sum[i].unitCount;   
                    }
                    
                    for(let i=0; i<sum.length; i++){
                        data.push({name:'', population:sum[i].unitCount})
                        st.push(this.hexToRgb(this.state.configStatus[sum[i].status].bgColor))
                    }                    
                    
                    this.setState({
                        teamStatusDetail:data,
                        tableTeamSum:sum,
                        totalTeamUnitCount:tol,
                        pieTeamStatusColor:st
                    })
                    
                    this.postRequestGetUnitType()
                }
                else{
                    this.setState({isLoading:false})
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          }); 
    }    
    
    postRequestGetUnitType(){
        const {navigation} = this.props;
        this.setState({isLoading:true});
        return fetch(api_getUnitTypeDetail, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          }, 
          body: JSON.stringify({
              blockIds: [blockId],
              endSalesDate: this.state.endDate,
              startSalesDate: this.state.startDate,
              status: null,
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
                if(result != null){
                    let units = result.unitTypes
                    let tol = 0;
                    let data = [];
                    let st = [];
//                    alert(JSON.stringify(units))
                    
                    for(let i=0; i<units.length; i++){
                        tol += units[i].count
                    }
                    
                    for(let i=0; i<units.length; i++){
                        data.push({name:'', population:units[i].count})
                        st.push(this.hexToRgb(this.state.unitTypeColor[i]))
                    }
                    
//                    for(let i=0; i<12; i++){
//                        data.push({name:'text', population:5})
//                        st.push(this.hexToRgb(this.state.unitTypeColor[i]))
//                    }
                            
                    this.setState({
                        totalUnitTypeCount:tol,
                        unitTypeDetail:data,
                        tableUnitTypeSum:units,
                        pieUnitTypeColor:st
                    })
                }
                else{
                    this.setState({isLoading:false})
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          }); 
    } 
    
    hexToRgb(hex){
        let c;
        if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
            c= hex.substring(1).split('');
            if(c.length== 3){
                c= [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c= '0x'+c.join('');
            return {r:(c>>16)&255, g:(c>>8)&255, b:c&255};
        }
        throw new Error('Bad Hex');
    }
    
    moneyFormat(rm){
        return rm.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    
 render() {
     
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    } 
     

    let options = {
      margin: {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
      },
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').width,
      color: '#2980B9',
      r: 3,
      R: 150,
      legendPosition: 'topRight',
      animate: {
        type: 'oneByOne',
        duration: 200,
        fillTransition: 3
      },
      label: {
        fontFamily: 'Arial',
        fontSize: 15,
        fontWeight: true,
        color: '#2E2EFE'
      }
    }

    return (
      <Container style={{backgroundColor:colors.bgColor}}>  
         <Header style={styles.headerOverlap}>
            <Left>
              <Button transparent onPress={()=>this.props.navigation.navigate('prosales',{hideTabBar:true})}>
                <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
              </Button>
            </Left>
            <Body>
              <Title style={{color:colors.formTxtColor}}>Dashboard</Title>
            </Body>
            <Right>
                  
            </Right>
          </Header>
          <View style={{width:Dimensions.get('window').width, height:50, backgroundColor:colors.btnColor, justifyContent:'center'}}>
              <Text style={{alignSelf:'center', fontSize:20}}>{this.state.phaseName}</Text>
          </View>
          {this.state.filterDate?
          <View style={{flexDirection:'row'}}>
            <Item style={{width:'50%', flexDirection:'row', borderColor:colors.btnColor}}>
                <Text style={{color:'#fff'}}>From: </Text>
                <DatePicker
                    defaultDate={new Date(this.state.startDate)}
                    minimumDate={new Date(1990, 1, 1)}
                    maximumDate={new Date()}
                    locale={"ms-my"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={Moment(new Date(this.state.startDate)).format('D/M/YYYY')}
                    textStyle={{ color: colors.btnColor }}
                    placeHolderTextStyle={{ color: colors.btnColor }}
                    onDateChange={(newDate)=>this.setState({startDate:newDate})}
                    disabled={false}
                    style={{alignSelf:'flex-end'}}
                />  
            </Item>
            <Item style={{width:'50%', flexDirection:'row', borderColor:colors.btnColor}}>
                <Text style={{color:'#fff'}}>To: </Text>
                <DatePicker
                    defaultDate={new Date(this.state.endDate)}
                    minimumDate={new Date(1990, 1, 1)}
                    maximumDate={new Date()}
                    locale={"ms-my"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={Moment(new Date(this.state.endDate)).format('D/M/YYYY')}
                    textStyle={{ color: colors.btnColor }}
                    placeHolderTextStyle={{ color: colors.btnColor }}
                    onDateChange={(newDate)=>this.setState({endDate:newDate})}
                    disabled={false}
                    style={{alignSelf:'flex-end'}}
                />
            </Item>
          </View>:<View/>}
          <View style={{padding:5, flexDirection:'row', justifyContent:'center'}}>
            {this.state.filterDate?
            <Button style={[styles.defaultBtn, {width:'40%'}]} onPress={()=>{this.setState({filterDate:false, startDate:null, endDate:null, isLoading:true}); setTimeout(()=>{this.postRequestGetPhaseStatus()},500)}}>
             <Text style={{color:colors.btnTxtColor, fontSize:13, textAlign:'center'}}>Show From Beginning</Text>
            </Button>:<View/>}
            <Button style={[styles.defaultBtn, {width:'40%', marginLeft:5}]} onPress={()=>this.state.filterDate?this.postRequestGetPhaseStatus():this.setState({filterDate:true, startDate:new Date(), endDate:new Date()})}>
             <Text style={{color:colors.btnTxtColor}}>{this.state.filterDate?'Apply Filter':'Filter Date'}</Text>
            </Button>
          </View>  
          
          <Content bounces={false}>
              <View style={{flexDirection:'column', justifyContent:'flex-start'}}>

                <Item style={{width:Dimensions.get('window').width, marginTop:10, borderColor:"transparent", justifyContent:'center'}}>
                    <Text style={{color:'#fff', fontWeight:'bold'}}>Unit Status Breakdown</Text>
                </Item>
                {this.state.phaseStatusDetail == []?
                  <View><Text>No record found within this date range</Text></View>:
                  <Pie
                  data={this.state.phaseStatusDetail}
                  options={options}
                  pallete={this.state.piePhaseStatusColor}       
                  accessorKey="population" />}
              </View>
              <View style={{backgroundColor:'transparent'}}>
                <Item style={{flexDirection:'row', backgroundColor:colors.btnColor, borderColor:'transparent', width:'100%'}}>
                  <Item style={{height:30, width:'35%', borderBottomWidth:0, justifyContent:'center', borderRightWidth:1, borderColor:'#000', paddingTop:8}}>
                      <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Status</Text>
                  </Item>
                  <Item style={{height:30, width:'40%', borderBottomWidth:0, justifyContent:'center', borderRightWidth:1, borderColor:'#000', paddingTop:8}}>
                      <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Amount</Text>
                  </Item>
                  <Item style={{height:30, width:'25%', borderBottomWidth:0, justifyContent:'center', borderColor:'transparent', paddingTop:8}}>
                      <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Count</Text>
                  </Item>
                </Item>                  
                <FlatList extraData={this.state.tableStatusSum} bounces={false} data={this.state.tableStatusSum}
                    renderItem={({item, index}) =>(                  
                        <Item style={{flexDirection:'row', backgroundColor: index%2>0? '#fff':colors.lightGray, borderColor:'transparent', width:'100%'}}>
                          <Item style={{height:30, width:'35%', borderBottomWidth:0, paddingLeft:5, paddingTop:5, borderRightWidth:1, borderColor:'#000', backgroundColor:this.state.configStatus[item.status].bgColor }}>
                              <Text style={{height:'100%', fontSize:13, color:this.state.configStatus[item.status].color}}>{this.state.configStatus[item.status].text}</Text>
                          </Item>
                          <Item style={{height:30, width:'40%', borderBottomWidth:0, paddingTop:5, borderRightWidth:1, borderColor:'#000', justifyContent:'flex-end', paddingRight:5}}>
                              <Text style={{height:'100%', fontSize:13, color:'#000'}}>RM {this.moneyFormat(item.totalValue)}</Text>
                          </Item>
                          <Item style={{height:30, width:'25%', borderBottomWidth:0, paddingTop:5, borderColor:'transparent', justifyContent:'flex-end', paddingRight:10}}>
                              <Text style={{height:'100%', fontSize:13, color:'#000'}}>{item.unitCount}({(item.unitCount/this.state.total*100).toFixed(1)}%)</Text>
                          </Item>
                        </Item>
                    )}
                    keyExtractor={(item, index) => 'status' +index}
                />                          
              </View>
              <View style={{flexDirection:'column', justifyContent:'flex-start'}}>

                <Item style={{width:Dimensions.get('window').width, marginTop:10, borderColor:"transparent", justifyContent:'center'}}>
                    <Text style={{color:'#fff', fontWeight:'bold'}}>Unit Type Breakdown</Text>
                </Item>
                {this.state.unitTypeDetail.length == 0?
                  <View style={{justifyContent:'center', flexDirection:'row'}}><Text style={{color:'#fff', fontSize:13}}>"No record found within this date range"</Text></View>:
                  <Pie
                  data={this.state.unitTypeDetail}
                   options = {{
                      margin: {
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0
                      },
                      width: Dimensions.get('window').width,
                      height: Dimensions.get('window').width,
                      color: '#2980B9',
                      r: 3,
                      R: 150,
                      legendPosition: 'topRight',
                      animate: {
                        type: 'oneByOne',
                        duration: 200,
                        fillTransition: 3
                      },
                      label: {
                        fontFamily: 'Arial',
                        fontSize: 15,
                        fontWeight: true,
                        color: '#000'
                      }
                  }}
                  pallete={this.state.pieUnitTypeColor}       
                  accessorKey="population" />}
              </View>
              {this.state.unitTypeDetail.length == 0?<View/>:
              <View style={{backgroundColor:'transparent'}}>
                <Item style={{flexDirection:'row', backgroundColor:colors.btnColor, borderColor:'transparent', width:'100%'}}>
                  <Item style={{height:30, width:'50%', borderBottomWidth:0, justifyContent:'center', borderRightWidth:1, borderColor:'#000', paddingTop:8}}>
                      <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Status</Text>
                  </Item>
                  <Item style={{height:30, width:'50%', borderBottomWidth:0, justifyContent:'center', borderRightWidth:1, borderColor:'#000', paddingTop:8}}>
                      <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Count</Text>
                  </Item>
                </Item>                  
                <FlatList extraData={this.state.tableUnitTypeSum} bounces={false} data={this.state.tableUnitTypeSum}
                    renderItem={({item, index}) =>(                  
                        <Item style={{flexDirection:'row', backgroundColor: index%2>0? '#fff':colors.lightGray, borderColor:'transparent', width:'100%'}}>
                          <Item style={{height:30, width:'50%', borderBottomWidth:0, paddingLeft:5, paddingTop:5, borderRightWidth:1, borderColor:'#000', backgroundColor:this.state.unitTypeColor[index]}}>
                              <Text style={{height:'100%', fontSize:13, color:'#000'}}>{item.unitType}</Text>
                          </Item>
                          <Item style={{height:30, width:'50%', borderBottomWidth:0, paddingTop:5, borderColor:'transparent', justifyContent:'flex-end', paddingRight:10}}>
                              <Text style={{height:'100%', fontSize:13, color:'#000'}}>{item.count}({(item.count/this.state.totalUnitTypeCount*100).toFixed(1)}%)</Text>
                          </Item>
                        </Item>
                    )}
                    keyExtractor={(item, index) => 'status' +index}
                />                          
              </View>}    
              <View style={{flexDirection:'column', justifyContent:'flex-start'}}>
                <Item style={{width:Dimensions.get('window').width, marginTop:10, borderColor:"transparent", justifyContent:'center'}}>
                    <Text style={{color:'#fff', fontWeight:'bold'}}>Team Sales Breakdown</Text>
                </Item>
                {this.state.teamStatusDetail.length ==0 ?
                <View style={{justifyContent:'center', flexDirection:'row', padding:5}}><Text style={{color:'#fff', fontSize:13}}>"No record found within this date range"</Text></View>:
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('teamDashboard', {hideTabBar:true, startDt:this.state.startDate, endDt:this.state.endDate})}>
                    <Pie
                      data={this.state.teamStatusDetail}
                      options={options}
                      pallete={this.state.pieTeamStatusColor}       
                      accessorKey="population" />
                </TouchableOpacity>}
              </View>
              {this.state.teamStatusDetail.length == 0?<View/>:
              <View style={{backgroundColor:'transparent'}}>
                <Item style={{flexDirection:'row', backgroundColor:colors.btnColor, borderColor:'transparent', width:'100%'}}>
                  <Item style={{height:30, width:'35%', borderBottomWidth:0, justifyContent:'center', borderRightWidth:1, borderColor:'#000', paddingTop:8}}>
                      <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Status</Text>
                  </Item>
                  <Item style={{height:30, width:'40%', borderBottomWidth:0, justifyContent:'center', borderRightWidth:1, borderColor:'#000', paddingTop:8}}>
                      <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Amount</Text>
                  </Item>
                  <Item style={{height:30, width:'25%', borderBottomWidth:0, justifyContent:'center', borderColor:'transparent', paddingTop:8}}>
                      <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Count</Text>
                  </Item>
                </Item>                  
                <FlatList extraData={this.state.tableTeamSum} bounces={false} data={this.state.tableTeamSum}
                    renderItem={({item, index}) =>(                  
                        <Item style={{flexDirection:'row', backgroundColor: index%2>0? '#fff':colors.lightGray, borderColor:'transparent', width:'100%'}}>
                          <Item style={{height:30, width:'35%', borderBottomWidth:0, paddingLeft:5, paddingTop:5, borderRightWidth:1, borderColor:'#000', backgroundColor:this.state.configStatus[item.status].bgColor }}>
                              <Text style={{height:'100%', fontSize:13, color:this.state.configStatus[item.status].color}}>{this.state.configStatus[item.status].text}</Text>
                          </Item>
                          <Item style={{height:30, width:'40%', borderBottomWidth:0, paddingTop:5, borderRightWidth:1, borderColor:'#000', justifyContent:'flex-end', paddingRight:5}}>
                              <Text style={{height:'100%', fontSize:13, color:'#000'}}>RM {this.moneyFormat(item.totalValue)}</Text>
                          </Item>
                          <Item style={{height:30, width:'25%', borderBottomWidth:0, paddingTop:5, borderColor:'transparent', justifyContent:'flex-end', paddingRight:10}}>
                              <Text style={{height:'100%', fontSize:13, color:'#000'}}>{item.unitCount}({(item.unitCount/this.state.totalTeamUnitCount*100).toFixed(1)}%)</Text>
                          </Item>
                        </Item>
                    )}
                    keyExtractor={(item, index) => 'status' +index}
                />                          
              </View>}                
          </Content>              
      </Container>
    )
  }    
}

function mapStateToProps(state) {
    return {
        token: state.token, 
      
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setStatus: (st) => dispatch({type: 'SET_STATUS_SETTING', param:st}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(dashboardPage);