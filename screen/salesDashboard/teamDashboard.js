import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Title, Icon, Button, Item, DatePicker} from "native-base";
import { TouchableOpacity, FlatList, Image, Dimensions, Modal, View, ActivityIndicator} from 'react-native'
import {Pie} from 'react-native-pathjs-charts'
import {connect} from 'react-redux'
import {styles, colors} from '../globalStyle.js';
import Moment from 'moment'
import {api_getTeamSalesRanking, phaseId, blockId} from '../config.js'

class teamDashboard extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            filterDate:false,
            totalCount:0,
            salesList:[],
            configStatus:["fff","fff","fff","fff","fff","fff","fff","fff"],
            pieTeamStatusColor:[],
            teamStatusDetail:[],
            tableTeamSum:[],
        }
    }
    
    componentDidMount(){
        this.postRequestGetTeamSales()
    }
    
    postRequestGetTeamSales(){
        const {navigation} = this.props;
        this.setState({isLoading:true});
        return fetch(api_getTeamSalesRanking, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              startSalesDate: this.state.startDate,
              endSalesDate: this.state.endDate,
              includeSales: true,
              teamType: '',
              phaseId: [phaseId]
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
                if(result != null){
                    let sum = result.activeSalesSummary
                    let statusColor = result.statuses
                    let teamSale = result.teamSales
                  
                    let tol = [];
                    let list=[]; 
                    
                    let data = [];
                    let saleDetailTable = [];
                    let defaultColor = [{bgColor:'#fff', txtColor:'#fff'},{bgColor:'#fff', txtColor:'#fff'},{bgColor:'#fff', txtColor:'#fff'},{bgColor:'#fff', txtColor:'#fff'},{bgColor:'#fff', txtColor:'#fff'},{bgColor:'#fff', txtColor:'#fff'}]
                    let st = [];
                    
                    for(let i=0; i<statusColor.length; i++){
                        defaultColor[statusColor[i].status].bgColor = statusColor[i].bgColor;
                        defaultColor[statusColor[i].status].txtColor = statusColor[i].color;
                        defaultColor[statusColor[i].status].text = statusColor[i].text;
                    }
                    
                    for(let i=0; i<teamSale.length; i++){
                        let sale = teamSale[i].activeSalesSummary;
                        let t = 0
                        for( let y=0; y<sale.length; y++){
                            t+=sale[y].unitCount;  
                        }
                        tol.push(t)
                    }
                    
                    for(let i=0; i<teamSale.length; i++){
                        let sale = teamSale[i].activeSalesSummary
                        data = [];
                        st = [];
                        let ttl=[]
                        for( let y=0; y<sale.length; y++){
                            data.push({name:'', population:sale[y].unitCount})
                            
                            st.push(this.hexToRgb(defaultColor[sale[y].status].bgColor))
                            ttl.push(tol[i])
                        }
                        
                        list.push({teamName:teamSale[i].teamName, teamSaleData:data, pieTeamSaleColor:st, activeSalesSummary:sale, total:ttl})
                        
                    }                    
                    
                    this.setState({
                        salesList:list,
                        configStatus:defaultColor,
                    })
                    
                    
                    
                }
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          }); 
    }  
    
    hexToRgb(hex){
        let c;
        if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
            c= hex.substring(1).split('');
            if(c.length== 3){
                c= [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c= '0x'+c.join('');
            return {r:(c>>16)&255, g:(c>>8)&255, b:c&255};
        }
        throw new Error('Bad Hex');
    }
    
    moneyFormat(rm){
        return rm.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    
    
  render() {
      
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    } 
      
    let options = {
      margin: {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
      },
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').width,
      color: '#2980B9',
      r: 3,
      R: 150,
      legendPosition: 'topRight',
      animate: {
        type: 'oneByOne',
        duration: 200,
        fillTransition: 3
      },
      label: {
        fontFamily: 'Arial',
        fontSize: 15,
        fontWeight: true,
        color: '#000'
      }
    }      
      
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
          <Header style={styles.headerOverlap}>
            <Left>
              <Button transparent onPress={()=>this.props.navigation.goBack()}>
                <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
              </Button>
            </Left>
            <Body>
              <Text style={{color:colors.formTxtColor, textAlign:'center'}}>Team Sales Report</Text>
            </Body>
            <Right>
            </Right>
          </Header>
          {this.state.filterDate?
          <View style={{flexDirection:'row'}}>
            <Item style={{width:'50%', flexDirection:'row', borderColor:colors.btnColor}}>
                <Text style={{color:'#fff'}}>From: </Text>
                <DatePicker
                    defaultDate={new Date(this.state.startDate)}
                    minimumDate={new Date(1990, 1, 1)}
                    maximumDate={new Date()}
                    locale={"ms-my"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={Moment(new Date(this.state.startDate)).format('D/M/YYYY')}
                    textStyle={{ color: colors.btnColor }}
                    placeHolderTextStyle={{ color: colors.btnColor }}
                    onDateChange={(newDate)=>this.setState({startDate:newDate})}
                    disabled={false}
                    style={{alignSelf:'flex-end'}}
                />  
            </Item>
            <Item style={{width:'50%', flexDirection:'row', borderColor:colors.btnColor}}>
                <Text style={{color:'#fff'}}>To: </Text>
                <DatePicker
                    defaultDate={new Date(this.state.endDate)}
                    minimumDate={new Date(1990, 1, 1)}
                    maximumDate={new Date()}
                    locale={"ms-my"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={Moment(new Date(this.state.endDate)).format('D/M/YYYY')}
                    textStyle={{ color: colors.btnColor }}
                    placeHolderTextStyle={{ color: colors.btnColor }}
                    onDateChange={(newDate)=>this.setState({endDate:newDate})}
                    disabled={false}
                    style={{alignSelf:'flex-end'}}
                />
            </Item>
          </View>:<View/>}
          <View style={{padding:5, flexDirection:'row', justifyContent:'center'}}>
            {this.state.filterDate?
            <Button style={[styles.defaultBtn, {width:'40%'}]} onPress={()=>{this.setState({filterDate:false, startDate:null, endDate:null, isLoading:true}); setTimeout(()=>{this.postRequestGetTeamSales()},500)}}>
             <Text style={{color:colors.btnTxtColor, fontSize:13, textAlign:'center'}}>Show From Beginning</Text>
            </Button>:<View/>}
            <Button style={[styles.defaultBtn, {width:'40%', marginLeft:5}]} onPress={()=>this.state.filterDate?this.postRequestGetTeamSales():this.setState({filterDate:true, startDate:new Date(), endDate:new Date()})}>
             <Text style={{color:colors.btnTxtColor}}>{this.state.filterDate?'Apply Filter':'Filter Date'}</Text>
            </Button>
          </View>  
          
          <Content bounces={false}>
              {this.state.salesList.length == 0?
              <View style={{flexDirection:'column', marginTop:'40%', padding:50, justifyContent:'center'}}>
                  <Text style={{textAlign:'center', color:'#fff'}}>No Record Had Found In Selected Date Range</Text>
              </View>:
              <FlatList extraData={this.state.salesList} bounces={false} data={this.state.salesList}
                renderItem={({item}) =>(
                  <View style={{backgroundColor:'transparent'}}>
                    <View style={{flexDirection:'column', justifyContent:'flex-start'}}>
                        <Item style={{width:Dimensions.get('window').width, marginTop:10, borderColor:"transparent", justifyContent:'center'}}>
                            <Text style={{color:'#fff', fontWeight:'bold'}}>{item.teamName}</Text>
                        </Item>
                        {item.teamSaleData.length == 0?<View><Text style={{textAlign:'center', color:'#fff', fontSize:13}}>No Record Had Found In Selected Date Range</Text></View>:
                        <Pie
                          data={item.teamSaleData}
                          options={options}
                          pallete={item.pieTeamSaleColor}       
                          accessorKey="population" 
                        />}
                    </View>                  
                    {item.teamSaleData.length == 0?<View/>:
                    <Item style={{flexDirection:'row', backgroundColor:colors.btnColor, borderColor:'transparent', width:'100%'}}>
                      <Item style={{height:30, width:'35%', borderBottomWidth:0, justifyContent:'center', borderRightWidth:1, borderColor:'#000', paddingTop:8}}>
                          <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Status</Text>
                      </Item>
                      <Item style={{height:30, width:'40%', borderBottomWidth:0, justifyContent:'center', borderRightWidth:1, borderColor:'#000', paddingTop:8}}>
                          <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Amount</Text>
                      </Item>
                      <Item style={{height:30, width:'25%', borderBottomWidth:0, justifyContent:'center', borderColor:'transparent', paddingTop:8}}>
                          <Text style={{height:'100%', fontSize:13, color:colors.btnTxtColor}}>Unit Count</Text>
                      </Item>
                    </Item>}                  
                    <FlatList extraData={item.activeSalesSummary} bounces={false} data={item.activeSalesSummary}
                        renderItem={({item:item2, index}) =>(                  
                            <Item style={{flexDirection:'row', backgroundColor: index%2>0? '#fff':colors.lightGray, borderColor:'transparent', width:'100%'}}>
                              <Item style={{height:30, width:'35%', borderBottomWidth:0, paddingLeft:5, paddingTop:5, borderRightWidth:1, borderColor:'#000', backgroundColor:this.state.configStatus[item2.status].bgColor }}>
                                  <Text style={{height:'100%', fontSize:13, color:this.state.configStatus[item2.status].txtColor}}>{this.state.configStatus[item2.status].text}</Text>
                              </Item>
                              <Item style={{height:30, width:'40%', borderBottomWidth:0, paddingTop:5, borderRightWidth:1, borderColor:'#000', justifyContent:'flex-end', paddingRight:5}}>
                                  <Text style={{height:'100%', fontSize:13, color:'#000'}}>RM {this.moneyFormat(item2.totalValue)}</Text>
                              </Item>
                              <Item style={{height:30, width:'25%', borderBottomWidth:0, paddingTop:5, borderColor:'transparent', justifyContent:'flex-end', paddingRight:10}}>
                                  <Text style={{height:'100%', fontSize:13, color:'#000'}}>{item2.unitCount}({(item2.unitCount/item.total[index]*100).toFixed(1)}%)</Text>
                              </Item>
                            </Item>
                        )}
                        keyExtractor={(item, index) => 'status' +index}
                    />                          
                  </View>                    
                )}
                keyExtractor={(item, index) => 'teamSales' +index}
              />}                    
            </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token, 
      
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setStatus: (st) => dispatch({type: 'SET_STATUS_SETTING', param:st}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(teamDashboard);