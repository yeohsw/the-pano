import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Left, Right, Body, Title, Icon } from 'native-base'
import { View, ActivityIndicator, Dimensions, Image } from 'react-native'
import { api_getAllAnnouncement, api_markReadAnnoucement } from './config.js'
import Toast from 'react-native-root-toast';
import {styles, colors} from './globalStyle.js'
import {connect} from 'react-redux'

class prosalesPage extends Component {
    
    constructor(props){
        super(props)
    }
    
    componentDidMount(){
        this.postRequestGetAnnouncement()
    }
    
    postRequestGetAnnouncement(){
//        this.setState({
//            isLoading:true
//        })
        
        return fetch(api_getAllAnnouncement, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            filter: null,
            sorting: null
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                     
                let result = responseJson.result;
                let tdyDate = new Date()
                if(result != null){
                    let announcements = result.items.filter((x)=>new Date(tdyDate.getFullYear(), tdyDate.getMonth(), 1) <= new Date(x.creationTime) && x.read == false)
                    let offset = 20
                    let delayTime = 0
                    for(let i=0; i<announcements.length; i++){
                        this.showToast(announcements[i].title, offset, delayTime)
                        this.markRead(announcements[i].id)
                        offset+=60
                        delayTime += 500
                    }

                }
                else{
//                    alert(JSON.stringify(responseJson))
                    alert('Fail to get announcement') 
                }

          })
          .catch((error) =>{
            this.setState({
                isLoading: false,
            });            
//            console.error(error);
          });        
    } 
    
    markRead = async (id) => {
        try{
            const response = await this.postRequestMarkRead(id);
            const json = await response.json();
            if(!json.success){
                alert('Fail to mark read announcement.')
            }
        }
        catch(error){
            alert('Fail to mark read announcement.')
//            alert(JSON.stringify(json))
        }
    }    
    
    postRequestMarkRead(id){
        
        return fetch(api_markReadAnnoucement, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token              
          },
          body: JSON.stringify({
              id: id
          }),
        })
    }     
    
    showToast(msg, h, t){
        Toast.show(msg, {
            duration: 3000,
            position: h,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: t,
            onShow: () => {
                // calls on toast\`s appear animation start
            },
            onShown: () => {
                // calls on toast\`s appear animation end.
            },
            onHide: () => {
                // calls on toast\`s hide animation start.
            },
            onHidden: () => {
                // calls on toast\`s hide animation end.
            }
        });           
    }
    
    render(){
        const {navigate} = this.props.navigation
        return(
            <Container style={{backgroundColor:colors.bgColor}}>
                <Toast/>
                <Header style={styles.headerOverlap}>
                  <Left>
                    <Button transparent onPress={()=>this.props.navigation.navigate('login')}>
                      <Icon name='logout' style={{color:'#000000'}} type={'MaterialCommunityIcons'} />
                    </Button>
                  </Left>
                  <Body>
                    <Title style={{color:colors.formTxtColor}}>Prosales</Title>
                  </Body>
                  <Right>                   
                  </Right>
                </Header>
                <View style={{flexDirection:'column', justifyContent:'center', flex:1}}>
                    {this.props.permission.filter((x)=>x == "ProjectReport.BlockStatusBreakdownReport").length>0?
                    <Button onPress={()=>navigate('dashboard', {hideTabBar:true})} style={[styles.defaultBtn,{backgroundColor:colors.btnColor, marginBottom:5}]}>
                        <Text style={{color:colors.btnTxtColor}}>Sales Chart</Text>
                    </Button>:<View/>}
                    {this.props.permission.filter((x)=>x == "Sales.Dashboard").length > 0?<Button onPress={()=>navigate('booking', {hideTabBar:true})} style={[styles.defaultBtn,{backgroundColor:colors.btnColor, marginTop:5}]}>
                        <Text style={{color:colors.btnTxtColor}}>Sales Booking</Text>
                    </Button>:<View/>}
                    {this.props.permission.filter((x)=>x == "Sales.SaleListing").length >0?<Button onPress={()=>navigate('salesListing', {hideTabBar:true})} style={[styles.defaultBtn,{backgroundColor:colors.btnColor, marginTop:10}]}>
                        <Text style={{color:colors.btnTxtColor}}>Sales Listing</Text>
                    </Button>:<View/>}
                    {this.props.permission.filter((x)=>x == "Crm.FollowUp").length >0?
                    <Button onPress={()=>navigate('crm', {hideTabBar:true})} style={[styles.defaultBtn,{backgroundColor:colors.btnColor, marginTop:10, marginBottom:5}]}>
                        <Text style={{color:colors.btnTxtColor}}>Customer Listing</Text>
                    </Button>:<View/>}                        
                </View>
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token,
        permission:state.permission
    }
}

export default connect(mapStateToProps)(prosalesPage);
