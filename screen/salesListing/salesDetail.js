import React, { Component } from 'react';
import {Platform, FlatList, TouchableOpacity, Linking, Alert, View, ActivityIndicator} from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Item, Input, DatePicker, Label, Picker, Content, Card, CardItem } from 'native-base'
import {api_getSaleDetail} from '../config.js'
import {styles, colors} from '../globalStyle.js'

import {connect} from 'react-redux'
import Moment from 'moment'

class salesDetail extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            unitDetail:{},
            saleDetail:{},
            parties:[]
        }
    }
    
    componentDidMount(){
        this.postRequestGetFollowUpList();
    }
    
    postRequestGetFollowUpList(){
        
        const { navigation } = this.props;
        let unit = navigation.getParam('selected',{})
        
        this.setState({isLoading:true});
        return fetch(api_getSaleDetail, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            id:unit.id
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.result;
                if(result != null){
                
                    this.setState({
                        unitDetail:result.unit,
                        saleDetail:result,
                        parties:result.parties
                    })
                    
                }    
                else{
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    
  render() {
    let sale = this.state.saleDetail;
    let unit = this.state.unitDetail;
//    let party = this.state.parties;
      
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    } 
      
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            <Button transparent onPress={()=>this.props.navigation.goBack()}>
              <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
            </Button>
          </Left>
          <Body>
            <Text style={{fontWeight:'bold', textAlign:'center', color:colors.formTxtColor}}>Sales Detail</Text>
          </Body>
          <Right>
          </Right>
        </Header>
        <Content padder>
            <Card>
                <CardItem header style={{borderBottomWidth:1, paddingLeft:10, paddingRight:10, borderColor:colors.bgColor, backgroundColor:colors.btnColor}}>
                    <Text style={{width:'100%'}}>Unit properties</Text>  
                </CardItem>
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>Project</Text>  
                    <Text style={{width:'60%'}}>: {unit.blockPhaseProjectName}</Text>  
                </CardItem>  
                <CardItem>
                    <Text style={{width:'40%'}}>Project Phase</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: {unit.blockPhaseName}</Text>  
                </CardItem>   
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>Block</Text>  
                    <Text style={{width:'60%'}}>: {unit.blockName}</Text>  
                </CardItem>                    
                <CardItem> 
                    <Text style={{width:'40%', alignSelf:'flex-start'}}>Unit no.</Text>  
                    <Text style={{width:'60%'}}>: {unit.unitNumber}</Text>  
                </CardItem>                    
                <CardItem style={{backgroundColor:'#E6E6E6'}}> 
                    <Text style={{width:'40%', alignSelf:'flex-start'}}>Unit Type</Text>  
                    <Text style={{width:'60%'}}>: {unit.unitType}</Text>  
                </CardItem>                   
                <CardItem> 
                    <Text style={{width:'40%', alignSelf:'flex-start'}}>Unit View</Text>  
                    <Text style={{width:'60%'}}>: {unit.unitView}</Text>  
                </CardItem>                  
                <CardItem style={{backgroundColor:'#E6E6E6'}}> 
                    <Text style={{width:'40%', alignSelf:'flex-start'}}>Size</Text>  
                    <Text style={{width:'60%'}}>: {unit.sizeSqf} ft{'\u00B2'}</Text>  
                </CardItem>                  
                <CardItem> 
                    <Text style={{width:'40%', alignSelf:'flex-start'}}>Bumi Unit</Text>  
                    <Text style={{width:'60%'}}>: {unit.isBumi? 'Yes':'No'}</Text>  
                </CardItem>
            </Card>
            <Card>
                <CardItem header style={{borderBottomWidth:1, paddingLeft:10, paddingRight:10, borderColor:colors.bgColor, backgroundColor:colors.btnColor}}>
                    <Text style={{width:'100%'}}>Sale properties</Text>   
                </CardItem>  
                <CardItem>
                    <Text style={{width:'40%'}}>Solicitor</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: {sale.solicitorName}</Text>  
                </CardItem>                       
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>Status</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: {sale.statusText}</Text>  
                </CardItem>  
                <CardItem>
                    <Text style={{width:'40%'}}>Sales Number</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: {sale.salesNumber}</Text>  
                </CardItem>                  
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>Booking Date</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: {Moment(new Date(sale.salesDate)).format('DD/MMM/YYYY')}</Text>  
                </CardItem>
                <CardItem>
                    <Text style={{width:'40%'}}>List Price</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: RM {sale.listPrice!=undefined?sale.listPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","):''}</Text>  
                </CardItem>       
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>Price (ft{'\u00B2'})</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: RM {sale.priceSqf!=undefined?sale.priceSqf.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","):''}</Text>  
                </CardItem>       
                <CardItem>
                    <Text style={{width:'40%'}}>Net Purchase Price</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: RM {sale.netPurchasePrice!=undefined?sale.netPurchasePrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","):''}</Text>  
                </CardItem>        
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>SPA Price</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: RM {sale.spaPrice!=undefined?sale.spaPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","):''}</Text>  
                </CardItem>        
                <CardItem>
                    <Text style={{width:'40%'}}>Discounts</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: RM {sale.totalDiscount!=undefined?sale.totalDiscount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","):''}({sale.totalDiscountPercentage == 0 || sale.totalDiscountPercentage == undefined? 0:sale.totalDiscountPercentage.toFixed(2)}%)</Text>  
                </CardItem>        
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>Rebates</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: RM {sale.totalRebate!=undefined?sale.totalRebate.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","):''}({sale.totalRebatePercentage == 0 || sale.totalRebatePercentage == undefined? 0:sale.totalRebatePercentage.toFixed(2)}%)</Text>  
                </CardItem>                
            </Card>
            <Card>
                <FlatList extraData={this.state.parties} bounces={false} data={this.state.parties}
                renderItem={({item}) =>(
                    <Card>    
                        <CardItem header style={{borderBottomWidth:1, paddingLeft:10, paddingRight:10, borderColor:colors.bgColor, backgroundColor:colors.btnColor}}>
                            <Text style={{width:'100%'}}>{item.isPrincipal?'Principal Purchaser':'Joint Purchaser'}</Text>   
                        </CardItem>  
                        <CardItem style={{backgroundColor:'#E6E6E6'}}>
                            <Text style={{width:'40%'}}>Name</Text>  
                            <Text style={{width:'60%', alignSelf:'flex-start'}}>: {item.title} {item.fullName}</Text>  
                        </CardItem> 
                        <CardItem>
                            <Text style={{width:'40%'}}>Nationality</Text>  
                            <Text style={{width:'60%', alignSelf:'flex-start'}}>: {item.nationalityName}</Text>  
                        </CardItem>
                        <CardItem style={{backgroundColor:'#E6E6E6'}}>
                            <Text style={{width:'40%'}}>National Id</Text>  
                            <Text style={{width:'60%', alignSelf:'flex-start'}}>: {item.nationalId}</Text>  
                        </CardItem>
                        <CardItem>
                            <Text style={{width:'40%'}}>Race</Text>  
                            <Text style={{width:'60%', alignSelf:'flex-start'}}>: {item.raceName}</Text>  
                        </CardItem>
                        <CardItem style={{backgroundColor:'#E6E6E6'}}>
                            <Text style={{width:'40%'}}>Email Address</Text>  
                            <Text style={{width:'60%', alignSelf:'flex-start'}}>: {item.emailAddress}</Text>  
                        </CardItem>
                        <CardItem>
                            <Text style={{width:'40%'}}>Contact No</Text>  
                            <Text style={{width:'60%', alignSelf:'flex-start'}}>: {item.phones[0].number}</Text>  
                        </CardItem>
                    </Card>
                    )}
                    keyExtractor={(item, index) => 'discount' +index}
                />  
            </Card>
            {sale.referrerName != '' && sale.referrerName != null?
            <Card>
                <CardItem header style={{borderBottomWidth:1, paddingLeft:10, paddingRight:10, borderColor:colors.bgColor, backgroundColor:colors.btnColor}}>
                    <Text style={{width:'100%'}}>Referrer</Text>                      
                </CardItem>
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>Name</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: {sale.referrerName}</Text>  
                </CardItem>
                <CardItem style={{backgroundColor:'#E6E6E6'}}>
                    <Text style={{width:'40%'}}>National Id</Text>  
                    <Text style={{width:'60%', alignSelf:'flex-start'}}>: {sale.referrerNationalId}</Text>  
                </CardItem>
            </Card>
            :<View/>}
        </Content>
        <Item style={{borderColor:'transparent', margin:10, justifyContent:'center', flexDirection:'column'}}>
            <Button style={styles.defaultBtn} onPress={()=>this.props.navigation.navigate('salesPayment',{salesId:[sale.id], submitPayment:false})}>
                <Text style={{color:colors.btnTxtColor}}>Sales Payment</Text>
            </Button>
            <Button transparent style={[styles.defaultBtn,{marginTop:10, backgroundColor:'transparent'}]} onPress={()=>this.props.navigation.goBack()}>
                <Text style={{color:colors.btnColor}}>Back</Text>
            </Button>
        </Item>
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token, 
        permission:state.permission
    }
}


export default connect(mapStateToProps)(salesDetail);