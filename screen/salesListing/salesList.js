import React, { Component } from 'react';
import {Platform, FlatList, TouchableOpacity, Linking, Alert, View, ActivityIndicator} from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Item, Input, DatePicker, Label, Picker, Content, Card, CardItem } from 'native-base'
import {api_getSalesListing, phaseId} from '../config.js'
import {styles, colors} from '../globalStyle.js'

import {connect} from 'react-redux'
import Moment from 'moment'

class salesListPage extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            isLoading:false,
            unitSale:[],
            startSaleDate:Moment().subtract(1, 'months').calendar(),
            endSaleDate:new Date(),
            filter:''
        }
    }
    
    componentDidMount(){
        this.postRequestGetFollowUpList();
    }
    
    componentWillUnmount(){
        this.state = {}
    }
    
    componentDidUpdate(prevProps) {
        
      if(this.props.refresh == true){
          this.postRequestGetFollowUpList();
          this.props.setRefresh(false);
      }
      
    }     
    
    postRequestGetFollowUpList(){
        
        this.setState({isLoading:true});
        return fetch(api_getSalesListing, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            endSalesDate: new Date(this.state.endSaleDate),
            endSpaDueDate: null,
            endSpaPurchaserSignDate: null,
            filter: this.state.filter,
            incentiveSchemeIds: null,
            maxResultCount: 200000,
            phaseId: phaseId,
            saleSchemeIds: null,
            sellerUserId: null,
            skipCount: 0,
            solicitorId: null,
            sorting: null,
            startSalesDate: new Date(this.state.startSaleDate),
            startSpaDueDate: null,
            startSpaPurchaserSignDate: null,
            status: null,
            statuses: [],
            teamId: null
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.result;
                if(result != null){
                    result = result.items.filter((x)=> x.status != 0 && x.status != 1).sort((a,b)=> b.id - a.id)
//                    alert(JSON.stringify(result[0]))
                    this.setState({
                        unitSale:result
                    })
              
                }    
                else{
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    goPage(item){
       const {navigate} = this.props.navigation
       if(item.status != 2){
           navigate('salesDetail', {hideTabBar:true, selected:item})
       }
       else{
           Alert.alert(
               'Resume Reservation?',
               '',
               [
                {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Resume', onPress:()=>navigate('pricing', {hideTabBar:true, selected:[item]}) },
               ],
               {cancelable:false}
           )
           
       }
    }
    
  render() {
      
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    } 
      
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            <Button transparent onPress={()=>this.props.navigation.navigate('prosales',{hideTabBar:true})}>
              <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
            </Button>
          </Left>
          <Body>
            <Text style={{fontWeight:'bold', textAlign:'center', color:colors.formTxtColor}}>Sales Listing</Text>
          </Body>
          <Right>
          </Right>
        </Header>
        <View style={{flex:0.1, flexDirection:'row', marginTop:2, marginLeft:2, marginRight:2}}>
          <Item  style={{backgroundColor:'transparent', width:'100%', borderColor:colors.btnColor}}>
            <Icon name="ios-search" style={{color:colors.btnColor}}/>
            <Input placeholder='e.g Unit Number, NRIC, Name' style={{color:'#fff'}} autoCapitalize={('characters')} value={this.state.filter} onChangeText={(value) => {this.setState({filter:value})}} placeholderTextColor={colors.btnColor} />
          </Item>
        </View>            
        <View style={{flex:0.1, flexDirection:'row'}}>
            <Item style={{width:'50%', borderColor:colors.btnColor}}>
                <Text style={{color:colors.txtColor}}>From: </Text>
                <DatePicker
                    defaultDate={new Date(this.state.startSaleDate)}
                    androidMode={'spinner'}
                    minimumDate={new Date(1999, 1, 1)}
                    maximumDate={new Date()}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={Moment(new Date(this.state.startSaleDate)).format('D/M/YYYY')}
                    textStyle={{ color: "green" }}
                    placeHolderTextStyle={{ color: colors.btnColor }}
                    onDateChange={(newDate)=>this.setState({startSaleDate:newDate})}
                    disabled={false}
                />
            </Item>
            <Item style={{width:'50%', borderColor:colors.btnColor}}>
                <Text style={{color:colors.txtColor}}>To: </Text>
                <DatePicker
                    defaultDate={new Date(this.state.endSaleDate)}
                    minimumDate={new Date(1999, 1, 1)}
                    maximumDate={new Date()}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={Moment(new Date(this.state.endSaleDate)).format('D/M/YYYY')}
                    textStyle={{ color: "green" }}
                    placeHolderTextStyle={{ color: colors.btnColor }}
                    onDateChange={(newDate)=>this.setState({endSaleDate:newDate})}
                    disabled={false}
                />
            </Item>
        </View> 
        <View style={{flex:0.1, flexDirection:'row', justifyContent:'center', marginBottom:10}}>
            <Button style={[styles.defaultBtn,{marginTop:10}]} onPress={()=>this.postRequestGetFollowUpList()}>
                <Text style={{color:colors.btnTxtColor}}>Apply Filter</Text>
            </Button>
        </View>
        <Content style={{paddingLeft:10, paddingRight:10, paddingBottom:10}}>           
            {this.state.unitSale.length > 0?
            <FlatList extraData={this.state.unitSale} bounces={false} data={this.state.unitSale}
                renderItem={({item}) =>(
                  <TouchableOpacity onPress={()=> this.goPage(item)}>
                      <Card>
                        <CardItem style={{backgroundColor:item.statusBgColor}}>
                            <Text style={{width:'30%', color:item.statusTextColor}}>Sales Status</Text>  
                            <Text style={{width:'70%', color:item.statusTextColor}}>: {item.statusText}</Text>  
                        </CardItem>                    
                        <CardItem> 
                            <Text style={{width:'30%', alignSelf:'flex-start'}}>Unit</Text>  
                            <Text style={{width:'70%'}}>: {item.unitUnitNumber} ({item.unitBlockPhaseName})</Text>  
                        </CardItem>    
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Expiry Date</Text>  
                            <Text style={{width:'70%'}}>: {Moment(new Date(item.expiryTime)).format('DD/MMM/YYYY')=='01/Jan/1970'?'':Moment(new Date(item.expiryTime)).format('DD/MMM/YYYY')}</Text>  
                        </CardItem>   
                        <CardItem>
                            <Text style={{width:'30%'}}>Booking Date</Text>  
                            <Text style={{width:'70%', alignSelf:'flex-start'}}>: {Moment(new Date(item.salesDate)).format('DD/MMM/YYYY')}</Text>  
                        </CardItem>  
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>SPA Sign Due</Text>  
                            <Text style={{width:'70%', alignSelf:'flex-start'}}>: {Moment(new Date(item.spaSignDueDate)).format('DD/MMM/YYYY')}</Text>  
                        </CardItem>                              
                        <CardItem>
                            <Text style={{width:'30%'}}>Sold By</Text>  
                            <Text style={{width:'70%'}}>: {item.sellerDisplayNames}</Text>  
                        </CardItem>  
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%', alignSelf:'flex-start'}}>Purchasers</Text>  
                            <Text style={{width:'70%'}}>: {item.purchaserFullNames}</Text>  
                        </CardItem>
                        {item.solicitorName != null?
                        <CardItem>
                            <Text style={{width:'30%', alignSelf:'flex-start'}}>Solicitor</Text>  
                            <Text style={{width:'70%'}}>: {item.solicitorName}</Text>     
                        </CardItem>:
                        <View/>}
   
                      </Card> 
                  </TouchableOpacity>
                )}
                keyExtractor={(item, index) => 'discount' +index}
              />:<View style={{flexDirection:'column', justifyContent:'center', height:300, alignSelf:'center'}}><Text style={{color:'#fff', fontSize:20}}>No Record Found</Text></View>}  
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token, 
        refresh:state.refreshSalesList,
        permission:state.permission
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setID: (id) => dispatch({type: 'SET_FOLLOW_UP_ID', param:id}),
        setRefresh: (id) => dispatch({type: 'SET_REFRESH_SALESLIST', param:id})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(salesListPage);