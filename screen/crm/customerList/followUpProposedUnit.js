import React, { Component } from 'react';
import {Platform, FlatList, TouchableOpacity, Linking, Alert, View, Dimensions, ActivityIndicator} from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Item, Input, DatePicker, Label, Picker, Content, Card, CardItem, Textarea } from 'native-base'
import {api_getFollowUpProposedUnit, api_getFollowUpUnit, api_deleteFollowUpAction, api_getUnitByStatus, phaseId, api_createFollowUpAction} from '../../config.js'
import {styles, colors} from '../../globalStyle.js'

import {connect} from 'react-redux'
import Moment from 'moment'

class followUpProposedUnitPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            proposedUnitList:[],
            unitDetail:[],
            allUnit:[],
            addFollowUp:false,
            selectedUnit:'',
            remark:'',
        }
    }
    
    
    componentDidMount(){
        this.postRequestGetProposedUnitList();
    }
    
    postRequestGetProposedUnitList(){
        this.setState({isLoading:true});
        
        return fetch(api_getFollowUpProposedUnit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id:this.props.followUpId
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
 
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        proposedUnitList:result.items
                    })
                    this.postRequestGetUnitDetail();
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    } 
    
    postRequestGetUnitDetail(){
        
        return fetch(api_getFollowUpUnit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id:this.props.followUpId
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        unitDetail:result,
                    })
                   
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }  
    
    postRequestDeleteFollowUp(id){
        this.setState({isLoading:true});
        return fetch(api_deleteFollowUpAction, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id:id
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.success;
                if(result == true){
                    this.postRequestGetProposedUnitList();
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }   
    
    postRequestUnitByStatus(){
        this.setState({isLoading:true});
        return fetch(api_getUnitByStatus, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              statuses: [0],
              phaseIds: [
                phaseId
              ],
              unitIds: []
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        allUnit:result,
                        addFollowUp:true
                    })
                }    
                else{
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }  
    
    postRequestAddFollowUp(){
        
        let unit = this.state.allUnit.filter((x)=> x.id == this.state.selectedUnit)
        this.setState({isLoading:true})
        return fetch(api_createFollowUpAction, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({              
            actionType: 0,
            followUpId: this.props.followUpId,
            unitId: this.state.selectedUnit,
            startTime: new Date(),
            quotation: unit.listPrice,
            notes:this.state.remark,
            isEdit: false,
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
 
                let result = responseJson.success;
                if(result == true){
                    this.setState({
                        addFollowUp:false,
                        remark:'',
                        selectedUnit:''
                    })
                    this.postRequestGetProposedUnitList();
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                    this.setState({isLoading:false})                    
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }    
    
    getUnitDetail(unitId){
        let detail = this.state.unitDetail.filter((x)=>x.id == unitId);
        Alert.alert(
            detail[0].unitNumber,
            'Status: '+detail[0].getStatus+'\nSize: '+detail[0].sizeSqf+' ft\u00B2\nPrice(ft\u00B2): RM '+detail[0].listPricePerSizeSqf.toFixed(2)+'\nListing Price: RM '+(detail[0].listPrice.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
            [
                {text:'Noted', onPress:()=> console.log('cancel'), style:'cancel'}
            ],
            {cancelable:false}
        )
    }
    
    prompt(id){
        Alert.alert(
            'Are you sure?',
            'Remove selected proposed unit?',
            [
                {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Remove', onPress:()=> this.postRequestDeleteFollowUp(id)}
            ],
            {cancelable:false}
        )
    }
 
    addPropose(){
        Alert.alert(
            "Are you sure?",
            "Add this proposed unit?",
            [
                {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Add', onPress:()=>this.postRequestAddFollowUp()},
            ],
            {cancelable:false}
        )    
    }
    
    render(){
        
        if(this.state.isLoading){
            return ( 
                <Container style={{backgroundColor:colors.bgColor}}>
                  <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                    <ActivityIndicator size="large" color="#ffffff" />
                    <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
                  </View>
                </Container>
            )
        }            
        
        return(
            <Container style={{backgroundColor:colors.bgColor}}>
                 {this.state.addFollowUp?
                    <View style={{flexDirection:'column', zIndex:2, position:'absolute', height:Dimensions.get('window').height, width:Dimensions.get('window').width, padding:5}}>
                        <Card style={{width:'100%', alignSelf:'center', borderColor:'#00f'}}>
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>Notes Type:</Label>
                                <Item style={{borderColor:'transparent'}}>
                                <Picker
                                  mode="dropdown"
                                  iosHeader="Select Unit"
                                  iosIcon={<Icon name="arrow-down"/>}
                                  style={{width:undefined}}
                                  selectedValue={this.state.selectedUnit}
                                  onValueChange={(value)=>this.setState({selectedUnit:value, remark:'Proposed to purchase '+this.state.allUnit.filter((x)=>x.id == value).map((x)=>x.description).toString().replace('[]','')})}
                                >
                                    <Picker.Item value='' label='Select Unit' />
                                    {this.state.allUnit.map((item, index)=>{
                                        return <Picker.Item key={index} value={item.id} label={item.description} />
                                    })}
                                </Picker>
                                </Item>
                            </CardItem>
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>List Price</Label>
                                <Text style={{alignSelf:'flex-start'}}>RM {this.state.selectedUnit!=''?this.state.allUnit.filter((x)=>x.id == this.state.selectedUnit).map((y)=> y.listPrice).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","):'0'}</Text>
                            </CardItem>
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>Remark</Label>
                                <Item regular >
                                    <Textarea style={{width:'100%'}} rowSpan={5} onChangeText={(value) => {this.setState({remark:value})}} value={this.state.remark}/>
                                </Item>
                            </CardItem>
                            <CardItem style={{justifyContent:'center'}}>
                                <Button onPress={()=>this.setState({addFollowUp:false, remark:'', selectedUnit:''})} style={{width:100, justifyContent:'center', backgroundColor:'#f00', marginRight:5}}><Text>Cancel</Text></Button>
                                <Button onPress={()=>this.addPropose()} style={{width:100, justifyContent:'center', backgroundColor:'#0f0', marginLeft:5}}><Text style={{color:'#000000'}}>Add</Text></Button>
                            </CardItem>
                        </Card>
                    </View>:<View/>
                 }            
                 <FlatList extraData={this.state.proposedUnitList} bounces={false} data={this.state.proposedUnitList}
                    renderItem={({item}) =>(
                        <TouchableOpacity onPress={()=>this.getUnitDetail(item.unitId)} >
                          <Card>
                            {this.props.statusCase == 4 || this.props.statusCase == 5 || this.props.statusCase == 6 || this.props.statusCase == 7?<View/>:
                            <CardItem header> 
                                <Text style={{width:'95%'}}>{item.unitNumber}</Text>  
                                <Icon style={{fontSize:20, alignSelf:'flex-end', color:'#00f'}} name='circle-with-cross' type='Entypo' onPress={this.prompt.bind(this,item.id)}/>
                            </CardItem>}                              
                            <CardItem> 
                                <Text style={{width:'30%'}}>Phase:</Text>  
                                <Text style={{width:'70%'}}>: {item.phaseName}</Text>  
                            </CardItem> 
                            <CardItem>
                                <Text style={{width:'30%'}}>Description</Text>  
                                <Text style={{flexWrap:'wrap', width:'70%'}}>: {item.content}</Text>  
                            </CardItem> 
                            <CardItem>
                                <Text style={{width:'30%'}}>Date Added</Text>  
                                <Text>: {Moment(new Date(item.startTime)).format('D MMM YYYY')}</Text>  
                            </CardItem> 
                          </Card> 
                        </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => 'discount' +index}
                  />   
                  {this.state.addFollowUp || this.props.statusCase == 4 || this.props.statusCase == 5 || this.props.statusCase == 6 || this.props.statusCase == 7?<View/>:
                  <Button onPress={()=>this.postRequestUnitByStatus()} rounded style={{alignSelf:'center', backgroundColor:colors.btnColor, marginBottom:10}}><Text style={{color:colors.btnTxtColor}}>+ Add</Text></Button>}
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token, 
        followUpId:state.followUpId,
        statusCase:state.caseClose
    }
}

export default connect(mapStateToProps)(followUpProposedUnitPage);