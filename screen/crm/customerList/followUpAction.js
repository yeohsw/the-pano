import React, { Component } from 'react';
import {Platform, FlatList, TouchableOpacity, Linking, Alert, View} from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Item, Input, DatePicker, Label, Picker, Content, Card, CardItem, Tabs, Tab, TabHeading } from 'native-base'
import {api_getFollowUpCustomer, phaseId} from '../../config.js'
import {styles, colors} from '../../globalStyle.js'
import Tab1 from './followUpProposedUnit.js'
import Tab2 from './followUpAppointment.js'
import Tab3 from './followUpNote.js'

import {connect} from 'react-redux'
import Moment from 'moment'


class followUpActionsPage extends Component {
    
    componentDidMount(){
        const {navigation} = this.props;
        
    }
    
    render(){
        
        return(
            <Container style={{backgroundColor:colors.bgColor}}>
                <Header style={styles.headerOverlap}>
                  <Left>
                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                      <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
                    </Button>
                  </Left>
                  <Body>
                    <Text style={{fontWeight:'bold', textAlign:'center', color:colors.formTxtColor}}>Follow Up</Text>
                  </Body>
                  <Right>
                  </Right>
                </Header>
                <Tabs>
                  <Tab heading={ <TabHeading style={{backgroundColor:colors.btnColor}} ><Text style={{color:colors.formTxtColor}}>Proposed Unit</Text></TabHeading>}>
                    <Tab1 />
                  </Tab>
                  <Tab heading={ <TabHeading style={{backgroundColor:colors.btnColor}}><Text style={{color:colors.formTxtColor}}>Appointment</Text></TabHeading>}>
                    <Tab2 />
                  </Tab>
                  <Tab heading={ <TabHeading style={{backgroundColor:colors.btnColor}}><Text style={{color:colors.formTxtColor}}>Note</Text></TabHeading>}>
                    <Tab3 />
                  </Tab>
                </Tabs>                
            </Container>
        )
    }
    
}

function mapStateToProps(state) {
    return {
        token: state.token,        
    }
}

export default connect(mapStateToProps)(followUpActionsPage);