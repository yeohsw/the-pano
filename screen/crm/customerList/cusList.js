import React, { Component } from 'react';
import {Platform, FlatList, TouchableOpacity, Linking, Alert, View, ActivityIndicator} from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Item, Input, DatePicker, Label, Picker, Content, Card, CardItem } from 'native-base'
import {api_getFollowUpCustomer, phaseId} from '../../config.js'
import {styles, colors} from '../../globalStyle.js'

import {connect} from 'react-redux'
import Moment from 'moment'
import Autolink from 'react-native-autolink';

class cusListPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            filter:'',
            startDate:new Date(),
            endDate:new Date(),
            filterStatus:'',
            followUpList:[],
            status:['New', 'In Progress', 'In Progress', 'In Progress', 'Won', 'Lost', 'Lost', 'Lost'],
            totalRecord:0
        }
    }
    
    componentDidMount(){
        this.postRequestGetFollowUpList()
    }
    
    componentDidUpdate(prevProps) {
        
      if(this.props.refresh == true){
          this.postRequestGetFollowUpList();
          this.props.setRefresh(false);
      }
      
    }    
    
    postRequestGetFollowUpList(){
        
        let st = this.state.filterStatus;
        if(st == '0'){
            st = [0];
        }
        else if(st == '1'){
            st = [1,2,3];
        }
        else if(st == '2'){
            st = [4];
        }
        else if(st == '3'){
            st = [5,6,7];
        }
        
        this.setState({isLoading:true});
        return fetch(api_getFollowUpCustomer, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              priorities: [],
              statuses: st == ''?[]:[st],
              actions: [],
              attitudes: [],
              customerIds: [],
              sources: [],
              phaseIds: [],
              projectIds: [],
              eventIds: [],
              displayAssignorItems: true,
              displayAssigneeItems: true,
              displayNonActiveItems: true,
              displayCompletedItems: true,
              startAssignDate:this.state.startDate,
              filter: this.state.filter,
              sorting: null,
              skipCount: 0,
              maxResultCount: 2000000
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.result;
                if(result != null){
                    
                    this.setState({
                        followUpList:result.items.filter((x)=>Moment(x.assignedDate).format('L')<=Moment(this.state.endDate).format('L')),
                        totalRecord:result.items.filter((x)=>Moment(x.assignedDate).format('L')<=Moment(this.state.endDate).format('L')).length
                    })
//                    alert(st)
                }       
                else{
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }       
    
        
    watsapp(phone, title, name){
        phone = phone.replace('+','');
        Alert.alert(
            'Whatsapp '+title+name+'?',
            '',
            [
                {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Proceed', onPress:()=>Linking.openURL('https://api.whatsapp.com/send?phone='+phone+'&text=hi')}
            ],
            {cancelable:false}
        ) 
    }
    
  render() {
      
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    } 
      
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            <Button transparent onPress={()=>this.props.navigation.navigate('prosales',{hideTabBar:true})}>
              <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
            </Button>
          </Left>
          <Body>
            <Text style={{fontWeight:'bold', textAlign:'center', color:colors.formTxtColor}}>Search Customer</Text>
          </Body>
          <Right>
          </Right>
        </Header>
        <Item searchBar style={{paddingLeft:10}}>
            <Icon name="ios-search" style={{color:colors.txtColor}}/>
            <Input onChangeText={(value) => {this.setState({filter:value})}} value={this.state.filter} placeholder="Search" placeholderTextColor={colors.btnColor} style={{color:colors.btnColor}}/>
        </Item>
        <Item>
            <Label style={{color:colors.txtColor, paddingLeft:10}}>Filter Date (d/m/y):</Label>   
            <DatePicker
                defaultDate={new Date(this.state.startDate)}
                minimumDate={new Date(2017, 1, 1)}
                maximumDate={new Date()}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText={Moment(new Date(this.state.startDate)).format('D/M/YYYY')}
                textStyle={{ color: colors.btnColor }}
                placeHolderTextStyle={{ color: colors.btnColor }}
                onDateChange={(newDate)=>this.setState({startDate:newDate})}
                disabled={false}
                style={{alignSelf:'flex-end'}}
                />  
            <Label style={{color:colors.txtColor}}>-</Label>   
            <DatePicker
                defaultDate={new Date(this.state.endDate)}
                minimumDate={new Date(2019, 1, 1)}
                maximumDate={new Date()}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText={Moment(new Date(this.state.endDate)).format('D/M/YYYY')}
                textStyle={{ color: colors.btnColor }}
                placeHolderTextStyle={{ color: colors.btnColor }}
                onDateChange={(newDate)=>this.setState({endDate:newDate})}
                disabled={false}
                />  
        </Item>
        <Item>
            <Label style={{color:colors.txtColor, paddingLeft:10}}>Status :</Label>
            <Picker
              mode="dropdown"
              iosHeader="Select Status"
              iosIcon={<Icon name="arrow-down" style={{color:colors.btnColor}} />}
              style={Platform.OS == 'ios'?{width:undefined}:{ width: undefined, color: colors.btnColor}}
              textStyle={{color:colors.btnColor}}
              selectedValue={this.state.filterStatus}
              onValueChange={(value)=>this.setState({filterStatus:value})}
            >
                <Picker.Item value={''} label={'All Status'} />
                <Picker.Item value={'0'} label={'New'} />
                <Picker.Item value={'1'} label={'In Progress'} />
                <Picker.Item value={'2'} label={'Won'} />
                <Picker.Item value={'3'} label={'Lost'} />
            </Picker>
        </Item>
        <Item style={{justifyContent:'center', borderColor:'transparent'}}>
            <Button style={[styles.defaultBtn, {marginTop:5}]} onPress={()=>this.postRequestGetFollowUpList()}>
                <Text style={{color:colors.btnTxtColor}}>Apply Filter ({this.state.totalRecord})</Text> 
            </Button>
        </Item>
        <Content padder>
            {this.state.followUpList.length > 0?
            <FlatList extraData={this.state.followUpList} bounces={false} data={this.state.followUpList}
                renderItem={({item}) =>(
                 <TouchableOpacity onPress={()=>{this.props.setID(item.id);this.props.setStatus(item.status);this.props.navigation.navigate('followUpDetail',{hideTabBar:true, selected:item})}}>                      
                    <Card>
                        <CardItem style={{backgroundColor:colors.lightGray}}> 
                            <Text style={{width:'30%'}}>Name</Text>  
                            <Text style={{width:'70%'}}>: {item.customer.title}{item.customer.fullName}</Text>  
                        </CardItem> 

                        <CardItem>
                            <Text style={{width:'30%'}}>Status</Text>  
                            <Text>: {this.state.status[item.status]}</Text>  
                        </CardItem> 
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Assign Date</Text>  
                            <Text>: {Moment(new Date(item.assignedDate)).format('D MMM YYYY')}</Text>  
                        </CardItem> 
                        <CardItem>
                            <Text style={{width:'30%'}}>Email</Text>
                            <Text>: </Text>
                            <TouchableOpacity >
                                <Autolink text={item.customer.emailAddress} />
                            </TouchableOpacity>
                        </CardItem>
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Contact</Text>
                            <Text >: </Text>
                            <TouchableOpacity >
                                <Autolink text={item.customer.phones!=null?item.customer.phones[0].number:''}/>
                            </TouchableOpacity>
                            {item.customer.phones!=null?
                            <Icon onPress={()=>this.watsapp(item.customer.phones[0].number, item.customer.title,item.customer.fullName)} name='whatsapp-square' type='FontAwesome5' style={{color:'#0f0', marginLeft:10}}/>:<View/>
                            }
                        </CardItem>
                        <CardItem>
                            <Text style={{width:'30%', alignSelf:'flex-start'}}>Service Agent</Text>
                            <Text style={{alignSelf:'flex-start'}}>: {item.assigneeName}</Text>
                        </CardItem>                        
                      </Card> 
                    </TouchableOpacity>
                )}
                keyExtractor={(item, index) => 'discount' +index}
              />:
            <View style={{justifyContent:'center', flexDirection:'column', flex:1}}>
                <Text style={{marginTop:'20%', color:colors.txtColor, alignSelf:'center'}}>No Customer Assigned Today</Text>
                <Icon name='refresh' type='MaterialCommunityIcons' style={{color:colors.txtColor, alignSelf:'center'}} onPress={()=>this.postRequestGetFollowUpList()}/>
            </View>   
            }
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token, 
        refresh:state.refreshFollowUpList
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setID: (id) => dispatch({type: 'SET_FOLLOW_UP_ID', param:id}),
        setRefresh: (id) => dispatch({type: 'SET_REFRESH_FOLLOW_UP', param:id}),
        setStatus: (id) => dispatch({type: 'SET_CASE_CLOSE', param:id})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(cusListPage);