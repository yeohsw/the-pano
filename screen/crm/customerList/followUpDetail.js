import React, { Component } from 'react';
import {Platform, FlatList, TouchableOpacity, Linking, Alert, View, ActivityIndicator} from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, CardItem, Input, DatePicker, Label, Picker, Content, Card, Item } from 'native-base'
import {api_updateStatus, api_resetStatus} from '../../config.js'
import {styles, colors} from '../../globalStyle.js'

import {connect} from 'react-redux'
import Moment from 'moment'
import Autolink from 'react-native-autolink';

class followUpDetailPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            detail:[],
            status:['New', 'In Progress', 'In Progress', 'In Progress', 'Won', 'Lost', 'Lost', 'Lost'],
            remark:'',
            updateStatus:false
        }
    }
    
    componentDidMount(){

    }
    
    postRequestUpdateStatus(activityId, id, type){        
        this.setState({isLoading:true});
        return fetch(api_updateStatus, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              activityId: activityId,
              followUpIds: [id],
              remark: this.state.remark,
              status: type,
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.success;
                if(result == true){
                    this.setState({
                        updateStatus:false,
                        remark:''
                    })
                    this.props.setRefresh(true);
                    Alert.alert(
                        'Status Updated',
                        '',
                        [{text:'Noted', onPress:()=>this.props.navigation.goBack(), style:'cancel'}],
                        {cancelable:false}
                    )
                    
                   
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    } 
    
    postRequestResetStatus(id){        
        this.setState({isLoading:true});
        return fetch(api_resetStatus, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              completionPercentage: 0,
              followUpIds: [id]
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.success;
                if(result == true){

                    this.props.setRefresh(true);
                    Alert.alert(
                        'Status Updated',
                        '',
                        [{text:'Noted', onPress:()=>this.props.navigation.goBack(), style:'cancel'}],
                        {cancelable:false}
                    )
                    
                   
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }       
    
    updateStatus(activityId, id, type){
        if(this.state.remark == ''){
            Alert.alert('Write some remark.','Please provide some details about the update.',[{text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}], {cancelable:false})
        }
        else{
            type == 4?
            Alert.alert('Are You Sure', 'Mark this follow up as won?', [{text:'Cancel', onPress:()=>console.log('cancel'), style:'cancel'},{text:'Proceed', onPress:()=>this.postRequestUpdateStatus(activityId, id, type)}],{cancelable:false}):
            Alert.alert('Are You Sure', 'Mark this follow up as lost?', [{text:'Cancel', onPress:()=>console.log('cancel'), style:'cancel'},{text:'Proceed', onPress:()=>this.postRequestUpdateStatus(activityId, id, type)}],{cancelable:false})
        }
    }
    
    render(){
        const {navigation} = this.props;
        
        let info = navigation.getParam('selected',[]);
        let customer = info.customer;    
        
        if(this.state.isLoading){
            return ( 
                <Container style={{backgroundColor:colors.bgColor}}>
                  <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                    <ActivityIndicator size="large" color="#ffffff" />
                    <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
                  </View>
                </Container>
            )
        }         
        
        return(
            <Container style={{backgroundColor:colors.bgColor}}>
                <Header style={styles.headerOverlap}>
                  <Left>
                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                      <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
                    </Button>
                  </Left>
                  <Body>
                    <Text style={{fontWeight:'bold', textAlign:'center', color:colors.formTxtColor}}>Customer Detail</Text>
                  </Body>
                  <Right>
                  </Right>
                </Header> 
                <Content style={{padding:10}}>
                    <Card>
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Name</Text>
                            <Text>: {customer.title}{customer.fullName}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{width:'30%'}}>NRIC</Text>
                            <Text>: {customer.nationalId}</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Email</Text>
                            <Text>: {customer.emailAddress}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{width:'30%'}}>Contact No.</Text>
                            <Text>: {customer.phones != null? customer.phones[0].number:''}</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Status</Text>
                            <Text>: {this.state.status[info.status]}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{width:'30%'}}>Date Assigned</Text>
                            <Text>: {Moment(new Date(info.assignedDate)).format('D MMM YYYY')}</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Description</Text>
                            <Text style={{flexWrap:'wrap', width:'70%'}}>: {info.opportunity}</Text>
                        </CardItem>
                        {this.state.updateStatus?
                        <CardItem style={{flexDirection:'column', padding:10, borderColor:'transparent', backgroundColor:'#ffffff'}}>
                            <Text style={{alignSelf:'flex-start', color:this.state.remark == ''?'#f00':'#000'}}>Remark*</Text>
                            <Item regular>
                                <Input onChangeText={(value) => {this.setState({remark:value})}} value={this.state.remark}/>
                            </Item>
                        </CardItem>:<View/>
                        }
                        {this.state.updateStatus?
                        <CardItem style={{padding:10, backgroundColor:'#ffffff', justifyContent:'center'}}>
                            <Button onPress={()=>this.updateStatus(info.activityId, info.id, 4)} style={{backgroundColor:'#0f0', marginRight:5, marginBottom:10}}><Text style={{color:colors.btnTxtColor}}>Mark as Won</Text></Button>
                            <Button onPress={()=>this.updateStatus(info.activityId, info.id, 5)} style={{backgroundColor:'#f00', marginLeft:5, marginBottom:10}}><Text style={{color:colors.btnTxtColor}}>Mark as Lost</Text></Button>
                        </CardItem>:
                        <CardItem style={{padding:10, backgroundColor:'transparent', justifyContent:'center'}}>
                            {info.status != 4 && info.status != 5 && info.status != 6 && info.status != 7?
                                <View style={{backgroundColor:'#fff', justifyContent:'center', padding:10}}>
                                    <Button onPress={()=>this.setState({updateStatus:true})} style={styles.defaultBtn}><Text style={{color:colors.btnTxtColor}}>Update Status</Text></Button>    
                                </View>:<View/>
                            }
                        </CardItem>
                        }
                    </Card>
                </Content>
                {info.status != 0?<Button style={[styles.defaultBtn]} onPress={()=>this.postRequestResetStatus(info.id)}>
                    <Text style={{color:colors.btnTxtBtn}}>Reset Status</Text>
                </Button>:<View/>}
                <Button onPress={()=> {this.setState({updateStatus:false});this.props.navigation.navigate('followUpAction',{hideTabBar:true, selected:info})}} style={[styles.defaultBtn,{marginTop:10}]}><Text style={{color:colors.btnTxtColor}}>Follow Up</Text></Button>
                <Button onPress={()=>this.props.navigation.goBack()} transparent style={{alignSelf:'center'}}><Text style={{color:colors.btnColor}}>Back</Text></Button>
            </Container>
        )
    }
    
}

function mapStateToProps(state) {
    return {
        token: state.token,        
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setRefresh: (id) => dispatch({type: 'SET_REFRESH_FOLLOW_UP', param:id})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(followUpDetailPage);