import React, { Component } from 'react';
import {Platform, FlatList, TouchableOpacity, Linking, Alert, View, Dimensions, ActivityIndicator} from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Textarea, Text, Item, Input, DatePicker, Label, Picker, Content, Card, CardItem } from 'native-base'
import {api_getFollowUpNote, api_deleteFollowUpAction, api_createFollowUpAction, api_getFollowUpProposedUnit, api_getFollowUpUnit} from '../../config.js'
import {styles, colors} from '../../globalStyle.js'

import {connect} from 'react-redux'
import Moment from 'moment'

class followUpNotePage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            noteList:[],
            addFollowUp:false,
            noteType:'2',
            remark:'',
            attitude:'0',
            proposedUnit:'',
            quotation:'',
            unitDetail:[]
        }
    }
    
    
    componentDidMount(){
        this.postRequestGetNoteList();
    }
    
    postRequestGetNoteList(){
        this.setState({isLoading:true});
        return fetch(api_getFollowUpNote, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id:this.props.followUpId
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        noteList:result.items
                    })
                   
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    } 
    
    postRequestGetUnitDetail(){
        this.setState({isLoading:true})
        return fetch(api_getFollowUpUnit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id:this.props.followUpId
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        unitDetail:result,
                        addFollowUp:true
                    })
                   
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }      
    
    postRequestDeleteFollowUp(id){
        this.setState({isLoading:true});
        return fetch(api_deleteFollowUpAction, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id:id
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.success;
                if(result == true){
                    this.postRequestGetNoteList();
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }       
    
    postRequestAddFollowUp(){
        this.setState({isLoading:true});
        return fetch(api_createFollowUpAction, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({              
            actionType: 2,
            customerAttitude: this.state.attitude,
            followUpId: this.props.followUpId,
            id: 0,
            isEdit: false,
            noteType: this.state.noteType,
            notes:this.state.remark,
            unitId:this.state.proposedUnit == ''? null: this.state.proposedUnit,
            quotation:this.state.quotation ==''? 0: this.state.quotation
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({
                    isLoading:false
                })
                let result = responseJson.success;
                if(result == true){
                    this.setState({
                        addFollowUp:false,
                        noteType:'2',
                        remark:'',
                        attitude:'0',
                        proposedUnit:'',
                        quotation:'',
                        unitDetail:[]
                    })
                    this.postRequestGetNoteList();
                }    
                else{
                    let errorMsg = responseJson.error;
//                    alert(JSON.stringify(errorMsg.message));
                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }    
    
        
    prompt(id){
        Alert.alert(
            'Are you sure?',
            'Remove selected proposed unit?',
            [
                {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Remove', onPress:()=> this.postRequestDeleteFollowUp(id)}
            ],
            {cancelable:false}
        )
    }    
    
    addPropose(){
        Alert.alert(
            "Are you sure?",
            "Add this note?",
            [
                {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Add', onPress:()=>this.postRequestAddFollowUp()},
            ],
            {cancelable:false}
        )    
    }    
 
    render(){
        
        if(this.state.isLoading){
            return ( 
                <Container style={{backgroundColor:colors.bgColor}}>
                  <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                    <ActivityIndicator size="large" color="#ffffff" />
                    <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
                  </View>
                </Container>
            )
        }                    
        
        return(
            <Container style={{backgroundColor:colors.bgColor}}>
                 {this.state.addFollowUp?
                    <View style={{flexDirection:'column', zIndex:2, position:'absolute', height:Dimensions.get('window').height, width:Dimensions.get('window').width, padding:5}}>
                        <Card style={{width:'100%', alignSelf:'center', borderColor:'#00f'}}>
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>Notes Type:</Label>
                                <Item style={{borderColor:'transparent'}}>
                                    <Picker
                                      mode="dropdown"
                                      iosHeader="Select Note Type"
                                      iosIcon={<Icon name="arrow-down"/>}
                                      style={{width:undefined}}
                                      selectedValue={this.state.noteType}
                                      onValueChange={(value)=>this.setState({noteType:value})}
                                    >
                                        <Picker.Item value={''} label={'Select Note Type'} />
                                        <Picker.Item value={'1'} label={'Customer Feedback'} />
                                        <Picker.Item value={'2'} label={'Special Notes'} />
                                        <Picker.Item value={'3'} label={'Success Review'} />
                                        <Picker.Item value={'4'} label={'Failure Review'} />
                                    </Picker>
                                </Item>
                            </CardItem>
                            {this.state.noteType == '1'?
                                <CardItem style={{flexDirection:'column'}}>
                                    <Label style={{alignSelf:'flex-start'}}>Attitude:</Label>
                                    <Item style={{borderColor:'transparent'}}>
                                        <Picker
                                          mode="dropdown"
                                          iosHeader="Select Attitude"
                                          iosIcon={<Icon name="arrow-down"/>}
                                          style={{width:undefined}}
                                          selectedValue={this.state.attitude}
                                          onValueChange={(value)=>this.setState({attitude:value})}
                                        >
                                            <Picker.Item value={''} label={'Select Attitude'} />
                                            <Picker.Item value={'0'} label={'Cold'} />
                                            <Picker.Item value={'1'} label={'Warm'} />
                                            <Picker.Item value={'2'} label={'Hot'} />
                                        </Picker>                  
                                    </Item>
                                </CardItem>:<View />
                            }
                            {this.state.noteType == '3' || this.state.noteType == '4'?
                                <CardItem style={{flexDirection:'column'}}>
                                    <Label style={{alignSelf:'flex-start'}}>Proposed Unit:</Label>
                                    <Item style={{borderColor:'transparent'}}>
                                        <Picker
                                          mode="dropdown"
                                          iosHeader="Select Unit"
                                          iosIcon={<Icon name="arrow-down"/>}
                                          style={{width:undefined}}
                                          selectedValue={this.state.proposedUnit}
                                          onValueChange={(value)=>this.setState({proposedUnit:value, quotation:this.state.unitDetail.filter((x)=>x.id == value).map((x)=>x.listPrice).toString().replace('[]','')})}
                                        >
                                            <Picker.Item value='' label='Select Unit' />
                                            {this.state.unitDetail.map((item, index)=>{
                                                return <Picker.Item key={index} value={item.id} label={item.description} />
                                            })}
                                        </Picker>                  
                                    </Item>
                                </CardItem>:<View/>                                
                            }
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>Remark</Label>
                                <Item regular >
                                    <Textarea style={{width:'100%'}} rowSpan={5} onChangeText={(value) => {this.setState({remark:value})}} value={this.state.remark}/>
                                </Item>
                            </CardItem>
                            <CardItem style={{justifyContent:'center'}}>
                                <Button onPress={()=>this.setState({addFollowUp:false, noteType:'2', remark:'', attitude:'0', proposedUnit:'', unitDetail:[]})} style={{width:100, justifyContent:'center', backgroundColor:'#f00', marginRight:5}}><Text>Cancel</Text></Button>
                                <Button onPress={()=>this.addPropose()} style={{width:100, justifyContent:'center', backgroundColor:'#0f0', marginLeft:5}}><Text style={{color:'#000000'}}>Add</Text></Button>
                            </CardItem>                            
                        </Card>
                    </View>:<View/>
                 }
                 <FlatList extraData={this.state.noteList} bounces={false} data={this.state.noteList}
                    renderItem={({item}) =>(
                          <Card>
                            {this.props.statusCase == 4 || this.props.statusCase == 5 || this.props.statusCase == 6 || this.props.statusCase == 7?<View/>:                        
                            <CardItem>
                                <Text style={{width:'95%'}}></Text>
                                <Icon style={{fontSize:20, alignSelf:'flex-end', color:'#00f'}} name='circle-with-cross' type='Entypo' onPress={this.prompt.bind(this,item.id)}/>
                            </CardItem>}            
                            <CardItem> 
                                <Text style={{width:'30%'}}>Action:</Text>  
                                <Text style={{width:'70%'}}>: {item.action}</Text>  
                            </CardItem> 
                            <CardItem>
                                <Text style={{width:'30%'}}>Description</Text>  
                                <Text style={{flexWrap:'wrap', width:'70%'}}>: {item.content}</Text>  
                            </CardItem> 
                            <CardItem>
                                <Text>{item.timeline}</Text>  
                            </CardItem> 
                          </Card> 
                    )}
                    keyExtractor={(item, index) => 'discount' +index}
                  />  
                  {this.state.addFollowUp || this.props.statusCase == 4 || this.props.statusCase == 5 || this.props.statusCase == 6 || this.props.statusCase == 7?<View/>:
                  <Button onPress={()=>this.postRequestGetUnitDetail()} rounded style={{alignSelf:'center', backgroundColor:colors.btnColor, marginBottom:10}}><Text style={{color:colors.btnTxtColor}}>+ Add</Text></Button> }                 
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token,     
        followUpId:state.followUpId,
        statusCase:state.caseClose
    }
}

export default connect(mapStateToProps)(followUpNotePage);