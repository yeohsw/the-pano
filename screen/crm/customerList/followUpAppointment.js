import React, { Component } from 'react';
import {Platform, FlatList, TouchableOpacity, Linking, Alert, View, Dimensions} from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Textarea, Item, Input, DatePicker, Label, Picker, Content, Card, CardItem } from 'native-base'
import {api_getFollowUpAppointment, api_deleteFollowUpAction, api_createFollowUpAction} from '../../config.js'
import {styles, colors} from '../../globalStyle.js'

import {connect} from 'react-redux'
import Moment from 'moment'

class followUpProposedUnitPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            appointmentList:[],
            addFollowUp:false,
            remark:'',
            subject:'',
            selectedDate:'',
            hour:'0',
            min:'0'
        }
    }
    
    
    componentDidMount(){
        this.postRequestGetAppointmentList();
    }
    
    postRequestGetAppointmentList(){
        
        return fetch(api_getFollowUpAppointment, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id:this.props.followUpId
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
 
                let result = responseJson.result;
                if(result != null){
                    this.setState({
                        appointmentList:result.items
                    })
//                    alert(JSON.stringify(result.items))
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({uploadLoading:false})
            console.error(error);
          });          
    }   
    
    postRequestDeleteFollowUp(id){
        
        return fetch(api_deleteFollowUpAction, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              id:id
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
 
                let result = responseJson.success;
                if(result == true){
                    this.setState({
                        isLoading:false
                    })
                    this.postRequestGetAppointmentList();
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({uploadLoading:false})
            console.error(error);
          });          
    }    
    
   postRequestAddFollowUp(){

        return fetch(api_createFollowUpAction, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({              
            actionType: 1,
            action: this.state.subject,
            followUpId: this.props.followUpId,
            startTime: new Date(new Date(this.state.selectedDate).getFullYear(), new Date(this.state.selectedDate).getMonth(), new Date(this.state.selectedDate).getDate(), parseInt(this.state.hour), parseInt(this.state.min)),
            endTime: new Date(new Date(this.state.selectedDate).getFullYear(), new Date(this.state.selectedDate).getMonth(), new Date(this.state.selectedDate).getDate(), parseInt(this.state.hour), parseInt(this.state.min)),
            notes:this.state.remark,
            isEdit: false,
            task:{
                category:0,
                description:this.state.remark,
                startTime: new Date(new Date(this.state.selectedDate).getFullYear(), new Date(this.state.selectedDate).getMonth(), new Date(this.state.selectedDate).getDate(), parseInt(this.state.hour), parseInt(this.state.min)),
                endTime: new Date(new Date(this.state.selectedDate).getFullYear(), new Date(this.state.selectedDate).getMonth(), new Date(this.state.selectedDate).getDate(), parseInt(this.state.hour), parseInt(this.state.min)),
                subject: this.state.subject
            }
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
 
                let result = responseJson.success;
                if(result == true){
                    this.setState({
                        addFollowUp:false,
                        remark:'',
                        selectedDate:'',
                        subject:'',
                    })
                    this.postRequestGetAppointmentList();
                }    
                else{
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
//                    alert(JSON.stringify(responseJson));
                } 
                

          })
          .catch((error) =>{
            this.setState({uploadLoading:false})
            console.error(error);
          });          
    }    
    
        
    prompt(id){
        Alert.alert(
            'Are you sure?',
            'Remove selected appointment?',
            [
                {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Remove', onPress:()=> this.postRequestDeleteFollowUp(id)}
            ],
            {cancelable:false}
        )
    }
    
    addPropose(){
        let msg = '';
        this.state.subject == ''? msg = "Subject field cannot be blank.":'';
        this.state.selectedDate == ''? msg = "Please provide the appointment date.":'';
        parseInt(this.state.min)>59 || parseInt(this.state.hour)>24? msg = "Invalid time given, please correct it and try again.":''
        msg == ''?
        Alert.alert(
            "Are you sure?",
            "Add this appointment?",
            [
                {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                {text:'Add', onPress:()=>this.postRequestAddFollowUp()},
            ],
            {cancelable:false}
        ):  
        Alert.alert(
            "Information Incomplete",
            msg,
            [
                {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'},
            ],
            {cancelable:false}
        )    
    }    
 
    render(){
        
        return(
            <Container style={{backgroundColor:colors.bgColor}}>
              {this.state.addFollowUp?
                    <View style={{flexDirection:'column', zIndex:2, position:'absolute', height:Dimensions.get('window').height, width:Dimensions.get('window').width, padding:5}}>
                        <Card style={{width:'100%', alignSelf:'center', borderColor:'#00f'}}>
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>Subject*:</Label>
                                <Item regular style={{borderColor:'#000'}}>
                                    <Input onChangeText={(value) => {this.setState({subject:value})}} value={this.state.subject}/>
                                </Item>
                            </CardItem>
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>Date of appointment*</Label>
                                <Item style={{alignSelf:'flex-start', width:'100%', borderColor:'transparent'}}>
                                    <DatePicker
                                        defaultDate={new Date()}
                                        minimumDate={new Date(2019, 1, 1)}
                                        maximumDate={new Date(2100, 12, 31)}
                                        locale={"en"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText="Select date"
                                        textStyle={{ color: "#000000" }}
                                        placeHolderTextStyle={{ color: "#d3d3d3" }}
                                        onDateChange={(newDate)=>this.setState({selectedDate:newDate})}
                                        disabled={false}
                                    />
                                </Item>
                            </CardItem>
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>Time*</Label>
                                <Item style={{flexDirection:'row', width:'100%', borderColor:'transparent'}}>
                                    <Item regular style={{width:'15%', borderColor:parseInt(this.state.hour)>24?'#f00':'#000'}}>
                                        <Input maxLength={2} onChangeText={(value) =>this.setState({hour:value.replace(/[^0-9]/g, '')})} value={this.state.hour} style={{textAlign:'right'}}/>
                                    </Item>
                                    <Text style={{marginLeft:5}}>{this.state.hour == 0 || this.state.hour>12?"PM":"AM"}</Text>
                                    <Item regular style={{width:'15%', marginLeft:10, borderColor:parseInt(this.state.min)>59?'#f00':'#000'}}>
                                        <Input maxLength={2} onChangeText={(value) => {this.setState({min:value.replace(/[^0-9]/g, '')})}} value={this.state.min} style={{textAlign:'right'}}/>
                                    </Item>  
                                    <Text style={{marginLeft:5}}>Minutes</Text>
                                </Item>
                            </CardItem>
                            <CardItem style={{flexDirection:'column'}}>
                                <Label style={{alignSelf:'flex-start'}}>Remark</Label>
                                <Item regular style={{borderColor:'#000'}}>
                                    <Textarea style={{width:'100%'}} rowSpan={5} onChangeText={(value) => {this.setState({remark:value})}} value={this.state.remark}/>
                                </Item>
                            </CardItem>
                            <CardItem style={{justifyContent:'center'}}>
                                <Button onPress={()=>this.setState({addFollowUp:false, remark:'', selectedDate:'', subject:''})} style={{width:100, justifyContent:'center', backgroundColor:'#f00', marginRight:5}}><Text>Cancel</Text></Button>
                                <Button onPress={()=>this.addPropose()} style={{width:100, justifyContent:'center', backgroundColor:'#0f0', marginLeft:5}}><Text style={{color:'#000000'}}>Add</Text></Button>
                            </CardItem>
                        </Card>
                    </View>:<View/>
                 }             
                 <FlatList extraData={this.state.appointmentList} bounces={false} data={this.state.appointmentList}
                    renderItem={({item}) =>(
                          <Card>
                            {this.props.statusCase == 4 || this.props.statusCase == 5 || this.props.statusCase == 6 || this.props.statusCase == 7?<View/>:
                            <CardItem>
                                <Text style={{width:'95%'}}></Text>
                                <Icon style={{fontSize:20, alignSelf:'flex-end', color:'#00f'}} name='circle-with-cross' type='Entypo' onPress={this.prompt.bind(this,item.id)}/>
                            </CardItem>}                
                            <CardItem>
                                <Text style={{width:'30%'}}>Date Booked</Text>  
                                <Text>: {Moment(new Date(item.startTime)).format('DD MMM YYYY hh:mma')}</Text>  
                            </CardItem>         
                            <CardItem> 
                                <Text style={{width:'30%'}}>Subject</Text>  
                                <Text style={{width:'70%'}}>: {item.action}</Text>  
                            </CardItem> 
                            <CardItem>
                                <Text style={{width:'30%'}}>Description</Text>  
                                <Text style={{flexWrap:'wrap', width:'70%'}}>: {item.content}</Text> 
                            </CardItem> 
                          </Card> 
                    )}
                    keyExtractor={(item, index) => 'discount' +index}
                  />    
                           {this.state.addFollowUp || this.props.statusCase == 4 || this.props.statusCase == 5 || this.props.statusCase == 6 || this.props.statusCase == 7?<View/>:<Button onPress={()=>this.setState({addFollowUp:true})} rounded style={{alignSelf:'center', backgroundColor:colors.btnColor, marginBottom:10}}><Text style={{color:colors.btnTxtColor}}>+ Add</Text></Button>}                
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token,        
        followUpId:state.followUpId,
        statusCase:state.caseClose
    }
}

export default connect(mapStateToProps)(followUpProposedUnitPage);