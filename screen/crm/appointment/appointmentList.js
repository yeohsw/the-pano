import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text } from 'native-base';
export default class HeaderIconExample extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={()=>this.props.navigation.navigate('login')}>
              <Icon name='logout' style={{color:'#000000'}} type={'MaterialCommunityIcons'} />
            </Button>
          </Left>
          <Body>
            <Text style={{fontWeight:'bold', textAlign:'center'}}>Appointment List</Text>
          </Body>
          <Right>
          </Right>
        </Header>
      </Container>
    )
  }
}