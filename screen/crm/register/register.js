import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Input, Item, Text, Label, Content, Picker, DatePicker, Textarea, Form, CheckBox, Card, CardItem} from 'native-base';
import { View, TouchableOpacity, FlatList, Dimensions, Image, Alert, ActivityIndicator} from 'react-native'
import {styles, colors} from '../../globalStyle.js'
import {api_findCustomer, api_getCustomerForEdit, api_getCountriesNumber, api_getCustomerTitle, api_uploadImage, api_updateFieldImage, api_createOrUpdateCustomer, api_getCustomers, api_createFollowUpCustomer, api_getFollowUpCustomerForEdit} from '../../config.js'
import {connect} from 'react-redux'
import ImagePicker from 'react-native-image-picker'
import Moment from 'moment'

class registerPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            id:'',
            ownerUserId:'',
            resourceKey:'',
            addressId:'0',
            phoneId:'0',
            createNew:false,
            searchOutput:'',
            searchResult:[],
            titleList:[],
            raceList:[],
            countryList:[],
            eventList:[],
            phaseList:[],
            serverList:[],
            fieldList:[],
            followUpCustomerSetting:[],
            followUpAgentList:[],
            assignAgent:false,
            followUpRemark:'',
            followAssignedAgent:'',
            inputCountry:'',
            inputTitle:'',
            inputFullName:'',
            inputIC:'',
            inputBirthDate:'',
            inputGender:'',
            inputRace:'',
            inputCountryCode:'',
            inputPhase:'',
            inputEvent:'',
            inputServiceAgent:[],
            inputEmail:'',
            inputOccupation:'',
            inputRelationship:'',
            inputMarital:'',
            inputRemark:'',
            inputStreet:'',
            inputCity:'',
            inputState:'',
            inputPostCode:'',
            inputAddCountry:'',
            inputContact:'',            
            inputAttitude:'',
            selectedAgent:'',
            countryCodeList:[],
            imageUri:{},
            imageData:'',
            imageId:'',
            imageMapId:'',
            imageMandatory:false,
            particular:false,
            contact:false,
            misc:false,
            field:false,
            fieldsEmpty:false,
            particularValidate:'#ff0000',
            contactValidate:'#f00',
            miscValidate:'#f00',
            fieldValidate:'#f00',
            
        }
    }
    
    componentDidMount(){
        
    }
    
//    Crm.FollowUpAssignCustomer
    
    postRequestFindCustomer(){
        this.setState({isLoading:true});
        return fetch(api_findCustomer, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
            filter: this.state.searchOutput,
            sorting: null,
            skipCount: 0,
            maxResultCount: 200000
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false});
                let result = responseJson.result;

                if(result != null){
                    this.setState({
                        searchResult:result.items,
                    })
                    if(result.items.length == 0){
                        Alert.alert(
                            'No Record Found',
                            "Register this customer?",
                            [
                                {text:'Back', onPress:()=> console.log('cancel'), style:'cancel'},
                                {text:'Proceed', onPress:()=> {this.postRequestGetCustomerForEdit({});}}
                            ],
                            {cancelable:false}
                        )                        
                        
                    }
                }     

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetCustomerForEdit(value){
        this.setState({isLoading:true, createNew:true});
        return fetch(api_getCustomerForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body:JSON.stringify(value),
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;
              
                if(result != null){
                    let customer = result.customer;
                    customer = customer.fields.filter((x)=> x.isActive == true)
                    
                    
                    let detail = result.customer;
                    
                    this.setState({
                        id:detail.id,
                        ownerUserId:detail.ownerUserId,
                        resourceKey:detail.resourceKey,
                        addressId:detail.mailingAddress.id,
                        phoneId:detail.phonese!=null?detail.phones.id:'0',
                        fieldList:customer,
                        countryList:result.countries,
                        raceList:result.races,
                        eventList:result.events,
                        phaseList:result.phases,
                        serverList:result.servers,
                        
                        inputCountry:detail.nationalityCode==null?'MY':detail.nationalityCode,
                        inputTitle:detail.title,
                        inputFullName:detail.fullName,
                        inputIC:detail.nationalId,
                        inputBirthDate:detail.birthDate,
                        inputGender:''+detail.gender,
                        inputRace:''+detail.raceId,
                        inputCountryCode:'60',
                        inputPhase:detail.customerPhases != null ? JSON.stringify(detail.customerPhases[0]):[],
                        inputEvent:detail.customerEvents != null ? JSON.stringify(detail.customerEvents[0]):[],
                        inputServiceAgent:detail.customerServers == null?result.servers.map((x)=>x.name):result.servers.filter((x)=>x.value == detail.customerServers.filter((y)=>y == x.value).map((v)=>v.value)).map((z)=>z.name),
                        inputEmail:detail.emailAddress,
                        inputOccupation:detail.occupation,
                        inputRelationship:detail.relationship,
                        inputMarital:''+detail.maritalStatus,
                        inputRemark:detail.remarks,
                        inputStreet:detail.mailingAddress.street,
                        inputCity:detail.mailingAddress.city,
                        inputState:detail.mailingAddress.state,
                        inputPostCode:detail.mailingAddress.postcode,
                        inputAddCountry:''+detail.mailingAddress.countryCode,
                        inputContact:detail.phones != null ? detail.phones[0].number:'',            
                        inputAttitude:''+detail.attitude,
                    });
//                    alert(JSON.stringify(detail.customerServers))
                    this.postRequestGetTitle();
                 
                }
                else{
                    alert('Internal Error')
                    this.setState({isLoading:false})
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetCustomers(filter){
        this.setState({isLoading:true});
        return fetch(api_getCustomers, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body:JSON.stringify({
            countRecord:true,
            filter:filter,
            maxResultCount:200000,
            skipCount:0
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;
                this.setState({isLoading:false});
                if(result != null){
                    this.setState({
                        searchResult:result.items
                    })
                    
                    if(result.items.length == 0){
                        Alert.alert(
                            'No Record Found',
                            "Register this customer?",
                            [
                                {text:'Back', onPress:()=> console.log('cancel'), style:'cancel'},
                                {text:'Proceed', onPress:()=> {this.postRequestGetCustomerForEdit({});}}
                            ],
                            {cancelable:false}
                        )                        
                        
                    }                    
                }
                else{
                    alert('Internal Error')
                    this.setState({isLoading:false})
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetTitle(){
        return fetch(api_getCustomerTitle, {
            method:'POST',
            headers: {
                Accept:'application/json',
                'Content-Type':'application/json',
                Authorization:'bearer '+this.props.token
            }
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    this.setState({
                        titleList:result.items
                    })
                    this.postRequestGetCountryCode()
                }
                else{
                    alert('Internal Error')
                    this.setState({isLoading:false})
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });             
    }

    postRequestGetCountryCode(){
        return fetch(api_getCountriesNumber, {
            method:'POST',
            headers: {
                Accept:'application/json',
                'Content-Type':'application/json',
                Authorization:'bearer '+this.props.token
            }
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;
                this.setState({isLoading:false});
                if(result != null){
                    this.setState({
                        countryCodeList:result.items
                    })
                }
                else{
                    alert('Internal Error')
                    this.setState({isLoading:false})
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });             
    }
    
    postRequestUploadFormImage(data){
  
        this.setState({isLoading:true});
        
        const formdata = new FormData();
        formdata.append('file', {uri: "data:image/png;base64,"+data, type: 'image/jpg', name: 'image1.jpg'});
        formdata.append('objectType', 'FieldImage');

        return fetch(api_uploadImage, {
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'bearer '+this.props.token
          },            
          body:formdata
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
                let id ='';
                if(result != null){
  
                    this.setState({imageId:result.fileName})
                }    
                else{
                    
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            alert(JSON.stringify(error))
            console.error(error);
          });          
    }   
    
    postRequestUpdateImage(cusId){
        
        return fetch(api_updateFieldImage, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body: JSON.stringify({
              originalFileName: "image1.jpg",
              fileName: this.state.imageId,
              height: 0,
              width: 0,
              x: 0,
              y: 0,
              customerId: cusId,
              mappingId: this.state.imageMapId
            })
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.success;
                if(result == true){
                    Alert.alert(
                        'Successfully Register',
                        'Thank you for register as our prosales customer.',
                        [
                            {text:'Noted', onPress:()=>this.setState({createNew:false, searchResult:[]}), style:'cancel'}
                        ],
                        {cancelable:false}
                    );
                     
                
                }    
                else{
//                    alert(JSON.stringify(this.state.salePartyDetail))
                    let errorMsg = responseJson.error;
                    alert(JSON.stringify(errorMsg.message));
                } 
                

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }   
    
    postRequestRegisterCustomer(){
        this.setState({isLoading:true});
        return fetch(api_createOrUpdateCustomer, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body:JSON.stringify({
              id: this.state.id,
              ownerUserId: this.state.ownerUserId,
              resourceKey: this.state.resourceKey,
              title: this.state.inputTitle,
              fullName: this.state.inputFullName,
              nationalId: this.state.inputIC,
              nationalityCode: this.state.inputCountry,
              raceId: this.state.inputRace,
              gender: this.state.inputGender,
              birthDate: this.state.inputBirthDate,
              emailAddress: this.state.inputEmail,
              occupation: this.state.inputOccupation,
              relationship: this.state.inputRelationship,
              maritalStatus: this.state.inputMarital,
              acceptsNewsletters: true,
              isActive: true,
              attitude: this.state.inputAttitude,
              remarks: this.state.inputRemark,
              contactTitle: null,
              contactName: null,
              contactNationalId: null,
              isCorporate: false,
              corporateType: null,
              newRemarks: "",
              fields: this.state.fieldList,
              isBuyer: false,
              customerEvents: this.state.inputEvent == ''|| this.state.inputEvent == undefined?[]:[
                this.state.inputEvent
              ],
              customerPhases: this.state.inputPhase == ''|| this.state.inputPhase == undefined?[]:[
                this.state.inputPhase
              ],
              customerServers: this.state.serverList.filter((x)=>x.name == this.state.inputServiceAgent.filter((y)=> y == x.name)).map((z)=>z.value),
              mailingAddress: {
                description: null,
                street: this.state.inputStreet,
                city: this.state.inputCity,
                state: this.state.inputState,
                postcode: this.state.inputPostCode,
                countryCode: this.state.inputAddCountry,
                id: this.state.addressId
              },
              "phones": [
                {
                  number: this.state.inputContact.indexOf('+')>=0?this.state.inputContact:'+'+this.state.inputCountryCode+this.state.inputContact,
                  description: 'H/P',
                  id: this.state.phoneId
                }
              ],
              otherContacts: null
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;

                if(result != null){
                    
                    if(this.state.imageMandatory){
                        this.postRequestUpdateImage(result);
                    }
                    else{
                        this.setState({isLoading:false});
                        Alert.alert(
                            this.state.searchResult.length>0?'Successfully Updated':'Successfully Register',
                            'Thank you for chosing our prosales system.',
                            [
                               {text:'Noted', onPress:()=>this.setState({createNew:false, searchResult:[], searchOutput:''}), style:'cancel'}
                            ],
                            {cancelable:false}
                        )    
                    }
                }
                else{
                    alert('Internal Error, fail to register new customer. '+ JSON.stringify(responseJson));
                    this.setState({isLoading:false})
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestGetFollowUpCustomersForEdit(){
        this.setState({isLoading:true});
        return fetch(api_getFollowUpCustomerForEdit, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body:JSON.stringify({
            id:this.state.id
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.result;
                this.setState({isLoading:false, assignAgent:true});
                if(result != null){
                    this.setState({
                        followUpCustomerSetting:result,
                        followUpAgentList:result.availableAgents
                    })              
                    
                }
                else{
                    alert('Internal Error')
                    this.setState({isLoading:false})
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }    
    
    postRequestCreateFollowUpCustomer(){
        let info = this.state.followUpCustomerSetting;
        this.setState({isLoading:true});
        return fetch(api_createFollowUpCustomer, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+this.props.token
          },
          body:JSON.stringify({
              opportunity: this.state.followUpRemark,
              customerId: info.customerId,
              customerName: info.customerName,
              assignedTime: new Date(),
              assignedBy: info.assignedBy,
              assignee: this.state.followAssignedAgent,
              expectedSale: 0,
              probability: 1,
              closingDate: new Date(),
              priority: 60,
              status: 0,
              isActive: true,
              leadSource: ['NA'],
              id: 0,
              availableAgents:info.availableAgents,
              availableUnits:info.availableUnits,
              defaultSettings:info.defaultSettings,
              events:info.events,
              freebies:info.freebies,            
              incentiveSchemes:info.incentiveSchemes,
              leadSources:info.leadSources,
              phaseDiscounts: null,
              phaseRebates: null,
              phases:info.phases,
              propertyTypes:info.propertyTypes,
              statuses:info.statuses
              
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {

                let result = responseJson.success;
                this.setState({isLoading:false})
                if(result == true){
                    Alert.alert(
                        'Successfully Assigned',
                        'This agent had been successfully assigned.',
                        [
                            {text:'Noted', onPress:()=>this.setState({createNew:false, searchResult:[], assignAgent:false}), style:'cancel'}
                        ],
                        {cancelable:false}
                    );                    
                }
                else{
                    alert('Internal Error, fail to follow up this customer. '+ JSON.stringify(responseJson));
                    this.setState({isLoading:false})
                }

          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    registerValidation(){
        let msg = '';
        this.state.fieldList.filter((x)=>x.isMandatory == true && x.answer == null).length >0? msg = 'Please do not leave any mandatory fields blank.':'';
        
        this.state.inputContact == ''? msg = 'Please provide your contact.':'';        
        this.state.inputStreet == ''? msg = 'Please provide your street address.':'';
        this.state.inputCity == ''? msg = 'Please provide your city/town.':'';
        this.state.inputFullName == ''? msg = 'Please provide your full name.':'';
        this.state.inputRace == 'null'? msg = 'Please provide your race':'';
        this.state.inputTitle == ''? msg = 'Please select at least one title.':'';
        this.state.inputCountry == ''? msg = 'Please select at least one nationality.':'';        
     

        
        if(msg != ''){
            Alert.alert(
                'Information Incomplete',
                msg,
                [
                    {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                ],
                {cancelable:false}
            )
        }
        else{
            Alert.alert(
                'Are you sure?',
                'Kindly confirm all the information given is correct before proceed.',
                [
                    {text:'Back', onPress:()=>console.log('cancel'), style:'cancel'},
                    {text:'Proceed', onPress:()=>this.postRequestRegisterCustomer()}
                ],
                {cancelable:false}
            )
            
        }
        
    }
    
    validateFollowUp(){
        let msg = '';
        this.state.followAssignedAgent == ''? msg = 'Please select at least one agent.':'';
        this.state.followUpRemark == ''? msg = 'Please provide a remark.':'';
        
        if(msg == ''){
              this.postRequestCreateFollowUpCustomer();
        }
        else{
            Alert.alert(
                'Information Incomplete',
                msg,
                [
                    {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                ],
                {cancelable:false}
            )
        }
    }
    
  uploadFormImage(mapId, isMandatory){
      
    ImagePicker.showImagePicker({
          title: 'Select Upload Option',
          takePhotoButtonTitle:'Take Photo',
          chooseFromLibraryButtonTitle:'Upload From Gallery',
          maxWidth:300,
          maxHeight:300,
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
      }, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            

        let img = this.state.fieldList;
        
        for(let i=0; i<img.length; i++){
            if(img[i].mappingId == mapId){
                img[i].answer == '';
            }
        }
        
        this.setState({
          imageUri: source,
          imageData: response.data,
          imageMapId: mapId,
          imageMandatory: isMandatory,
          fieldList:img
        });
          
        this.postRequestUploadFormImage(response.data);
      }
    });      
  }    
    
  render() {
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    }       
      
    register=(
        <Container>
            <Item style={{padding:10, backgroundColor:colors.btnColor, borderColor:this.state.inputRace != 'null' && this.state.inputCountry !='' && this.state.inputTitle !='' && this.state.inputFullName !='' && this.state.inputCountry != null && this.state.inputTitle != null && this.state.inputFullName != null && this.state.inputRace != null?'#0f0':'#f00'}}>
                <Item style={{width:'90%', borderColor:'transparent'}}>
                    <Text style={{color:colors.bgColor, fontWeight:'bold'}}>Particulars*</Text>
                </Item>
                <TouchableOpacity onPress={()=>this.state.particular? this.setState({particular:false}):this.setState({particular:true, contact:false, misc:false, field:false})}>
                    {this.state.particular? <Icon style={{alignSelf:'flex-end'}} name='minus' type='EvilIcons' />:<Icon style={{alignSelf:'flex-end'}} name='plus' type='EvilIcons'/>}
                </TouchableOpacity>
            </Item>
            {this.state.particular?
            <Content padder>   
                <Label style={{marginTop:10}}>Nationality*</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Country"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputCountry}
                  onValueChange={(value)=>this.setState({inputCountry:value})}
                >
                    <Picker.Item value='' label='Select Country' />
                    {this.state.countryList.map((item, index)=>{
                        return <Picker.Item key={index} value={item.value} label={item.displayText} />
                    })}
                </Picker>
                <Item />
                <Label style={{marginTop:10}}>IC / Passport no.</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputIC:value})}} value={this.state.inputIC}/>
                </Item>
                <Label style={{marginTop:10}}>Title*</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Title"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputTitle}
                  onValueChange={(value)=>this.setState({inputTitle:value})}
                >
                    {this.state.titleList.map((item, index)=>{
                        return <Picker.Item key={index} value={item.name} label={item.name} />
                    })}
                </Picker> 
                <Item/>
                <Label style={{marginTop:10}}>Full Name*</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputFullName:value})}} value={this.state.inputFullName}/>
                </Item>  
                <Label style={{marginTop:10}}>Birth Date</Label>
                <Item style={{alignSelf:'flex-end', borderColor:'transparent'}}>
                    <DatePicker
                        defaultDate={new Date()}
                        minimumDate={new Date(1900, 1, 1)}
                        maximumDate={new Date()}
                        locale={"en"}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={"fade"}
                        androidMode={"default"}
                        placeHolderText="Select date"
                        textStyle={{ color: colors.btnTxtColor }}
                        placeHolderTextStyle={{ color: "#d3d3d3" }}
                        onDateChange={(newDate) => this.setState({inputBirthDate:newDate})}
                        disabled={false}
                    />  
                </Item>
                <Item/>
                <Label style={{marginTop:10}}>Gender</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Country"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputGender}
                  onValueChange={(value)=>this.setState({inputGender:value})}
                >
                    <Picker.Item value={'0'} label={'Undefined'} />
                    <Picker.Item value={'1'} label={'Male'} />
                    <Picker.Item value={'2'} label={'Female'} />
                </Picker>  
                <Item/>
                <Label style={{marginTop:10}}>Race*</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Race"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputRace}
                  onValueChange={(value)=>this.setState({inputRace:value})}
                >
                    <Picker.Item key={'default'} value={'null'} label={'Select Race'} />
                    {this.state.raceList.map((item, index)=>{
                        return <Picker.Item key={index} value={''+item.value} label={item.name} />
                    })}
                </Picker>
                <Item/>
                <Label style={{marginTop:10}}>Email Address</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputEmail:value})}} value={this.state.inputEmail} />
                </Item>
                <Label style={{marginTop:10}}>Marital Status</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Country"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputMarital}
                  onValueChange={(value)=>this.setState({inputMarital:value})}
                >
                    <Picker.Item value={'0'} label={'Undefined'} />
                    <Picker.Item value={'1'} label={'Single'} />
                    <Picker.Item value={'2'} label={'Married'} />
                </Picker>
                <Item/>
                <Label style={{marginTop:10}}   >Occupation</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputOccupation:value})}} value={this.state.inputOccupation}/>
                </Item>
                <Label style={{marginTop:10}}   >Remarks</Label>
                <Item regular>
                    <Textarea rowSpan={5} onChangeText={(value) => {this.setState({inputRemark:value})}} value={this.state.inputRemark}/>
                </Item>
            </Content>:<View />
            }
            <Item style={{padding:10, backgroundColor:colors.btnColor, marginTop:5, borderColor:this.state.inputCity !='' && this.state.inputStreet !='' && this.state.inputContact !=''?'#0f0':'#f00'}}>
                <Item style={{width:'90%', borderColor:'transparent'}}>
                    <Text style={{color:colors.bgColor, fontWeight:'bold'}}>Contacts*</Text>
                </Item>
                <TouchableOpacity onPress={()=>this.state.contact? this.setState({contact:false}):this.setState({contact:true, particular:false, misc:false, field:false})}>
                    {this.state.contact? <Icon style={{alignSelf:'flex-end'}} name='minus' type='EvilIcons' />:<Icon style={{alignSelf:'flex-end'}} name='plus' type='EvilIcons'/>}
                </TouchableOpacity>                
            </Item>
            {this.state.contact?
            <Content padder>   
                <Label style={{marginTop:10}}>Nationality*</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Country"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputAddCountry}
                  onValueChange={(value)=>this.setState({inputAddCountry:value})}
                >
                    <Picker.Item value='' label='Select Country' />
                    {this.state.countryList.map((item, index)=>{
                        return <Picker.Item key={index} value={item.value} label={item.displayText} />
                    })}
                </Picker>
                <Label style={{marginTop:10}}>Postcode</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputPostCode:value})}} value={this.state.inputPostCode}/>
                </Item>
                <Label style={{marginTop:10}}>State/Province</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputState:value})}} value={this.state.inputState}/>
                </Item>     
                <Label style={{marginTop:10}}>City/Town*</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputCity:value})}} value={this.state.inputCity}/>
                </Item>  
                <Label style={{marginTop:10}}>Street Address*</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputStreet:value})}} value={this.state.inputStreet}/>
                </Item> 
                <Label>Contact number*</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Country"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputCountryCode}
                  onValueChange={(value)=>this.setState({inputCountryCode:value})}
                >
                    {this.state.countryCodeList.map((item, index)=>{
                        return <Picker.Item key={index} value={''+item.callingCode} label={'(+'+item.callingCode+' '+item.name+')'} />
                    })}
                </Picker>   
                <Item regular>
                    <Input placeholder="e.g. 123456789" keyboardType={'numeric'} onChangeText={(value) => {this.setState({inputContact:value.replace(/[^0-9]/g, '')})}} value={this.state.inputContact}/>
                </Item>
            </Content>:<View/>
            }
            <Item style={{padding:10, backgroundColor:colors.btnColor, marginTop:5}}>
                <Item style={{width:'90%', borderColor:'transparent'}}>
                    <Text style={{color:colors.bgColor, fontWeight:'bold'}}>Misc.</Text>
                </Item>
                <TouchableOpacity onPress={()=>this.state.misc? this.setState({misc:false}):this.setState({misc:true, contact:false, particular:false, field:false})}>
                    {this.state.misc? <Icon style={{alignSelf:'flex-end'}} name='minus' type='EvilIcons' />:<Icon style={{alignSelf:'flex-end'}} name='plus' type='EvilIcons'/>}
                </TouchableOpacity>                
            </Item>
            {this.state.misc?
            <Content padder>   
                <Label style={{marginTop:10}}>Project Interests</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Phase"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputPhase}
                  onValueChange={(value)=>this.setState({inputPhase:value})}
                >
                    <Picker.Item value='' label='Select Phase' />
                    {this.state.phaseList.map((item, index)=>{
                        return <Picker.Item key={index} value={''+item.value} label={item.name} />
                    })}
                </Picker>
                <Label style={{marginTop:10}}>How do you know about our project? </Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Event"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputEvent}
                  onValueChange={(value)=>this.setState({inputEvent:value})}
                >
                    <Picker.Item value='' label='Select Event' />
                    {this.state.eventList.map((item, index)=>{
                        return <Picker.Item key={index} value={''+item.value} label={item.name} />
                    })}
                </Picker>
                <Label style={{marginTop:10}}>Service Agents</Label>
                <FlatList extraData={this.state.inputServiceAgent} bounces={false} data={this.state.inputServiceAgent}
                    renderItem={({item, index}) =>(
                        <Item style={{alignSelf:'flex-start', borderColor:'transparent'}}>
                            <Text style={{color:'#1E90FF', width:'90%'}}>{item}</Text>
                            <TouchableOpacity onPress={()=>this.setState({inputServiceAgent:this.state.inputServiceAgent.filter((x)=> x != item)})}>
                                <Icon name='cancel' type='MaterialIcons' style={{fontSize:20}}/>
                            </TouchableOpacity>                            
                        </Item>
                    )}
                    keyExtractor={(item, index) => 'agents' +index}
                />
                <Picker
                  mode="dropdown"
                  iosHeader="Select Agent"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.selectedAgent}
                  onValueChange={(value)=>this.setState((prev)=>({inputServiceAgent:prev.inputServiceAgent.filter((x)=>x == value).length >0?[...prev.inputServiceAgent]:[...prev.inputServiceAgent, value]}))}
                >
                    <Picker.Item value='' label='Add Agent' />
                    {this.state.serverList.map((item, index)=>{
                        return <Picker.Item key={index} value={item.name} label={item.name} />
                    })}
                </Picker>
                    
                <Label style={{marginTop:10}}>Attitude</Label>
                <Picker
                  mode="dropdown"
                  iosHeader="Select Attitude"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.inputAttitude}
                  onValueChange={(value)=>this.setState({inputAttitude:value})}
                >
                    <Picker.Item value='0' label='Cold' />
                    <Picker.Item value='1' label='Warm' />
                    <Picker.Item value='2' label='Hot' />
                </Picker>
                <Label style={{marginTop:10}}>How are you affiliated with us?</Label>
                <Item regular>
                    <Input onChangeText={(value) => {this.setState({inputRelationship:value})}} value={this.state.inputRelationship}/>
                </Item> 
            </Content>:<View/>
            }
            {this.state.fieldList.length >0?
            <Item style={{padding:10, backgroundColor:colors.btnColor, marginTop:5, borderColor:this.state.fieldList.filter((x)=> x.isMandatory == true && x.answer == null).length>0?'#f00':'#0f0'}}>
                <Item style={{width:'90%', borderColor:'transparent'}}>
                    <Text style={{color:colors.bgColor, fontWeight:'bold'}}>Fields</Text>
                </Item>
                <TouchableOpacity onPress={()=>this.state.field? this.setState({field:false}):this.setState({field:true, contact:false, misc:false, particular:false})}>
                    {this.state.field? <Icon style={{alignSelf:'flex-end'}} name='minus' type='EvilIcons' />:<Icon style={{alignSelf:'flex-end'}} name='plus' type='EvilIcons'/>}
                </TouchableOpacity>                
            </Item>:<View/>
            }
            {this.state.field?
            <Content padder>   
                <FlatList extraData={this.state} bounces={false} data={this.state.fieldList}
                    renderItem={({item, index}) => {
                    if(item.type == 0){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Input onChangeText={(value) => {let list = [...this.state.fieldList];list[index].answer = value; this.setState({fieldList:list})}} value={this.state.fieldList[index].answer} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
                        </Item>)
                    }
                    else if(item.type == 1 && item.isActive){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Form style={{width:'100%'}}>
                                <Textarea onChangeText={(value) => {let list = [...this.state.fieldList];list[index].answer = value; this.setState({fieldList:list})}} value={this.state.fieldList[index].answer} rowSpan={5} style={{borderColor:colors.formTxtColor}} bordered />
                            </Form>
                        </Item>)
                    }
                    else if(item.type == 2){
                        return (
                        <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <FlatList extraData={this.state.fieldList} bounces={false} data={item.choices}
                                renderItem={({item:item2}) => (
                                     <Item style={{borderColor:'transparent', marginTop:5}}>
                               
                                          <Text style={{width:'90%'}}>{item2}</Text>
                                      
                                           <CheckBox checked={this.state.fieldList[index].answer == item2?true:false} onPress={()=>{let list = [...this.state.fieldList];list[index].answer = item2; this.setState({fieldList:list})}}/>
                               
                                      </Item>      
                                )}
                                keyExtractor={(item, index) => 'radio' +index}
                            />  
                        </Item>)
                    }
                    else if(item.type == 3){
                        return (<View style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            
                                <Picker
                                  mode="dropdown"
                                  iosHeader={"Select "+item.label}
                                  iosIcon={<Icon name="arrow-down" />}
                                  style={{ width: undefined }}
                                  selectedValue={this.state.fieldList[index].answer == null?'':this.state.fieldList[index].answer}
                                  onValueChange={
                                        (value)=>{
                                            let list = [...this.state.fieldList];
                                            list[index].answer = value; 
                                            this.setState({fieldList:list})
                                        }
                                    }
                                >
                                    <Picker.Item value='' label={'Select '+item.label} />
                                    {item.choices.map((item, index)=>{
                                        return <Picker.Item key={index} value={item} label={item} />
                                    })}
                                </Picker>
                            
                        </View>)
                    }
                    else if(item.type == 6){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <FlatList extraData={this.state.fieldList} bounces={false} data={item.choices}
                                renderItem={({item:item4}) => (
                                     <Item style={{borderColor:'transparent', marginTop:5}}>
                                  
                                          <Text style={{width:'90%'}}>{item4}</Text>
                                      
                                           <CheckBox checked={this.state.fieldList[index].answer == null?false:this.state.fieldList[index].answer.indexOf(item4) >= 0?true:false} 
                                               onPress={()=>{
                                                    let list = [...this.state.fieldList];
                                                    if(list[index].value != null){
                                                        if(list[index].answer.indexOf(item4) < 0){

                                                            list[index].answer = item4+','+list[index].answer
                                                        }else{
                                                            list[index].answer.lastIndexOf(item4+',') < 0?
                                                            list[index].answer = list[index].answer.replace(item4, ''):
                                                            list[index].answer = list[index].answer.replace(item4+',', '')

                                                        }
                                                    }
                                                    else{
                                                        list[index].answer = item4;
                                                    }
                                                    this.setState({fieldList:list})
                                                }
                                               }/>
                                      
                                      </Item>      
                                )}
                                keyExtractor={(item, index) => 'radio' +index}
                            />
                        </Item>)
                    }
                    else if(item.type == 7){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            {this.state.imageData != ''?
                                <Image source={this.state.imageUri} style={{width:150, height:150}} />:<View/>}
                            <Button onPress={()=>this.uploadFormImage(item.mappingId, item.isMandatory)} style={{alignSelf:'center', marginTop:10}}><Text>Upload {item.label} Photo</Text></Button>
                
                        </Item>)
                    }
                    else if(item.type == 100){
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Item style={{alignSelf:'flex-end', borderColor:'transparent'}}>
                            <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(1918, 1, 1)}
                                    maximumDate={new Date(2108, 12, 31)}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText="Select Date"
                                    textStyle={{ color: colors.btnTxtColor }}
                                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                                    onDateChange={(newDate)=>{
                                            let list = this.state.fieldList; 
                                            list[index].answer = newDate; 
                                            this.setState({fieldList:list})
                                        }
                                    }
                                    disabled={false}
                            />
                            </Item>
                        </Item>)
                    }
                    else{
                        return (<Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                            <Label style={{alignSelf:'flex-start', color:colors.formTxtColor}}>{item.label}{item.isMandatory?'*':''}</Label>
                            <Input onChangeText={(value) => {let list = this.state.sectionList;list[index].answer = value; this.setState({sectionList:list})}} value={this.state.sectionList[index].answer} style={{borderColor:colors.formTxtColor, borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>                               
                        </Item>)
                    }}
                        }
                keyExtractor={(item, index) => 'section' +index}
            />     
            </Content>:<View/>
            }
            <Button style={[styles.defaultBtn,{marginTop:10}]} onPress={()=>this.registerValidation()}><Text style={{color:colors.btnTxtColor}}>{this.state.searchResult.length>0?'Update':'Submit'}</Text></Button>
            {this.state.searchResult.length>0 && this.props.permission.filter((x)=>x == 'Crm.FollowUpAssignCustomer').length>0?
            <Button style={[styles.defaultBtn,{marginTop:10}]} onPress={()=>this.postRequestGetFollowUpCustomersForEdit()}><Text style={{color:colors.btnTxtColor}}>Assign Agent</Text></Button>:<View />}
            <Button transparent style={{marginTop:10, alignSelf:'center'}} onPress={()=>this.setState({createNew:false})}><Text style={{color:colors.btnTxtColor}}>Back</Text></Button>
        </Container>
    )
    
    search=(
         <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>
              <Item rounded style={{borderColor:colors.btnColor, marginLeft:'10%', marginRight:'10%'}}>
                <Icon name="ios-search" style={{color:colors.btnColor}}/>
                <Input placeholder="NRIC, phone, email..." placeholderTextColor={colors.btnColor} style={{color:'#ffffff'}}  value={this.state.searchOutput} onChangeText={(value) => {this.setState({searchOutput:value})}}/>
              </Item>
              <Button style={[styles.defaultBtn,{marginTop:10}]}  onPress={()=>this.state.searchOutput == ''?alert('Please provide at least NRIC to proceed.'):this.postRequestGetCustomers(this.state.searchOutput)}>
                <Text style={{color:colors.btnTxtColor}}>Search</Text>
              </Button>
              <Button style={{alignSelf:'center', marginTop:10, borderColor:colors.btnColor}} transparent onPress={this.postRequestGetCustomers.bind(this, '')}>
                <Text style={{color:colors.btnColor}}>Show Customer List</Text>
              </Button>
          </View>
    )
    
    searchList=(
        <Container style={{backgroundColor:colors.bgColor, padding:10}}>
            <FlatList extraData={this.state.searchResult} bounces={false} data={this.state.searchResult}
                renderItem={({item}) =>(
                  <TouchableOpacity onPress={()=>this.postRequestGetCustomerForEdit({id:item.id})} >
                      <Card>
                        <CardItem style={{backgroundColor:colors.lightGray}}> 
                            <Text style={{width:'30%'}}>Name</Text>  
                            <Text style={{width:'70%'}}>: {item.title} {item.fullName}</Text>  
                        </CardItem>    
                        <CardItem>
                            <Text style={{width:'30%'}}>IC/Passport</Text>  
                            <Text style={{width:'70%'}}>: {item.nationalId}({item.nationalityCode})</Text>  
                        </CardItem>    
                        <CardItem style={{backgroundColor:colors.lightGray}}>
                            <Text style={{width:'30%'}}>Contact</Text>  
                            <Text style={{width:'70%'}}>: {item.phones[0].number}</Text>  
                        </CardItem>    
                        <CardItem>
                            <Item style={{width:'30%', flexDirection:'column', justifyContent:'flex-start', borderColor:'transparent'}}>
                                <Text style={{alignSelf:'flex-start'}}>Service Agents</Text>  
                            </Item>
                            <Text style={{width:'70%', alignSelf:'flex-start'}}>: {item.servers.map((x)=>x.name).toString().replace('[]','')}</Text>  
                        </CardItem> 
                      </Card> 
                  </TouchableOpacity>
                )}
                keyExtractor={(item, index) => 'discount' +index}
              />     
              <Button style={{alignSelf:'center'}} transparent onPress={()=>this.setState({searchResult:[]})}>
                <Text style={{color:colors.btnColor}}>Back</Text>
              </Button>
              
        </Container>
    )
      
    return (
      <Container style={{backgroundColor:colors.bgColor}}>
        <Header style={styles.headerOverlap}>
          <Left>
            {this.state.searchResult.length >0 || this.state.createNew?
            <Button transparent onPress={()=>this.state.createNew?this.setState({createNew:false}):this.setState({searchResult:[]})}>
              <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
            </Button>:
            <Button transparent onPress={()=>this.props.navigation.navigate('prosales',{hideTabBar:true})}>
              <Icon name='back' style={{color:'#000000'}} type={'Entypo'} />
            </Button>}
          </Left>
          <Body>
            <Title style={{color:colors.formTxtColor}}>{this.state.searchResult.length == 0?'Register':'Customer List'}</Title>
          </Body>
          <Right>
          </Right>
        </Header>
         {this.state.assignAgent?
            <View style={{flexDirection:'column', justifyContent:'center', zIndex:2, position:'absolute', height:Dimensions.get('window').height, width:Dimensions.get('window').width, padding:5}}>
                <Card style={{width:'100%', alignSelf:'center', borderColor:'#00f'}}>
                    <CardItem style={{flexDirection:'column'}}>
                        <Label style={{alignSelf:'flex-start'}}>Assign to agent:</Label>
                        <Item>
                            <Picker
                              mode="dropdown"
                              iosHeader="Select Agent"
                              iosIcon={<Icon name="arrow-down"/>}
                              style={{width:undefined}}
                              selectedValue={this.state.followAssignedAgent}
                              onValueChange={(value)=>this.setState({followAssignedAgent:value})}
                            >
                                <Picker.Item value='' label='Select Agent' />
                                {this.state.followUpAgentList.map((item, index)=>{
                                    return <Picker.Item key={index} value={item.value} label={item.name} />
                                })}
                            </Picker>
                        </Item>
                    </CardItem>
                    <CardItem style={{flexDirection:'column'}}>
                        <Label style={{alignSelf:'flex-start'}}>Description</Label>
                        <Item regular >
                            <Input style={{width:'100%'}} onChangeText={(value) => {this.setState({followUpRemark:value})}} value={this.state.followUpRemark}/>
                        </Item>
                    </CardItem>
                    <CardItem style={{justifyContent:'center'}}>
                        <Button onPress={()=>this.setState({assignAgent:false, followUpRemark:'', followAssignedAgent:''})} style={{width:100, backgroundColor:'#f00', marginRight:5, justifyContent:'center'}}><Text>Cancel</Text></Button>
                        <Button onPress={()=>this.validateFollowUp()} style={{width:100, backgroundColor:'#0f0', marginLeft:5, justifyContent:'center'}}><Text style={{color:'#000000'}}>Add</Text></Button>
                    </CardItem>
                </Card>
            </View>:<View/>
         }                
        {this.state.createNew?
            <View style={{flex:1}}>{register}</View>:<View style={{flex:1}}>
            {this.state.searchResult.length>0?
                <View style={{flex:1}}>{searchList}</View>:
                <View style={{flex:1}}>{search}</View>
            }
            </View>
        }
        
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token,
        permission:state.permission
    }
}

export default connect(mapStateToProps)(registerPage);