import React, { Component } from 'react';
import {View, Image, TouchableOpacity, Dimensions} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content, Text} from 'native-base';
import {colors} from './globalStyle.js'
import {major, minor, patch} from './config.js'
export default class HeaderIconExample extends Component {
    
    componentDidMount(){

    }
         
    
  render() {  
      
    return (
      <Container>
        <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('bottomTab')}>
                <Image     
                    style={{ 
                            width: '100%',
                            height: '100%',
                            resizeMode: 'cover',
                            opacity:1
                            }}
                    source={require('./image/home/bg.png')}
                />
            
                <Text style={{alignSelf:'flex-end', fontSize:15, color:'#ffffff', marginTop:Dimensions.get('window').height*0.97, paddingRight:10, position:'absolute'}}>v{major}.{minor}.{patch}</Text>
            </TouchableOpacity>
        </View>
      </Container>
    );
  }
}