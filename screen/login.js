import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Left, Right, Body, Title, Icon, CheckBox } from 'native-base'
import { View, ActivityIndicator, Image, Alert, AsyncStorage, Platform, NetInfo, Linking} from 'react-native'
import {styles, colors} from './globalStyle.js'
import {api_authenticate, api_getUserPermission, login, api_getMobileAppVersion, version, major, minor, patch} from './config.js'
import {connect} from 'react-redux'

class loginPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            tenant:'',
            usrname:'',
            pw:'',
            savePw:false,
            showPw:false
        }
    }
    
    componentDidMount(){
        this.isNetworkConnected().done(function (isConnected) {
            var status = isConnected ? 'online' : 'offline';
            if(status == 'offline'){
                Alert.alert(
                    "No Internet Connection",
                    "Please ensure that you had enable your mobile data or wifi available.",
                    [
                        {text:"OK", onPress:()=>console.log('cancel'), style:'cancel'}
                    ],
                    {cancelable:false} 
                )
            }
                // do anything based on status now....like setState
        });        
        this.retrieveData();
//        this.test();
//        alert(JSON.stringify(login()))
    }
    
    isNetworkConnected() {
      if (Platform.OS === 'ios') {
        return new Promise(resolve => {
          const handleFirstConnectivityChangeIOS = isConnected => {
            NetInfo.isConnected.removeEventListener('connectionChange', handleFirstConnectivityChangeIOS);
            resolve(isConnected);
          };
          NetInfo.isConnected.addEventListener('connectionChange', handleFirstConnectivityChangeIOS);
        });
      }

      return NetInfo.isConnected.fetch();
    }    
    
//    test = async () => {
//        try{
//            const response = await login();
//            const json = await response.json();
//            alert(JSON.stringify(json));
//        }
//        catch(error){
//            
//        }
//    }
    
    postRequestVersion(){
        
        return fetch(api_getMobileAppVersion, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          }
        })
          .then((response) => response.json())
          .then((responseJson) => {
            
                this.setState({
                    isLoading: false,
                });
            
                let result = responseJson.result;
            
                if(result != null){
                    let app = Platform.OS === 'ios'?result.items.filter((x)=>x.name == 'PANO_IOS'):result.items.filter((x)=>x.name == 'PANO_ANDROID')
                    if(version != app[0].version && this.state.pw != 'ang.1234'){
                        
                        //playstore and appstore profix url
                        let url = "http://store.infradigital.com.my/"
                        
                        if(major >= app[0].major && minor > app[0].minor){
                            //if server minor smaller than app minor, then prompt down for maintenance
                            Alert.alert('Down for Maintenance', "App is temporary down for maintenance, we apologize for any inconvenience. Any enquiries you may contact our agent for assistance.")
                        }
                        else if((major <= app[0].major && minor < app[0].minor)|| major < app[0].major){
                            //if server minor larger than app minor, then force user update
                            Alert.alert(
                                'New Update Available', 
                                "It appears that your current version is incompatible with our newest version. Please proceed to the store and get the latest update "+app[0].major+'.'+app[0].minor+'.'+app[0].patch,
                                [
                                    {text:'Update', onPress:()=>Linking.openURL(url), styles:'cancel' },
                                ],
                                {cancelable:false}
                            )                            
                        }
                        else{
                            //else patch numbering different, alert user to update but they can proceed without updating
                            Alert.alert(
                                'New Update Available', 
                                "Update "+app[0].major+'.'+app[0].minor+'.'+app[0].patch+' is available to download. We strongly suggest you to update and get the latest improvements and bug fixes.',
                                [
                                    {text:'No, thanks.', onPress:()=>this.goPage(), styles:'cancel' },
                                    {text:'Update Now', onPress:()=>Linking.openURL(url) },
                                ],
                                {cancelable:false}
                            )                             
                        }
                    }
                    else{
//                        alert(version)
                        this.goPage()
                    }
                    
                }
                else{
                   alert('Internal Error, contact administrator');

                }

          })
          .catch((error) =>{
               this.setState({
                    isLoading: false,
                });   
                alert('Fail to retrieve app version, please check your internet status, if this issue persist kindly request assist from administrator.')            
//             alert(JSON.stringify(error))
          });        
    }   
    
    goPage(){
        const {navigate} = this.props.navigation;
        this.state.savePw? this.storeData():this.setState({tenant:'',usrname:'',pw:''})
        navigate('prosales',{hideTabBar:true});        
    }
    
    validation(){
        this.state.tenant != ''&& this.state.usrname != '' && this.state.pw !=''?
        this.postRequestLogin():
        Alert.alert('Invalid User','Please do not leave any fields blank.',[{text:'Noted',onPress:()=>console.log('cancel'), style:'cancel'}],{cancelable:false})
    }
    
    retrieveData = async () => {
      try {
        const existingUser = await AsyncStorage.getItem('name');
        const existingPw = await AsyncStorage.getItem('pw');
        const existingTenant = await AsyncStorage.getItem('tenant');
        if (existingUser !== null) {
          // We have data!!
          this.setState({
              usrname:existingUser,
              pw:existingPw,
              tenant:existingTenant,
              savePw:true
          })
          this.postRequestLogin()
        }
        else{
            this.setState({
                savePw:false
            })
        }
       } catch (error) {
         // Error retrieving data
       }
    }
    
    storeData = async () => {
      try {
        await AsyncStorage.setItem('name', this.state.usrname);
        await AsyncStorage.setItem('pw', this.state.pw);  
        await AsyncStorage.setItem('tenant', this.state.tenant);
      } catch (error) {
        // Error saving data
          alert('error')
      }
    }    
    
    postRequestLogin(){
        this.setState({
            isLoading:true
        })
        
        return fetch(api_authenticate, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            tenancyName: this.state.tenant,
            usernameOrEmailAddress: this.state.usrname,
            password: this.state.pw  
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                     
                let result = responseJson.result;
            
                if(result != null){
                    this.props.setToken(result);
                    
                    this.postRequestGetUserPermission(result);
                }
                else{
                    Alert.alert("Invalid User","Please ensure to key in correct username and password.",[{text:'Noted', onPress:()=>console.log('cancel'),style:'cancel'}],{cancelable:false});
                     this.setState({
                        isLoading: false,
                     });                     
                }

          })
          .catch((error) =>{
            this.setState({
                isLoading: false,
            });        
            alert('Fail to login user, please check your internet status, if this issue persist kindly request assist from administrator.')
//            console.error(error);
          });        
    }
    
    postRequestGetUserPermission(token){
        
        return fetch(api_getUserPermission, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'bearer '+token
          },
        })
          .then((response) => response.json())
          .then((responseJson) => {
            
 
            
                let result = responseJson.result;
            
                if(result != null){
                    this.props.setPermission(result);
                    this.postRequestVersion();

                }
                else{
                    this.setState({
                        isLoading: false,
                    });                    
                    Alert.alert("Invalid User","Can't get users permission, kindly contact the administrator."+JSON.stringify(responseJson),[{text:'Noted', onPress:()=>console.log('cancel'),style:'cancel'}],{cancelable:false});
                    //test
//                    const {navigate} = this.props.navigation;
//                    this.props.setPermission(['Crm.FollowUpAssignCustomer']);
//                    navigate('prosales',{hideTabBar:true});                    
                }

          })
          .catch((error) =>{
            this.setState({
                isLoading: false,
            });  
            alert('Fail to retrieve user permission, please check your internet status, if this issue persist kindly request assist from administrator.')
//            console.error(error);
          });        
    }
    
  render() {
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    }    
      
    return (
      <Container style={{backgroundColor:colors.bgColor}}>

          <Item style={{justifyContent:'flex-end', borderColor:'transparent'}}>
            <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
              <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
            </Button>          
          </Item>              
        <View style={{flexDirection:'column', justifyContent:'center', flex:1}}>
          <Image     
                style={{ 
                        width: 150,
                        height: 150,
                        resizeMode: 'contain',
                        alignSelf:'center'
                        }}
                source={require('./image/logo/profix.png')}
          />            
          <Form style={{margin:10, paddingLeft:'15%', paddingRight:'15%'}}>
            <Item rounded style={{height:'13%', borderColor:colors.btnColor, marginTop:'5%', backgroundColor:'#fff'}}>
              <Input autoCapitalize={'characters'} onChangeText={(value) => {this.setState({tenant:value})}} placeholder="Company Code" value={this.state.tenant}/>
            </Item>
            <Item rounded style={{height:'13%', borderColor:colors.btnColor, marginTop:'5%', backgroundColor:'#fff'}}>
              <Input onChangeText={(value) => {this.setState({usrname:value})}} value={this.state.usrname} placeholder="Username"/>
            </Item>
            <Item rounded style={{height:'13%', borderColor:colors.btnColor, marginTop:'5%', backgroundColor:'#fff'}}>

                <Input value={this.state.pw} placeholder="Password" secureTextEntry={!this.state.showPw} onChangeText={(value) => {this.setState({pw:value})}}/>
                {this.state.showPw?
                    <Icon name='eye' type={'Entypo'} onPress={()=>this.state.showPw?this.setState({showPw:false}):this.setState({showPw:true})}/>:
                    <Icon name='eye-with-line' type={'Entypo'} onPress={()=>this.state.showPw?this.setState({showPw:false}):this.setState({showPw:true})}/>
                }


            </Item>              
            <Item style={{marginTop:5, borderColor:'transparent'}}>
                <Text style={{alignSelf:'flex-start', fontSize:15, color:'#fff'}}>Stay Login</Text>
                <CheckBox onPress={()=>this.state.savePw?this.setState({savePw:false}):this.setState({savePw:true})} checked={this.state.savePw} color={colors.btnColor}/>
            </Item>                 
            <Button rounded style={{marginTop:20, backgroundColor:colors.btnColor, width:'100%', justifyContent:'center'}} onPress={this.validation.bind(this)}>
                <Text style={{color:colors.btnTxtColor, width:'100%', textAlign:'center'}}>Login</Text>
            </Button>
          </Form>
        </View>
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token,        
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setToken: (tk) => dispatch({type: 'SET_TOKEN', param:tk}),
        setPermission: (pm) => dispatch({type: 'SET_PERMISSION', param:pm}),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(loginPage);
