import {createStackNavigator, createSwitchNavigator} from 'react-navigation'
import React, {Component} from 'react'
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'
import {Icon} from 'native-base'
import { Platform, TouchableOpacity } from 'react-native'
import { styles, colors } from './globalStyle.js'


import home from './home.js'
import intro from './projects/intro.js'
import usp from './projects/usp.js'
import gallery from './projects/gallery.js'
import plan from './projects/plan.js'
import booking from './salesBooking/booking.js'
import location from './projects/location.js'
import document from './projects/document.js'
import enquiry from './enquiry.js'
import login from './login.js'
import pricing from './salesBooking/pricing.js'
import purchaser from './salesBooking/purchaser.js'
import salesInfo from './salesBooking/salesInfo.js'
import salesList from './salesListing/salesList.js'
import salesDetail from './salesListing/salesDetail.js'
import supportingDoc from './salesListing/supportingDoc.js'
import confirmation from './salesBooking/confirmation.js'
import summaryInfo from './salesBooking/summaryInfo.js'
import salesPayment from './salesBooking/salesPayment.js'
import projectNavigation from './projects/projectNavigation.js'
import prosales from './prosales.js'
import appointmentList from './crm/appointment/appointmentList.js'
import cusList from './crm/customerList/cusList.js'
import register from './crm/register/register.js'
import followUpDetail from './crm/customerList/followUpDetail.js'
import followUpAction from './crm/customerList/followUpAction.js'

import dashboard from './salesDashboard/dashboard.js'
import teamDashboard from './salesDashboard/teamDashboard.js'
import agentDashboard from './salesDashboard/agentDashboard.js'

const projectStack = createStackNavigator({
    projectNav:{
        screen:projectNavigation,
        navigationOptions: ({navigation}) => ({
            header:null
        })        
    },
    intro:{
        screen:intro,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    usp:{
        screen:usp,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    gallery:{
        screen:gallery,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    plan:{
        screen:plan,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
    location:{
        screen:location,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
    document:{
        screen:document,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
})

const crmFirstStack = createStackNavigator({
    register:{
        screen:register,
        navigationOptions: ({navigation}) => ({
            header:null
        })        
    },
    intro:{
        screen:intro,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    usp:{
        screen:usp,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    gallery:{
        screen:gallery,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    plan:{
        screen:plan,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
    location:{
        screen:location,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
    document:{
        screen:document,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
})

const crmSecondStack = createStackNavigator({
    cusLis:{
        screen:cusList,
        navigationOptions: ({navigation}) => ({
            header:null
        })        
    },
    followUpDetail:{
        screen:followUpDetail,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    followUpAction:{
        screen:followUpAction,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    gallery:{
        screen:gallery,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    plan:{
        screen:plan,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
    location:{
        screen:location,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
    document:{
        screen:document,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
})

const crmThirdStack = createStackNavigator({
    appointmentList:{
        screen:appointmentList,
        navigationOptions: ({navigation}) => ({
            header:null
        })        
    },
    intro:{
        screen:intro,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    usp:{
        screen:usp,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    gallery:{
        screen:gallery,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    plan:{
        screen:plan,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
    location:{
        screen:location,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
    document:{
        screen:document,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },    
})

const crmBottomNav = createMaterialBottomTabNavigator({
    Register:{
        screen:crmFirstStack,
        navigationOptions: {
            tabBarLabel:'Register',
            labeled:true,
            tabBarColor:colors.yellowColor,
            tabBarIcon: ({ tintColor, focused }) => (
                  <Icon name={'add-user'} type={'Entypo'} style={{fontSize:20, color: tintColor }} />
                )
        }
    },
    CusList:{
        screen:crmSecondStack,
        navigationOptions: ({ navigation }) => {
           
            const { routeName, routes } = navigation.state;
            let params = routes && routes[1] && routes[1].params;
            return {                                            
                tabBarVisible: params && params.hideTabBar != null? !params.hideTabBar : true,    
                tabBarLabel:'Follow Up List',
                labeled:true,
                tabBarColor:colors.yellowColor,
                tabBarIcon: ({ tintColor, focused }) => (
                      <Icon name={ 'list' } type={'FontAwesome5'} style={{fontSize:20, color: tintColor }} />
                    ),
            };
        }
    },
//    Appointment:{
//        screen:crmThirdStack,
//        navigationOptions: {
//            tabBarLabel:'Appointment',
//            tabBarColor: colors.yellowColor,
//            tabBarIcon: ({ tintColor, focused }) => (
//                  <Icon name={'calendar'} type={'AntDesign'} style={{fontSize:20, color: tintColor }} />
//                )
//        }
//    },
},{
    initialRouteName: 'Register',
    activeTintColor: colors.bgColor,
    inactiveTintColor: '#6495ED',
    shifting:false,
    labeled:true,
    barStyle:{backgroundColor:colors.btnColor},
    navigationOptions: ({ navigation }) => {
        const { routeName, routes } = navigation.state;
        let params = routes && routes[1] && routes[1].params;
        return {                                            
            tabBarVisible: params && params.hideTabBar != null? !params.hideTabBar : true,    
        };
    },
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
//      showLabel: false,
    },
});

const salesDashboardStack = createStackNavigator({
    dashboard:{
        screen:dashboard,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    teamDashboard:{
        screen:teamDashboard,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    agentDashboard:{
        screen:agentDashboard,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    
})

const salesListingStack = createStackNavigator({
    salesList:{
        screen:salesList,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    salesDetail:{
        screen:salesDetail,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    supportingDoc:{
        screen:supportingDoc,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    salesPayment:{
        screen:salesPayment,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    }    
    
})

const bookingStack = createStackNavigator({
    login:{
        screen:login,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    prosales:{
        screen:prosales,
        navigationOptions: ({navigation}) => ({
            header:null
            
        })
    },
    crm:{
        screen:crmBottomNav,
        navigationOptions: ({navigation}) => ({
            header:null
            
        })
    },
    salesDashboard:{
        screen:salesDashboardStack,
        navigationOptions: ({navigation}) => ({
            header:null
            
        })
    },
    salesListing:{
        screen:salesListingStack,
        navigationOptions: ({navigation}) => ({
            header:null
            
        })
    },
    booking:{
        screen:booking,
        navigationOptions: ({navigation}) => ({
            header:null
            
        })
    },
    confirmation:{
        screen:confirmation,
        navigationOptions: ({navigation}) => ({
            header:null
            
        })
    },
    pricing:{
        screen:pricing,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    purchaser:{
        screen:purchaser,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    salesInfo:{
        screen:salesInfo,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    },
    summaryInfo:{
        screen:summaryInfo,
        navigationOptions: ({navigation}) => ({
            header:null
        })
    }
},{
     initialRouteName: 'login',
})


const bottomNav = createMaterialBottomTabNavigator({
    Project:{
        screen:projectStack,
        navigationOptions: {
            tabBarLabel:'Project',
            labeled:true,
            tabBarColor:colors.yellowColor,
            tabBarIcon: ({ tintColor, focused }) => (
                  <Icon size={15} name={'project-diagram'} type={'FontAwesome5'} style={{ fontSize:20, color: tintColor }} />
                )
        }
    },
    LevelPlan:{
        screen:bookingStack,
        navigationOptions: ({ navigation }) => {
           
            const { routeName, routes } = navigation.state;
            let params = routes && routes[1] && routes[1].params;
            return {                                            
                tabBarVisible: params && params.hideTabBar != null? !params.hideTabBar : true,    
                tabBarLabel:'Booking',
                labeled:true,
                tabBarColor:colors.yellowColor,
                tabBarIcon: ({ tintColor, focused }) => (
                      <Icon size={15} name={ 'building' } type={'FontAwesome'} style={{ fontSize:20, color: tintColor }} />
                    ),
            };
        }
    },
    Enquiry:{
        screen:enquiry,
        navigationOptions: {
            tabBarLabel:'Enquiry',
            labeled:true,
            tabBarColor: colors.yellowColor,
            tabBarIcon: ({ tintColor, focused }) => (
                  <Icon size={15} name={'question'} type={'EvilIcons'} style={{ fontSize:20, color: tintColor }} />
                )
        }
    },
},{
    initialRouteName: 'Project',
    activeTintColor: colors.bgColor,
    inactiveTintColor: '#6495ED',
    shifting:false,
    labeled:true,
    barStyle:{backgroundColor:colors.btnColor},
    navigationOptions: ({ navigation }) => {
        const { routeName, routes } = navigation.state;
        let params = routes && routes[1] && routes[1].params;
        return {                                            
            tabBarVisible: params && params.hideTabBar != null? !params.hideTabBar : true,    
        };
    },
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
//      showLabel: false,
    },
});

const switchNav = createSwitchNavigator({
    homePage:{
        screen:home,
        navigationOptions: ({navigation}) => ({
            header:null   
        })
    },
    bottomTab:bottomNav
},{
    initialRouteName:'homePage'
})



export default switchNav;
//export default createAppContainer(switchNav);