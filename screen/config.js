//home page
export const api_getMobileAppVersion = "https://salesbooking.infradigital.com.my/api/services/app/mobileAppsVersioning/GetMobileAppsVersioning";

//login page
export const api_authenticate = "https://salesbooking.infradigital.com.my/api/Account";
export const api_getUserPermission = "https://salesbooking.infradigital.com.my/api/services/app/defectAppointment/AppGetCurrentUserPermission";

//prosale page
export const api_getAllAnnouncement = "https://salesbooking.infradigital.com.my/api/services/app/announcement/GetAnnouncements";
export const api_markReadAnnoucement = "https://salesbooking.infradigital.com.my/api/services/app/announcement/MarkAnnouncementRead";

//booking page
export const api_getUnits = "https://salesbooking.infradigital.com.my/api/services/app/unit/GetUnits";
//export const api_getStatusStyle = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitStatusStyles";
export const api_getFloorPlan = "https://salesbooking.infradigital.com.my/api/services/app/floorPlan/GetFloorPlans";
//export const api_getUnitSalesLayout = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSalesLayout";
export const api_getUserProfile = "https://salesbooking.infradigital.com.my/api/services/app/user/AppGetUserOwnInfo";
export const api_getAllSetting = "https://salesbooking.infradigital.com.my/api/services/app/tenantSettings/GetAllSettings";

export const api_getUnitStatusStyle = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitStatusStyles";
export const api_getBlocks = "https://salesbooking.infradigital.com.my/api/services/app/commonLookup/GetBlocks"
export const api_getUnitTypes = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitTypes"
export const api_getUnitView = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitViews"
export const api_getUnitSalesLayout = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSalesLayout"

//confirmation page
export const api_getUnitTypeImage = "https://salesbooking.infradigital.com.my/api/services/app/unitTypePlan/GetUnitTypePlans"
export const getUnitTypePlan = "https://salesbooking.infradigital.com.my/Project/AppGetUnitTypePlanImageById?id="

//enquiry page
export const api_getEvents = "https://salesbooking.infradigital.com.my/api/services/app/public/GetEvents";
export const api_getCountriesNumber = "https://salesbooking.infradigital.com.my/api/services/app/public/GetCountries";
export const api_registerCustomer = "https://salesbooking.infradigital.com.my/api/services/app/public/RegisterCustomer";

//pricing page
export const api_getUnitSale = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSale";
export const api_reserveUnit = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/ReserveUnits";
export const api_findRebate = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/FindRebates";
export const api_deleteRebate = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/DeleteUnitSaleRebate"
export const api_createUnitSaleRebate = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/CreateUnitSaleRebate";
export const api_createOrUpdateDiscount = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/CreateOrUpdateUnitSaleDiscount";
export const api_getUnitDiscountForEdit = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSaleDiscountForEdit";
export const api_deleteDiscount = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/DeleteUnitSaleDiscount";
export const api_getGrrRebate = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/FindGRRRebates"
export const api_createGrrRebate = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/CreateUnitSaleRebate"

//purchaser page
export const api_searchIC = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/FindCustomer";
export const api_findCustomer = "https://salesbooking.infradigital.com.my/api/services/app/customer/FindCustomers";
export const api_getCustomerTitle = "https://salesbooking.infradigital.com.my/api/services/app/commonLookup/GetCustomerTitles";
export const api_getBusinessTitle = "https://salesbooking.infradigital.com.my/api/services/app/commonLookup/GetBusinessTitles";
export const api_getUnitSalePartyForEdit = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSalePartyForEdit";
export const api_getCorporateTypeCode = "https://salesbooking.infradigital.com.my/api/services/app/commonLookup/GetCorporateTypeCodes";
export const api_getCustomer = "https://salesbooking.infradigital.com.my/api/services/app/customer/GetCustomerForEdit";  
export const api_claimCustomer = "https://salesbooking.infradigital.com.my/api/services/app/customer/ClaimCustomer";
export const api_createOrUpdateUnitSaleParty = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/CreateOrUpdateUnitSaleParty";
export const api_createOrUpdateAddress = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/CreateOrUpdateCorrespondenceAddress";
export const api_getCorrespondAddress = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetCorrespondenceAddressForEdit";
export const api_getUnitSalesDetailForEdit = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSalesForEdit";
export const api_removeSaleParty = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/DeleteUnitSaleParty"

//salesInfo page
export const api_updateUnitSale = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/UpdateUnitSale";
export const api_getUnitSaleForEdit = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSaleForEdit";

//salesPayment page
export const api_getUnitSalePayment = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSalePaymentForEdit";
export const api_getUnitSalesSummaries = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSaleSummaries";
export const api_createSalesPayment = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/CreateOrUpdateUnitSalePayment";
export const api_deleteSalesPayment = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/DeleteUnitSalePayment";
export const getPaymentImage = "https://salesbooking.infradigital.com.my/Project/AppGetPaymentPictureById?id=";
export const api_sendSalesSubmmisionEmail = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/SendSaleSubmissionEmails";

//pending.....
export const api_getTermCondition = "https://salesbooking.infradigital.com.my/api/services/app/commonLookup/GetSalesTermsAndConditions";
export const api_getPdpa = "https://salesbooking.infradigital.com.my/api/services/app/commonLookup/GetPdpa";


//summaryInfo page
export const api_updateSaleFormSignature = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/UpdateSaleFormSignature";
export const api_updateSaleFormImage = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/UpdateSaleFormImage";
export const api_updatePartySignature = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/UpdatePartySignature";
export const api_updateIC = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/UpdateNationalIdPicture";
export const api_uploadImage = "https://salesbooking.infradigital.com.my/Temp/Upload";
export const api_submitReservation = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/SubmitReservation";

//register page
export const api_getCustomerForEdit = "https://salesbooking.infradigital.com.my/api/services/app/customer/GetCustomerForEdit";
export const api_updateFieldImage = "https://salesbooking.infradigital.com.my/api/services/app/customer/UpdateFieldImage";
export const api_createOrUpdateCustomer = "https://salesbooking.infradigital.com.my/api/services/app/customer/CreateOrUpdateCustomer";
export const api_getCustomers = "https://salesbooking.infradigital.com.my/api/services/app/customer/GetCustomers";
export const api_createFollowUpCustomer = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/CreateOrUpdateFollowUpCustomer";
export const api_getFollowUpCustomerForEdit = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/GetFollowUpCustomerForEdit";

//sales listing page
export const api_getSalesListing = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSales";

//sales detail page
export const api_getSaleDetail = "https://salesbooking.infradigital.com.my/api/services/app/unitSale/GetUnitSaleDetail";

//cusList page
export const api_getFollowUpCustomer = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/GetFollowUpCustomers";

//cusDetail page
export const api_updateStatus = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/BatchUpdateStatus";
export const api_resetStatus = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/UpdateFollowUpProgress";

//followUpAction page
export const api_getFollowUpNote = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/GetFollowUpNotes";
export const api_getFollowUpAppointment = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/GetFollowUpTasks";
export const api_getFollowUpProposedUnit = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/GetFollowUpQuotations";
export const api_getFollowUpUnit = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/GetFollowUpUnitsById";
export const api_deleteFollowUpAction = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/DeleteFollowUpActionItem";
export const api_createFollowUpAction = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/CreateOrUpdateFollowUpAction";
export const api_getUnitByStatus = "https://salesbooking.infradigital.com.my/api/services/app/customerFollowUp/GetUnitsByStatus";

//dashboard
export const api_getUnitStatusDetail = "https://salesbooking.infradigital.com.my/api/services/app/dashboard/GetUnitStatusBreakdown";
export const api_getPhaseStatusDetail = "https://salesbooking.infradigital.com.my/api/services/app/projectReport/GeneratePhaseStatusBreakdownReport";
export const api_agentSalesRanking = "https://salesbooking.infradigital.com.my/api/services/app/dashboard/GetSalesRanking";
export const api_getUnitTypeDetail = "https://salesbooking.infradigital.com.my/api/services/app/dashboard/GetUnitTypeChart";

//dashboard
//teamDashboard
export const api_getTeamSalesRanking = "https://salesbooking.infradigital.com.my/api/services/app/saleReport/GenerateTeamSalesReport";

//testing
export function login(){

    return fetch("https://salesbooking.infradigital.com.my/api/Account", {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        tenancyName: 'ocr',
        usernameOrEmailAddress: 'admin',
        password: 'prosales.1234' 
      }),
    })

}

//config for ocr
//staging
//export const phaseId = 71;
//export const blockAId = 69;
//export const blockBId = 70;

//live
export const phaseId = 117;
export const blockAId = 114;
export const blockBId = 115;
export const tenantName = 'OCR';
export const version = '1:1:5';
export const major = 1;
export const minor = 1;
export const patch = 5;