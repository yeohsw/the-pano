import React, { Component } from 'react';
import { Container, Content, Header, Left, Body, Right, Button, Icon, Title, Input, Item, Label, Textarea, Form, Text, Picker } from 'native-base';
import {Platform, Alert, View, ActivityIndicator } from 'react-native'
import {styles, colors} from './globalStyle.js'
import { api_getEvents, api_getCountriesNumber, api_registerCustomer, phaseId, blockId, tenantName } from './config.js'
import {connect} from 'react-redux'

class enquiryPage extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            eventList:[],
            countriesList:[],
            selectedEvent:'',
            selectedCountryCode:'MY',
            selectedTitle:'',
            name:'',
            ic:'',
            email:'',
            contact:'',
            agentEmail:'',
            remark:'',
            isLoading:false
        }
    }
    
    componentDidMount(){
        this.postRequestEvent()
    }
    
    submit(){
        let msg = ''
        let proceed = false
        this.state.selectedEvent == ''? msg='Please select an event.':proceed = false;
        this.state.contact == ''? msg='Please do not leave the contact number field blank.':proceed = false;
        this.state.email == ''? msg='Please do not leave the email address field blank.':proceed = false;
        this.state.angentEmail == ''? msg='Please do not leave the agent email address field blank.':proceed = false;
        this.state.ic == ''? msg='Please do not leave the NRIC field blank.':proceed = false;
        this.state.name == ''? msg='Please do not leave the name field blank.':proceed = false;
        this.state.selectedTitle == ''? msg='Please select your title.':proceed = false;
        msg == ''? proceed = true:proceed = false
        if(!proceed){
            Alert.alert(
                'Invalid Info',
                msg,
                [
                    {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                ],
                {cancelable:false}
            )
        }else{
            this.postRequestRegister();
        }
        
    }
    
    clearField(){
        this.setState({
            selectedEvent:'',
            selectedCountryCode:'MY',
            selectedTitle:'',
            name:'',
            ic:'',
            email:'',
            contact:'',
            agentEmail:'',
            remark:'',
        })
    }
    
    postRequestRegister(){
        this.setState({isLoading:true});
        return fetch(api_registerCustomer, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              title: this.state.selectedTitle,
              fullName: this.state.name,
              nationalId: this.state.ic,
              nationalityCode: this.state.selectedCountryCode,
              emailAddress: this.state.email,
              contactNumber: this.state.contact,
              acceptsNewsletters: true,
              customerPhases: [
                phaseId
              ],
              customerEvents: [
                this.state.selectedEvent
              ],
              serverEmail: this.state.agentEmail,
              appendServer: true,
              remarks: this.state.remark,
              fields: [],
              tenancyName: tenantName
            }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.success;
                if(result == true){
                    Alert.alert(
                        'Successfully Register',
                        'Thank you for registered one of us.',
                        [
                            {text:'Proceed', onPress:()=>this.clearField(), style:'cancel'}
                        ],
                        {cancelable:false}
                    )
                }
                else{
                    let err = responseJson.error;
                    let msg = ''
//                    alert(JSON.stringify(err))
                    if(err.validationErrors != null){
                        msg = err.validationErrors[0].message
                    }else{
                        msg = "This customer may already registered, you may try search them in crm."
                    }
                    Alert.alert(
                        'Fail To Register',
                        msg,
                        [
                            {text:'Noted', onPress:()=>console.log('cancel'), style:'cancel'}
                        ],
                        {cancelable:false}
                    )                    
                }
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }
    
    postRequestEvent(){
        this.setState({isLoading:true});
        return fetch(api_getEvents, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            tenancyName: 'OCR',
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
        
                let result = responseJson.result;
                if(result != null){
                    
                    this.setState({
                        eventList:result.items
                    })
                    this.postRequestCountries();
                }
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    } 
    
    postRequestCountries(){

        return fetch(api_getCountriesNumber, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          }
        })
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({isLoading:false})
                let result = responseJson.result;
                if(result != null){
                    
                    this.setState({
                        countriesList:result.items
                    })
                }
          })
          .catch((error) =>{
            this.setState({isLoading:false})
            console.error(error);
          });          
    }    
    
  render() {
      
    if(this.state.isLoading){
        return ( 
            <Container style={{backgroundColor:colors.bgColor}}>
              <View style={[styles.container, styles.horizontal, {marginTop:'50%', alignSelf:'center', backgroundColor:colors.bgColor}]}>
                <ActivityIndicator size="large" color="#ffffff" />
                <Text style={{textAlign:'center', flex:1, color:colors.txtColor}}>Please Wait...</Text>
              </View>
            </Container>
        )
    }
      
    return (
      <Container >
        <Header style={styles.headerOverlap}>
          <Left>
          </Left>
          <Body>
            <Title style={{color:colors.formTxtColor}}>Enquiry</Title>
          </Body>
          <Right>
            <Button transparent onPress={()=>this.props.navigation.navigate('homePage')}>
              <Icon name='home' style={{color:'#000000'}} type={'AntDesign'} />
            </Button>                     
          </Right>
        </Header>
        <Content padder style={{flex:1, flexDirection:'column'}}>
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start'}}>Title</Label>
                <Item style={{flexDirection:'row', justifyContent:'flex-end', width:'100%', borderColor:'transparent'}}> 
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down"/>}
                      style={{ width: undefined }}
                      headerTitleStyle={{fontSize:15}}
                      selectedValue={this.state.selectedTitle}
                      onValueChange={(value)=>{this.setState({selectedTitle:value})}}
                    >
                        <Picker.Item value='' label='Select Title' />
                        <Picker.Item value="Mr." label='Mr.' />
                        <Picker.Item value="Mrs." label='Mrs.' />
                        <Picker.Item value="Ms." label='Ms.' />
                        <Picker.Item value="Madam" label='Madam' />
                        <Picker.Item value="Dr" label='Dr' />
                        <Picker.Item value="Prof" label='Prof' />
                        <Picker.Item value="Sir" label='Sir' />
                        <Picker.Item value="En" label='En' />
                        <Picker.Item value="Cik" label='Cik' />
                        <Picker.Item value="Puan" label='Puan' />
                        <Picker.Item value="Puan Sri" label='Puan Sri' />
                        <Picker.Item value="Datin" label='Datin' />
                        <Picker.Item value="Dato" label='Dato' />
                        <Picker.Item value="Datuk" label='Datuk' />
                        <Picker.Item value="Dato Seri" label='Dato Seri' />
                        <Picker.Item value="Datuk Seri" label='Datuk Seri' />
                        <Picker.Item value="Tan Sri" label='Tan Sri' />
                        <Picker.Item value="Tan Sri Dato" label='Tan Sri Dato' />
                        <Picker.Item value="Tengku" label='TengKu' />
                        <Picker.Item value="Tun" label='Tun' />
                    </Picker>
                </Item>
            </Item>           
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start'}}>Name</Label>
                <Input value={this.state.name} onChangeText={(value) => {this.setState({name:value})}} style={{borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
            </Item>
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start'}}>IC/Passport</Label>
                <Input value={this.state.ic} keyboardType='numeric' onChangeText={(value) => {this.setState({ic:value.replace(/[^0-9]/g, '')})}} style={{borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
            </Item>
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start'}}>Email</Label>
                <Input value={this.state.email} onChangeText={(value) => {this.setState({email:value})}} style={{borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>
            </Item>
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start'}}>Contact</Label>
                <Item style={{flexDirection:'row', borderColor:'transparent'}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down"/>}
                      style={{ width: undefined }}
                      headerTitleStyle={{fontSize:15}}
                      selectedValue={this.state.selectedCountryCode}
                      onValueChange={(value)=>{this.setState({selectedCountryCode:value})}}
                    >
                        <Picker.Item value='' label='Select Country Code' />
                        {this.state.countriesList.map((item, index)=>{
                            return <Picker.Item key={index} value={item.value} label={'(+'+item.callingCode+' '+item.name+')'} />
                        })}
                    </Picker> 
                </Item>
                <Input keyboardType='numeric' value={this.state.contact} onChangeText={(value) => {this.setState({contact:value.replace(/[^0-9]/g, '')})}} placeholder={'e.g 123456789'} style={{borderWidth:1, alignSelf:'flex-start', width:'100%'}}/>                    
            </Item>
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start'}}>Agent Email</Label>
                <Input style={{borderWidth:1, alignSelf:'flex-start', width:'100%'}} value={this.state.agentEmail} onChangeText={(value) => {this.setState({agentEmail:value})}}/>
                <Label style={{alignSelf:'flex-start'}}>*Note: ProSales - Registered Email Only</Label>
            </Item>
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start'}}>Event</Label>
                <Item style={{borderColor:'transparent', justifyContent:'flex-end', width:'100%', flexDirection:'row'}}>
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down"/>}
                  style={{ width: undefined }}
                  headerTitleStyle={{fontSize:15}}
                  selectedValue={this.state.selectedEvent}
                  onValueChange={(value)=>{this.setState({selectedEvent:value,})}}
                >
                    <Picker.Item value='' label='Select Event' />
                    {this.state.eventList.map((item, index)=>{
                        return <Picker.Item key={index} value={item.value} label={item.name} />
                    })}
                </Picker> 
                </Item>
            </Item>
            <Item style={{borderColor:'transparent', flexDirection:'column', marginTop:10}}>
                <Label style={{alignSelf:'flex-start'}}>Remark</Label>
                <Form style={{width:'100%'}}>
                    <Textarea rowSpan={5} bordered style={{borderColor:'#000000'}}/>
                </Form>
            </Item>
            <Button block style={[styles.defaultBtn,{marginBottom:20, marginTop:20}]} onPress={this.submit.bind(this)}><Text style={{color:colors.btnTxtColor}}>Submit</Text></Button>
        </Content>
        
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        token: state.token,        
    }
}

export default connect(mapStateToProps)(enquiryPage);