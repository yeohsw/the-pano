import {StyleSheet, Platform, Dimensions} from 'react-native';

//var {vw, vh, vmin, vmax} = require('react-native-viewport-units');

export const styles = StyleSheet.create({
    defaultBtn:{
        backgroundColor:'#ffd64f',
        width:Dimensions.get('window').width*0.5,
        justifyContent:'center',
        alignSelf:'center'
    },
    centerFlex:{
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
    },
    borderlessItem:{
        borderColor: 'transparent',
        flex:1, 
        flexDirection:'row',
        justifyContent:'center', 
        margin:10
    },
    headerOverlap:{
        backgroundColor:'#E6E6E6'
    },
    inputField:{
      width: '70%',
      height: '10.3%',
      opacity: 0.5,
      borderRadius: 15.4,
      backgroundColor: "#ffffff"
    },
    loginBtn:{
      width: '70%',
      height: '10.3%',
      opacity: 0.7,
      borderRadius: 15.4
    }
})

export const fonts = {
    btnFont:14,
    contentFont:14,
    titleFont:14,
}

export const colors = {
    btnColor:'#ffd64f',
    btnTxtColor:'#000000',
    bgColor:'#1b3967',
    txtColor:'#ffffff',
    formTxtColor:'#000000',
    topHeaderColor:'#ffffff',
    headerColor:'#00bac6',
    bottomTabBarColor:'#00bac6',
    lightBgColor:'#cedae3',
    yellowColor:'#ffd64f',
    lightGray:'#E6E6E6'
}